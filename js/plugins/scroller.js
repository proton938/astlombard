(function($, undef) {
    var START_DELAY = 300,
        INCREMENT_INTERVAL = 50;

    var defaultOptions = {
        disabledClass: 'is-disabled',
        vertical: false,
        duration: 300,
        delay: 5000,
        autoScroll: false,
        hideArrs: false,
        coefGrad: 1.1,
        defaultGrad: 5,
        maxGrad: 30,
        scrollDelta: 5,
        swipeHandler: function(dx, dy) {
            if (Math.abs(dx) < Math.abs(dy) || Math.abs(dx) < 20) return 0;
            if (dx > 0) {
                return -1;
            } else {
                return 1;
            }
        },
        afterChange: function(idx, itemCount) {}
    };

    var Scroller = function(items, options) {
        this.$items = items;
        this.opt = options;
        this.process = false;
        this.idx = -1;
        this.timer = -1;
        this.setup();
        return this;
    }
    Scroller.prototype = {
        setup: function() {
            var self = this;

            this.$container = (this.opt.container) ? $(this.opt.container) : this.$items.eq(0).parent();
            this.container = this.$container.get(0);

            this.$prev = $(this.opt.prev);
            this.$next = $(this.opt.next);

            this.prevClick = function(e) {
                e.preventDefault();
            }
            this.prevMouseDown = function(e) {
                self.startScrolling(-1);
            }
            this.prevMouseUp = function(e) {
                if (self.started) {
                    if (self.process) {
                        self.stopScrolling(-1);
                    } else {
                        self.clearScrolling();
                        self.scroll(-1);
                    }
                }
            }
            this.prevMouseLeave = function(e) {
                if (self.process) self.stopScrolling(-1);
            }

            this.nextClick = function(e) {
                e.preventDefault();
            }
            this.nextMouseDown = function(e) {
                self.startScrolling(1);
            }
            this.nextMouseUp = function(e) {
                if (self.started) {
                    if (self.process) {
                        self.stopScrolling(1);
                    } else {
                        self.clearScrolling();
                        self.scroll(1);
                    }
                }
            }
            this.nextMouseLeave = function(e) {
                if (self.process) self.stopScrolling(1);
            }

            this.$prev.on('mousedown', this.prevMouseDown).on('mouseup', this.prevMouseUp).on('mouseleave', this.prevMouseLeave).on('click', this.prevClick);
            this.$next.on('mousedown', this.nextMouseDown).on('mouseup', this.nextMouseUp).on('mouseleave', this.nextMouseLeave).on('click', this.nextClick);

            this.touchStart = function(e) {
                var touches = e.originalEvent.touches || e.originalEvent.targetTouches;
                if ((touches && touches.length !== 1) || self.touch) {
                    return true;
                }

                self.$container.on('touchmove', self.touchMove).on('touchend', self.touchEnd).on('touchcancel', self.touchCancel);

                self.touch = {
                    x: touches[0].pageX,
                    y: touches[0].pageY
                };
            }
            this.touchMove = function(e) { }
            this.touchEnd = function(e) {
                if (!self.touch) {
                    return true;
                }

                var touches = e.originalEvent.changedTouches;

                var dx = touches[0].pageX - self.touch.x,
                    dy = touches[0].pageY - self.touch.y;

                var delta = self.opt.swipeHandler(dx, dy);

                if (delta !== 0) {
                    if (delta < 0) {
                        if (self.opt.loop || self.idx > 0) self.scrollPrev();
                    } else {
                        if (self.opt.loop || self.idx < self.$items.length - 1) self.scrollNext();
                    }
                }
                self.$container.off('touchmove', self.touchMove).off('touchend', self.touchEnd).off('touchcancel', self.touchCancel);
                delete self.touch;
            }
            this.touchCancel = function(e) {
                self.$container.off('touchmove', self.touchMove).off('touchend', self.touchEnd).off('touchcancel', self.touchCancel);
                delete self.touch;
            }

            this.currentGrad = this.opt.defaultGrad;

            this.resizeHandler = function() {
                self.scrollTo(self.idx, true);
            };
            this.$container.on('touchstart', this.touchStart);
            this.$container.on('resize', this.resizeHandler);
            $(window).on('resize', this.resizeHandler);

            if (typeof this.opt.idx !== 'undefined') {
                this.scrollTo(this.opt.idx, true);
            } else {
                this.setIdx();
                this.check();
                this.opt.afterChange.call(this.$items.get(this.idx), this.idx, this.$items.length);
            }

            if (this.opt.autoScroll && this.$items.length > 1) {
                this.resetTimer();

                this.mouseOver = function() {
                    self.clearTimer();
                }
                this.mouseOut = function() {
                    self.resetTimer();
                }

                $(this.container).on('mouseover', this.mouseOver).on('mouseout', this.mouseOut);
            }
        },
        resetTimer: function() {
            var self = this;
            if (this.stimer > -1) this.clearTimer();
            this.stimer = setTimeout(function() { self.scrollNext() }, this.opt.delay);
        },
        clearTimer: function() {
            if (this.stimer > -1) {
                clearTimeout(this.stimer);
                this.stimer = -1;
            }
        },
        scrollNext: function() {
            var i = this.idx + 1;
            if (i > this.$items.length - 1) i = 0;
            var k = (i > this.idx) ? 1 : -1;
            this.scroll(k);
        },
        scrollPrev: function() {
            var i = this.idx - 1;
            if (i < 0) i = this.$items.length - 1;
            var k = (i < this.idx) ? -1 : 1;
            this.scroll(k);
        },
        startScrolling: function(k) {
            var self = this;
            this.started = true;
            this.dtimer = setTimeout(function() {
                self.process = true;
                self.currentValue = self._getScrollPos(self.container);
                self.currentGrad = self.opt.defaultGrad;
                self.timer = setInterval(function() { self._scroll(k) }, INCREMENT_INTERVAL);
            }, START_DELAY);
        },
        clearScrolling: function() {
            if (this.started) {
                this.started = false;
                clearTimeout(this.dtimer);
                this.dtimer = -1;
            }
        },
        stopScrolling: function(k) {
            this.clearScrolling();
            if (this.process) {
                this.process = false;
                clearInterval(this.timer);
                this.timer = -1;
                this.scroll(k);
            }
        },
        _scroll: function(k) {
            this.currentValue += k*this.currentGrad;
            var newValue = this.currentValue;
            var hiddenSize = this._getScrollSize(this.container) - this._getClientSize(this.container);

            var stop = false
            if (newValue < 0) {
                newValue = 0;
                stop = true;
            } else if (newValue > hiddenSize) {
                newValue = hiddenSize;
                stop = true;
            }

            this._setScrollPos(this.container, newValue);
            this.check(newValue);

            if (stop) {
                this.stopScrolling(k);
            } else {
                this._incGrad();
            }
        },
        _incGrad: function() {
            if (this.currentGrad < this.opt.maxGrad) this.currentGrad *= this.opt.coefGrad;
            if (this.currentGrad > this.opt.maxGrad) this.currentGrad = this.opt.maxGrad;
        },
        scroll: function(k, force) {
            var d = (force) ? 0 : this.opt.duration;

            var hiddenSize = this._getScrollSize(this.container) - this._getClientSize(this.container);
            var scrollPos = this._getScrollPos(this.container);
            var clientSize = this._getClientSize(this.container);

            if (k < 0) {
                for (var i = 1; i < this.$items.length; i ++) {
                    if (this._getOffsetPos(this.$items.get(i)) >= scrollPos - this.opt.scrollDelta) {
                        scrollPos = this._getOffsetPos(this.$items.get(i - 1));
                        break;
                    }
                }
            } else if (k > 0) {
                for (var i = this.$items.length - 2; i > -1; i --) {
                    if (this._getOffsetPos(this.$items.get(i)) + this._getOffsetSize(this.$items.get(i)) <= scrollPos + clientSize + this.opt.scrollDelta) {
                        scrollPos = this._getOffsetPos(this.$items.get(i + 1)) + this._getOffsetSize(this.$items.get(i + 1)) - clientSize;
                        break;
                    }
                }
            }
            
            if (scrollPos < 0) {
                scrollPos = 0;
            } else if (scrollPos > hiddenSize) {
                scrollPos = hiddenSize;
            }

            this._setScrollPos(this.container, scrollPos, d);

            this.setIdx(scrollPos);
            this.check(scrollPos);
            this.opt.afterChange.call(this.$items.get(this.idx), this.idx, this.$items.length);

            if (this.stimer > -1) this.resetTimer();
        },
        scrollTo: function(i, force) {
            var d = (force) ? 0 : this.opt.duration;

            var hiddenSize = this._getScrollSize(this.container) - this._getClientSize(this.container);
            var clientSize = this._getClientSize(this.container);
            var scrollPos = this._getOffsetPos(this.$items.get(i));

            if (scrollPos < 0) {
                scrollPos = 0;
            } else if (scrollPos > hiddenSize) {
                scrollPos = hiddenSize;
            }

            this._setScrollPos(this.container, scrollPos, d);

            this.setIdx(scrollPos);
            this.check(scrollPos);
            this.opt.afterChange.call(this.$items.get(this.idx), this.idx, this.$items.length);

            if (this.stimer > -1) this.resetTimer();
        },
        check: function(scrollPos) {
            var scrollSize = this._getScrollSize(this.container);
            var clientSize = this._getClientSize(this.container);

            if (typeof scrollPos == 'undefined') scrollPos = this._getScrollPos(this.container);

            if (this.opt.hideArrs) {
                if (scrollSize <= clientSize) {
                    this.$prev.hide();
                    this.$next.hide();
                } else {
                    this.$prev.show();
                    this.$next.show();
                }
            }

            if (scrollPos <= 0) {
                this.$prev.addClass(this.opt.disabledClass)
            } else {
                this.$prev.removeClass(this.opt.disabledClass);
            }
            if (scrollPos >= scrollSize - clientSize) {
                this.$next.addClass(this.opt.disabledClass)
            } else {
                this.$next.removeClass(this.opt.disabledClass);
            }
        },
        setIdx: function(scrollPos) {
            var idx = -1;
            if (typeof scrollPos == 'undefined') scrollPos = this._getScrollPos(this.container);

            for (var i = 0; i < this.$items.length - 1; i ++) {
                if (this._getOffsetPos(this.$items.get(i)) >= scrollPos) {
                    idx = i;
                    break;
                }
            }
            this.idx = idx;
        },
        destroy: function() {
            this.$prev.off('mousedown', this.prevMouseDown).off('mouseup', this.prevMouseUp).off('mouseleave', this.prevMouseLeave).off('click', this.prevClick).removeClass(this.opt.disabledClass);
            this.$next.off('mousedown', this.nextMouseDown).off('mouseup', this.nextMouseUp).off('mouseleave', this.nextMouseLeave).off('click', this.nextClick).removeClass(this.opt.disabledClass);
            $(this.container).off('resize', this.resizeHandler);

            if (this.opt.autoScroll && this.$items.length > 1) {
                $(this.container).off('mouseover', this.mouseOver).off('mouseout', this.mouseOut);
            }

            $(window).off('resize', this.resizeHandler);
        },
        _setScrollPos: function(el, val, d) {
            var p = (this.opt.vertical) ? 'scrollTop' : 'scrollLeft';
            if (d > 0) {
                var par = {}; par[p] = val;
                $(el).animate(par, d);
            } else {
                el[p] = val;
            }
        },
        _getScrollPos: function(el) {
            var p = (this.opt.vertical) ? 'scrollTop' : 'scrollLeft';
            return Math.round(el[p]);
        },
        _getOffsetPos: function(el) {
            var p = (this.opt.vertical) ? 'offsetTop' : 'offsetLeft';
            return Math.round(el[p]);
        },
        _getScrollSize: function(el) {
            var p = (this.opt.vertical) ? 'scrollHeight' : 'scrollWidth';
            return Math.round(el[p]);
        },
        _getOffsetSize: function(el) {
            var p = (this.opt.vertical) ? 'offsetHeight' : 'offsetWidth';
            return Math.round(el[p]);
        },
        _getClientSize: function(el) {
            var p = (this.opt.vertical) ? 'clientHeight' : 'clientWidth';
            return Math.round(el[p]);
        }
    }

    $.fn.extend({
        scroller: function(options) {
            if (!this._scroller) {
                this._scroller = new Scroller(this, $.extend({}, defaultOptions, options));
            } else {
                $.extend(this._scroller.options, options);
                this._scroller.check();
            }
            return this;
        },
        unscroller: function() {
            if (this._scroller) {
                this._scroller.destroy();
                delete this._scroller;
            }
            return this;
        }
    });
})(jQuery);
