(function($, undef) {
    var defaultOptions = {
        duration: 300,
        delay: 5000,
        autoChange: false,
        stopOnMouseOver: false,
        loop: false,
        activeClass: 'is-active',
        disabledClass: 'is-disabled',
        hideArrs: false,
        hideNav: false,
        swipeHandler: function(dx, dy) {
            if (Math.abs(dx) < Math.abs(dy) || Math.abs(dx) < 20) return 0;
            if (dx > 0) {
                return -1;
            } else {
                return 1;
            }
        },
        beforeChange: function(prevIdx, nextIdx, itemCount) {},
        afterChange: function(prevIdx, nextIdx, itemCount) {},
        goOutCssBefore: function(prevIdx, nextIdx, itemCount) { return { opacity: 1 } },
        goOutCssAfter: function(prevIdx, nextIdx, itemCount) { return { opacity: 0 } },
        goInCssBefore: function(prevIdx, nextIdx, itemCount) { return { opacity: 0 } },
        goInCssAfter: function(prevIdx, nextIdx, itemCount) { return { opacity: 1 } }
    };

    var Slider = function(items, options) {
        this.$items = items;
        this.opt = options;
        this.process = false;
        this.idx = -1;
        this.timer = -1;
        this.setup();
        return this;
    }
    Slider.prototype = {
        setup: function() {
            var self = this;

            this.$container = (this.opt.container) ? $(this.opt.container) : this.$items.eq(0).parent();

            this.$nav = $(this.opt.nav);
            this.$prev = $(this.opt.prev);
            this.$next = $(this.opt.next);

            this.prevClick = function(e) {
                if (self.opt.loop || self.idx > 0) self.showPrev();
                return false;
            }
            this.nextClick = function(e) {
                if (self.opt.loop || self.idx < self.$items.length - 1) self.showNext();
                return false;
            }
            this.navClick = function(e) {
                var idx = self.$nav.index(this);
                if (idx != self.idx) {
                    self.show(idx);
                }
                return false;
            }

            this.touchStart = function(e) {
                var touches = e.originalEvent.touches || e.originalEvent.targetTouches;
                if ((touches && touches.length !== 1) || self.touch) {
                    return true;
                }

                self.$container.on('touchmove', self.touchMove).on('touchend', self.touchEnd).on('touchcancel', self.touchCancel);

                self.touch = {
                    x: touches[0].pageX,
                    y: touches[0].pageY
                };
            }
            this.touchMove = function(e) { }
            this.touchEnd = function(e) {
                if (!self.touch) {
                    return true;
                }

                var touches = e.originalEvent.changedTouches;

                var dx = touches[0].pageX - self.touch.x,
                    dy = touches[0].pageY - self.touch.y;

                var delta = self.opt.swipeHandler(dx, dy);

                if (delta !== 0) {
                    if (delta < 0) {
                        if (self.opt.loop || self.idx > 0) self.showPrev();
                    } else {
                        if (self.opt.loop || self.idx < self.$items.length - 1) self.showNext();
                    }
                }
                self.$container.off('touchmove', self.touchMove).off('touchend', self.touchEnd).off('touchcancel', self.touchCancel);
                delete self.touch;
            }
            this.touchCancel = function(e) {
                self.$container.off('touchmove', self.touchMove).off('touchend', self.touchEnd).off('touchcancel', self.touchCancel);
                delete self.touch;
            }

            this.$prev.on('click', this.prevClick);
            this.$next.on('click', this.nextClick);
            this.$nav.on('click', this.navClick);

            this.$container.on('touchstart', this.touchStart);

            if (typeof this.opt.idx !== 'undefined') {
                this.idx = this.opt.idx;
            } else {
                this.idx = 0;
                for (var i = 0; i < this.$nav.length; i ++) {
                    if (this.$nav.eq(i).hasClass(this.opt.activeClass)) {
                        self.idx = i; break;
                    }
                }
            }

            this.opt.beforeChange.call(this.$items.get(this.idx), -1, this.idx, this.$items.length);
            this.$items.hide().eq(this.idx).show();
            this.$nav.removeClass(this.opt.activeClass).eq(this.idx).addClass(this.opt.activeClass);
            this.opt.afterChange.call(this.$items.get(this.idx), -1, this.idx, this.$items.length);

            this.check();

            if (this.opt.autoChange && this.$items.length > 1) {
                this.resetTimer();
                if (this.opt.stopOnMouseOver) {
                    this.containerMouseOver = function(e) {
                        self.clearTimer();
                    }
                    this.containerMouseOut = function(e) {
                        self.resetTimer();
                    }
                    this.$container.on('mouseover', this.containerMouseOver).on('mouseout', this.containerMouseOut);
                }
            }
        },
        resetTimer: function() {
            var self = this;
            if (this.timer > -1) this.clearTimer();
            this.timer = setTimeout(function() { self.showNext() }, this.opt.delay);
        },
        clearTimer: function() {
            if (this.timer >= 0) {
                clearTimeout(this.timer);
                this.timer = -1;
            }
        },
        showNext: function(force) {
            var i = this.idx + 1;
            if (i > this.$items.length - 1) i = 0;
            this.show(i, force);
        },
        showPrev: function(force) {
            var i = this.idx - 1;
            if (i < 0) i = this.$items.length - 1;
            this.show(i, force);
        },
        show: function(i, force) {
            if (i == this.idx) return;
            var self = this;
            this.process = true;
            var d = (force) ? 0 : this.opt.duration;
            var $prev = this.$items.eq(this.idx),
                $next = this.$items.eq(i),
                prev = $prev.get(0),
                next = $next.get(0),
                prevIdx = this.idx,
                count = this.$items.length;

            self.opt.beforeChange.call(prev, prevIdx, i, count);
            $prev
                .stop(true, true)
                .css(self.opt.goOutCssBefore.call(prev, prevIdx, i, count))
                .animate(self.opt.goOutCssAfter.call(prev, prevIdx, i, count), { duration: d, complete: function() {
                    $prev.hide();
                } });
            $next
                .stop(true, true)
                .css(self.opt.goInCssBefore.call(next, prevIdx, i, count)).show()
                .animate(self.opt.goInCssAfter.call(next, prevIdx, i, count), { duration: d, complete: function() {
                    self.process = false;
                    self.opt.afterChange.call(next, prevIdx, i, count);
                } });
            self.$nav.removeClass(self.opt.activeClass).eq(i).addClass(self.opt.activeClass);

            this.idx = i;
            this.check();

            if (this.timer > -1) this.resetTimer();
        },
        check: function() {
            if (this.$items.length <= 1) {
                if (this.opt.hideArrs) {
                    this.$prev.hide();
                    this.$next.hide();
                }
                if (this.opt.hideNav) {
                    this.$nav.hide();
                }
            } else {
                if (this.opt.hideArrs) {
                    this.$prev.show();
                    this.$next.show();
                }
                if (this.opt.hideNav) {
                    this.$nav.show();
                }
            }
            if (!this.opt.loop && this.idx == 0) this.$prev.addClass(this.opt.disabledClass)
            else this.$prev.removeClass(this.opt.disabledClass);
            if (!this.opt.loop && this.idx == this.$items.length - 1) this.$next.addClass(this.opt.disabledClass)
            else this.$next.removeClass(this.opt.disabledClass);
        },
        destroy: function() {
            this.clearTimer();
            this.$prev.off('click', this.prevClick).removeClass(this.opt.disabledClass);
            this.$next.off('click', this.nextClick).removeClass(this.opt.disabledClass);
            this.$nav.off('click', this.navClick).removeClass(this.opt.activeClass);
            this.$items.css({ display: '' }).css(this.opt.goInCssAfter(0, 1, 1));
            this.$container.off('touchstart', this.touchStart);
            if (this.opt.autoChange && this.$items.length > 1 && this.opt.stopOnMouseOver) {
                this.$container.off('mouseover', this.containerMouseOver).off('mouseout', this.containerMouseOut);
            }
        }
    }

    $.fn.extend({
        slider: function(options) {
            if (!this._slider) {
                this._slider = new Slider(this, $.extend({}, defaultOptions, options));
            } else {
                this._slider.check();
            }
            return this;
        },
        unslider: function() {
            if (this._slider) {
                this._slider.destroy();
                delete this._slider;
            }
            return this;
        }
    });
})(jQuery);
