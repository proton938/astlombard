'use strict';

/* global $ */

let settings = {
    selector: '.js-home-slider',
    itemSelector: '.js-home-slider__item',
    navSelector: '.js-home-slider__nav',
    linkSelector: '.js-home-slider__link',
    prevSelector: '.js-home-slider__prev',
    nextSelector: '.js-home-slider__next',
    slider: {
        duration: 400,
        autoChange: true,
        delay: 5000,
        loop: true
    }
};

class HomeSlider {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ HomeSlider/init ]');

        let $block = $(this.opt.selector);
        if ($block.length) {
            let $items = $block.find(this.opt.itemSelector),
                $nav = $block.find(this.opt.navSelector),
                $links = $block.find(this.opt.linkSelector),
                $prev = $block.find(this.opt.prevSelector),
                $next = $block.find(this.opt.nextSelector);

            $items.slider($.extend({}, this.opt.slider, {
                nav: $links,
                prev: $prev,
                next: $next
            }));
        }
    }
}

export {
	HomeSlider
};
