'use strict';

/* global $ */

import { Ajax } from '../utils/ajax';
import { fancybox } from '../settings';

let settings = {
	linkSelector: '.js-location-select__link',
	citySelector: '.js-location-select__city',
	fancybox: {}
};

class LocationSelect {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ LocationSelect/init ]');

        let $link = $(this.opt.linkSelector);

        $link.on('click', (e) => {
	        e.preventDefault();
	        this.open();
	    });
    }
    open() {
        console.log('CitySelect/open');

        Ajax.get('get_location_select').then((data) => {
            if (data.ok) {
                this.show(data.content);
            } else {
                throw new Error('Cannot get location select');
            }
        }).catch((error) => {
            throw error;
        });
    }
    show(content) {
        console.log('CitySelect/show');

        let self = this;

        $.fancybox($.extend(true, {}, fancybox, this.opt.fancybox, {
            type: 'html',
            content: content,
            afterShow: function() {
                this.inner.find(self.opt.citySelector).click(function(e) {
                    e.preventDefault();
                    self.select($(this).data()).then((data) => {
                    	$.fancybox.close();
                    }).catch((error) => {
                    	throw error;
                    });
                });
            }
        }));
    }
    select(data) {
    	return Ajax.get('save_location', data).then((data) => {
            if (data.ok) {
                document.location.reload();
                return data;
            } else {
                throw new Error("Cannot save location select");
            }
        });
    }
}

export {
    LocationSelect
};
