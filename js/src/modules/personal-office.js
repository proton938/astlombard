'use strict';

/* global $ */

import { Ajax } from 'utils/ajax';

let settings = {
    rootWrap: '.js-po-wrap',
};
let $wrap;
class PersonalOffice {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ PersonalOffice/init ]');
        $wrap = $(this.opt.rootWrap);
        this.getLogin();
    }
    getLogin() {
    	let self = this;
    	let loginTemplate = 

            '<div class="personal-office__toptext personal-office__paragraph">' +
    	    '  <h1 class="h1 m--block-yellow personal-office__paragraph">Войти в личный кабинет</h1>' +
            '</div>' +
            '<div class="personal-office__toptext personal-office__linksbottom personal-office__paragraph">' +
    	    '   <a id="registration" class="personal-office__link" href="#">Регистрация</a>' +
    	    '   <a id="recall-password" class="personal-office__link" href="#">Забыли пароль</a>' +
            '</div>' +
            '<div class="personal-office__height" >' +
            '   <div class="personal-office__textblock personal-office__offset">' +
            '       <div class="filter__subblock-header">В Личном кабинете:</div>' +
            '       <div class="news__item-preview "><p>' +
            '           &#8212; Вся информация по каждому вашему займу (действующим и погашенным залоговым билетам)<br>' +
            '           &#8212; Ваш статус лояльности<br>' +
            '           &#8212; Личные предложения<br>' +
            '           &#8212; Возможность пермонально обратиться в службу поддержки' +
            '       </p></div>' +
            '   </div>' +
            '   <div class="personal-office__fieldwidth">' +
    	    '      <form class="personal-office__form">' +
            '        <input type="hidden" value="Authenticate" name="request" />' +
            '        <div class="personal-office__offset">' +
            '           <label >' +
            '              <input name="phone" class="personal-office__field  personal-office__fieldwidth" placeholder="Введите номер телефона" />' +
            '           </label>' +
            '        </div>' +
            '        <div class="personal-office__offset">' +
            '           <label >' +
            '              <input name="iin" class="personal-office__field  personal-office__fieldwidth" placeholder="Введите номер ИИН" />' +
            '           </label>' +
            '        </div>' +
    	    '	     <div class="personal-office__offset">' +
    	    '		    <label>' +
    	    '			    <input name="password" type="password" class="personal-office__field  personal-office__fieldwidth"  placeholder="Введите Ваш пароль" /> ' +
            '               <div class="personal-office__icon"></div>' + 
    	    '		    </label>' +
    	    '	     </div>' +
    	    '	     <div class = "personal-office__fieldwidth">' +
    	    '		    <button class="personal-office__button personal-office__center">ВОЙТИ</button>' + 
    	    '	     </div>' +
    	    '      </form>' + 
            '   </div>' +
            '</div>'

            ;

    	let tmplt = _.template(loginTemplate);
    	$wrap.html(tmplt());
    	$wrap.on('click', '#registration', function (e) {
            self.registration();
            e.preventDefault();
        });
        let $form = $wrap.find('form');
        $form.submit((e) => {
            e.preventDefault();
            let params = $form.serializeObject();
            console.log('Before sendRequest', params);
            this.sendRequest(params, (data) => {this.processAuthenticate(data); });
        });
	}
	registration() {
//		let $wrap = $(this.opt.rootWrap);
		let registrationTemplate = 

            '<div class="personal-office__breadcrumbs">' +
            '   <a href="/" class="breadcrumbs__link">Главная страница></a>' +
            '   <a class="breadcrumbs__item  is-active" >Личный кабинет</a>' +
            '</div>' +
            '<div class="personal-office__toptext personal-office__paragraph">' +
            '  <h1 class="h1 m--block-yellow">Регистрация кабинета</h1>' +
            '</div>' +
            '<div class="personal-office__toptext personal-office__linksbottom">' +
            '  <a id="registration" class="personal-office__link" href="#">Вход в личный кабинет</a>' +
            '</div>' +
            '<div class="personal-office__height personal-office__offset" >' +
            '  <form class = "personal-office__form">' +
    	    '	 <input type="hidden" value="Register" name="request" />' +
    	    '	 <div class="personal-office__offset">' +
            '         <label >' +
            '            <input name="iin" class="personal-office__field  personal-office__fieldwidth" placeholder = "Введите номер ИНН" />' +
            '         </label>' +
            '         <div class="personal-office__warningincorrect  personal-office__warningdisplay">Некорректный номер ИНН</div>' +
    	    '	 </div>' +
            '    <div class="personal-office__offset">' +
            '         <label >' +
            '            <input name="phone" class="personal-office__field  personal-office__fieldwidth"  placeholder = "Введите номер Вашего телефона" />' +
            '         </label>' +
            '         <div class="personal-office__warningincorrect   personal-office__warningdisplay" >Некорректный номер телефона</div>' +
            '    </div>' +
            '    <div class = "personal-office__fieldwidth">' +
            '          <button class="personal-office__button  personal-office__center">Отправить данные</button>' + 
            '    </div>' +
    	    '</form> ' +
            '</div>' 
         
		;
    	let tmplt = _.template(registrationTemplate);
    	$wrap.html(tmplt());

    	// $wrap.on('register:sendCode', (e, data) => {
    	// 	this.processRegisterCode( data );
    	// });
    	
    	let $form = $wrap.find('form');
        $form.submit((e) => {
            e.preventDefault();
            let params = $form.serializeObject();
            console.log('Before sendRequest', params);
            this.sendRequest(params, (data) => {this.processRegisterCode(data); });
        });
	}
    getCheckCode(params) {
        console.log('[ PersonalOffice/getCheckCode ]', params);
        
    	let template = 

            '<div class="personal-office__breadcrumbs">' +
            '   <a href="/" class="breadcrumbs__link">Главная страница></a>' +
            '   <a class="breadcrumbs__item  is-active" >Личный кабинет</a>' +
            '</div>' +
    		'<div class="personal-office__toptext">' +
            '   <h1 class="h1 m--block-yellow">Введите код из СМС</h1>' +
            '</div>' +
            '<div class="personal-office__height" >' +
            '<div class="personal-office__textblock personal-office__fieldwidth personal-office__offset" ><p>' +
            '   На ваш номер мобильного телефона отправлено СМС-уведомление с кодом для активации заявки регистрации личного кабинета.<br>' +
            '   Для продолжения регистрации введите код из СМС в поле и нажмите кнопку "Далее"' +
            '</p></div>' +
            '   <div class="personal-office__fieldwidth" >' +    
            '       <form class="personal-office__form">' +
            '           <input type="hidden" value="Register" name="request" />' +
            '           <div class="personal-office__offset">' +
            '               <label >' +
            '                   <input class="personal-office__field" name="code"  placeholder = "Код из СМС" />' +
            '               </label>' +
            '           </div>' +
            '           <div class="personal-office__offset">' +
            '               <label>' +
            '                   <input name="password" type="password" class="personal-office__field personal-office__fieldwidth" placeholder="Придумайте пароль" /> ' +
            '                   <div class="personal-office__icon"></div>' + 
            '               </label>' +
            '           </div>' +
            '           <div class="personal-office__offset">' +
            '               <label>' +
            '                   <input name="password_dbl" type="password" class="personal-office__field  personal-office__fieldwidth" placeholder = "Повторите" /> ' +
            '                   <div class="personal-office__icon"></div>' + 
            '               </label>' +
            '           </div>' +
            '           <div class = "personal-office__fieldwidth">' +
            '               <button class="personal-office__button personal-office__center">Далее</button>' + 
            '           </div>' +
            '       </form>'  +
            '   </div>' +
            '</div>'
    	;
    	let tmplt = _.template(template);
    	$wrap.html(tmplt());
    	let $form = $wrap.find('form');
        $form.submit((e) => {
            e.preventDefault();
            let params = $form.serializeObject();
            // console.log(params);
            this.sendRequest(params, (data) => {this.processRegisterCode(data); });
        });
    }
    registerSuccess() {
        console.log('[ PersonalOffice/registerSuccess ]');
        let template = 
            '<p>' +
            '    Вы успешно зарегистрировались в личном кабинете' +
            '</p>' +
            '<form>' +
            '   <div class = "personal-office__fieldwidth">' +
            '       <button class="personal-office__button personal-office__center">Далее</button>' + 
            '   </div>' +
            '</form>';
        let tmplt = _.template(template);
        $wrap.html(tmplt());
        let $form = $wrap.find('form');
        $form.submit((e) => {
            e.preventDefault();
            this.getLogin();
        });
    }
    processRegisterCode( data ) {
    	console.log('[ PersonalOffice/processRegisterCode ]', data);
        switch (data.ok) {
        	case "0" :
        	    console.log('Нет ошибки');
                if (data.type == 1) {
        		    this.getCheckCode('');
                } else {
                    this.registerSuccess();
                }
        		break;
        	case "1" :
        		console.log('Ошибка...');
        		break;
        	case "2" :
        		console.log('Неверный код');
        		break;
        	case "3" :
        		console.log('Ошибка в ИНН');
        		break;
        	case "4" :
        		console.log('Ошибка в номере телефона');
        		break;
        	default :
        		console.error('Error in request');
        		break;
        }
    }
    processAuthenticate( data ) {
        console.log('[ PersonalOffice/processAuthenticate ]', data);
        let self = this;
        switch (data.ok) {
            case "0" :
                console.log('Операция выполнена успешно');
                let params = {request: 'GetTickets'};
                this.sendRequest(params, (data) => {self.processGetTickets(data); });
                break;
            case "1" :
                console.log('Неверный пароль');
                break;
            case "2" :
                console.log('Клиент не зарегистрирован');
                break;
        }
    }
    processGetTickets( data ) {
        console.log('[ PersonalOffice/processGetTickets ]', data);
        let len = data.Credits.length;
        console.log(len);
        let template = 
        '<div class="loans__table">' +
        '   <div class="loans__row">' +
        '       <div class="loans__general-cell-active">Номер билета</div>' +
        '       <div class="loans__general-cell-active-even">Статус билета</div>' +
        '       <div class="loans__general-cell-active">Дата окончания займа</div>' +
        '       <div class="loans__general-cell-active-even">Осталось дней</div>' +
        '       <div class="loans__general-cell-active">Сумма займа, т</div>' +
        '       <div class="loans__general-cell-active-even">Процент займа</div>' +
        '       <div class="loans__general-cell-active">Общая сумма задолженности</div>' +
        '       <div class="loans__general-cell-active-even">&nbsp;</div>' +
        '   </div>' +
        '   <% items.forEach(function(item) { %>' +
        '       <div class="loans__row">' +
        '           <div class="loans__cell-active">' +
        '               <div class="loans__cell-active__block">' +
        '                   <span class="loans__number-active"><%= item.Ticket %></span>' +
        '               </div>' +
        '           </div>' +
        '           <div class="loans__cell-active-even">' +
        '                <div class="loans__cell-active-even__block">' +
        '                   <%= item.Status %>' +
        '                </div>' +
        '           </div>' +
        '           <div class="loans__cell-active">' +
        '                <div class="loans__cell-active__block">' +
        '                &nbsp;</div>' +
        '           </div>' +
        '           <div class="loans__cell-active-even">' +
        '                <div class="loans__cell-active-even__block">' +
        '                &nbsp;</div>' +
        '           </div>' +
        '           <div class="loans__cell-active">' +
        '                <div class="loans__cell-active__block">' +
        '                &nbsp;</div>' +
        '           </div>' +
        '           <div class="loans__cell-active-even">' +
        '                <div class="loans__cell-active-even__block">' +
        '                &nbsp;</div>' +
        '           </div>' +
        '           <div class="loans__cell-active">' +
        '                <div class="loans__cell-active__block">' +
        '                &nbsp;</div>' +
        '           </div>' +
        '           <div class="loans__cell-active-even">' +
        '                <div class="loans__cell-active-even__block">' +
        '                &nbsp;</div>' +
        '           </div>' +
        '       </div>' +
        '   <% }); %>' +
        '</div>';
        let tmplt = _.template(template);
        $wrap.html(tmplt({items: data.Credits}));
    }
	sendRequest(params, resolve) {
        console.log('[ PersonalOffice/sendRequest ]', params);

        Ajax.get(params.request, params).then((data) => {
			resolve(data);
        }).catch(function(error) {
            throw new Error(error);
        });
    }
}

export {
    PersonalOffice
};
