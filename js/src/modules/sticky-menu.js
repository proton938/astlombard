'use strict';

/* global $ */

let settings = {
    stickyMenuSelector: '.js-sticky-menu',
    mainMenuSelector: '.js-main-menu'
};

class StickyMenu {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ StickyMenu/init ]');

        /*let $stickyMenu = $(this.opt.stickyMenuSelector),
            $mainMenu = $(this.opt.mainMenuSelector);*/

        let scrollIntervalID = setInterval(this.stickIt, 10);
    }
    stickIt() {
        let $stickyMenu = $('.js-sticky-menu'),
            $mainMenu = $('.js-main-menu');

        let originalMenuPos = $mainMenu.offset(),
            originalMenuTop = originalMenuPos.top;

        if ($(window).scrollTop() >= (originalMenuTop))  {
            $mainMenu.css('visibility', 'hidden');
            $stickyMenu.removeClass('is-hidden');
        } else {
            $stickyMenu.addClass('is-hidden');
            $mainMenu.css('visibility', 'visible');
        }
    }
}

export {
    StickyMenu
};
