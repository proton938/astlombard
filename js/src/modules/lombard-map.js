'use strict';

/* global $ */

let settings = {
    mapSelector: '.js-lombard__map',
    map: {
        zoom: 16,
        controls: ['default']
    },
    ymapsScriptSrc: 'https://api-maps.yandex.ru/2.1/?lang=ru_RU'
};

class LombardMap {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ LombardMap/init ]');

        this.$map = $(this.opt.mapSelector);
        if (this.$map.length) {
            this.coords = this.$map.data('coords').split(/[^0-9.\-\+]+/);
            this.opt.map.center = this.coords;

            $.getScript(this.opt.ymapsScriptSrc).done(() => {
                ymaps.ready(() => {
                    this.initMap();
                });
            }).fail(() => {
                throw new Error(`Can't load script ${ this.opt.ymapsScriptSrc }`);
            });
        }
    }
    initMap() {
        let map = new ymaps.Map(this.$map.get(0), this.opt.map);

        let placemark = new ymaps.Placemark(this.opt.map.center, {}, {
            iconLayout: 'default#image',
            iconImageHref: '/i/icons/icon_circle.svg',
            iconImageSize: [24, 24]
        });
        map.geoObjects.add(placemark);
        
        map.behaviors.disable('scrollZoom');
    }
}

export {
    LombardMap
};
