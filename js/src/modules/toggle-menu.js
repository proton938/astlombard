'use strict';

/* global $ */

let settings = {
    listSelector: '.js-toggle-menu__list',
    itemSelector: '.js-toggle-menu__item',
    sublistSelector: '.js-toggle-menu__sublist',
    triggerSelector: '.js-toggle-menu__trigger'
};

class ToggleMenu {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ ToggleMenu/init ]');

        let $trigger = $(this.opt.triggerSelector),
            $list = $(this.opt.listSelector),
            $item = $(this.opt.itemSelector);

        $trigger.on('click', function(e) {
            $list.toggleClass('is-opened');
            $trigger.toggleClass('is-active');
        });
    }
}

export {
    ToggleMenu
};
