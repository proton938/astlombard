'use strict';

/* global $ */

let settings = {
    selector: '.js-products-slider',
    itemSelector: '.js-products-slider__item',
    prevSelector: '.js-products-slider__prev',
    nextSelector: '.js-products-slider__next',
    slider: {
        duration: 400,
        loop: true
    }
};

class ProductsSlider {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ ProductsSlider/init ]');

        let $blocks = $(this.opt.selector);

        for (let i = 0; i < $blocks.length; i ++) {
            let $block = $blocks.eq(i);

            let $items = $block.find(this.opt.itemSelector),
                $prev = $block.find(this.opt.prevSelector),
                $next = $block.find(this.opt.nextSelector);

            $items.scroller($.extend({}, this.opt.slider, {
                prev: $prev,
                next: $next
            }));
        }
    }
}

export {
    ProductsSlider
};
