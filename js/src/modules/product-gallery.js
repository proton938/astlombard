'use strict';

/* global $ */

let settings = {
    sliderSelector: '.js-product-gallery-slider',
    sliderContainerSelector: '.js-product-gallery-slider-container',
    sliderItemSelector: '.js-product-gallery-slider-item',
    sliderPrevSelector: '.js-product-gallery-slider-prev',
    sliderNextSelector: '.js-product-gallery-slider-next',
    sliderNavSelector: '.js-product-gallery-slider-nav',
    sliderOptions: {
        duration: 400,
        loop: true
    },
    scrollerSelector: '.js-product-gallery-scroller',
    scrollerContainerSelector: '.js-product-gallery-scroller-container',
    scrollerItemSelector: '.js-product-gallery-scroller-item',
    scrollerOptions: {
        duration: 200,
        hideArrs: true
    }
};

class ProductGallery {
    constructor(options) {
    	this.opt = $.extend({}, settings, options);
        this.init();
	}
    init() {
        console.log('[ ProductGallery/init ]');

        let $slider = $(this.opt.sliderSelector);
        if ($slider.length) {
            let $container = $slider.find(this.opt.sliderContainerSelector),
            	$items = $slider.find(this.opt.sliderItemSelector),
                $prev = $slider.find(this.opt.sliderPrevSelector),
                $next = $slider.find(this.opt.sliderNextSelector),
                $nav = $slider.find(this.opt.sliderNavSelector);

            $items.slider($.extend({}, this.opt.sliderOptions, {
                container: $container,
                prev: $prev,
                next: $next,
                nav: $nav
            }));
        }

        let $scroller = $(this.opt.scrollerSelector);
        if ($scroller.length) {
        	let $container = $scroller.find(this.opt.scrollerContainerSelector),
                $items = $scroller.find(this.opt.scrollerItemSelector);

            $items.scroller($.extend({}, this.opt.scrollerOptions, {
            	container: $container
            }));
        }
    }
}

export {
    ProductGallery
};
