'use strict';

/* global $ */

let settings = {
    selector: '.js-advantages-slider',
    itemSelector: '.js-advantages-slider__item',
    prevSelector: '.js-advantages-slider__prev',
    nextSelector: '.js-advantages-slider__next',
    slider: {
        duration: 400,
        loop: true
    }
};

class AdvantagesSlider {
    constructor(options) {
        this.opt = $.extend({}, settings, options);
        this.init();
    }
    init() {
        console.log('[ AdvantagesSlider/init ]');

        let $block = $(this.opt.selector);
        if ($block.length) {
            let $items = $block.find(this.opt.itemSelector),
                $prev = $block.find(this.opt.prevSelector),
                $next = $block.find(this.opt.nextSelector);

            $items.scroller($.extend({}, this.opt.slider, {
                prev: $prev,
                next: $next
            }));
        }
    }
}

export {
    AdvantagesSlider
};
