'use strict';

/* global $ */

import { interfaceName } from './settings';

import { AjaxForm } from './modules/ajax-form';
import { Goal } from './modules/goal';
import { Responsive } from './modules/responsive';
import { ToggleMenu } from './modules/toggle-menu';
import { StickyMenu } from './modules/sticky-menu';
import { HomeSlider } from './modules/home-slider';
import { AdvantagesSlider } from './modules/advantages-slider';
import { ProductsSlider } from './modules/products-slider';
import { ProductGallery } from './modules/product-gallery';
import { LombardMap } from './modules/lombard-map';
import { LocationSelect } from './modules/location-select';
import { PersonalOffice } from './modules/personal-office';

class Proj {
    constructor() {
        this.init();
    }
    init() {
        this.interfaceName = interfaceName;

        if (interfaceName == 'home_page') {
            this.homeSlider = new HomeSlider();
            this.advantagesSlider = new AdvantagesSlider();
            this.stickyMenu = new StickyMenu();
        }

        if (interfaceName == 'lombard') {
            this.lombardMap = new LombardMap();
        } 
        if (interfaceName == 'personal_office') {
            this.personalOffice = new PersonalOffice();
        }
        this.toggleMenu = new ToggleMenu();
        this.productsSlider = new ProductsSlider();
        this.productGallery = new ProductGallery();
        this.locationSelect = new LocationSelect();

        this.ajaxForm = new AjaxForm();
        this.goal = new Goal({
            goals: [
                {
                    selector: '.js-goal__link',
                    event: 'click',
                    handler: function() {
                        $(window).trigger('main:reachGoal', $(this).data());
                    }
                }, {
                    selector: '.js-goal__form',
                    event: 'submit',
                    handler: function() {
                        $(window).trigger('main:reachGoal', $(this).data());
                    }
                }, {
                    selector: window,
                    event: 'goal ajaxForm:goal',
                    handler: function(e, data) {
                        $(window).trigger('main:reachGoal', data);
                    }
                }
            ]
        });

        this.responsive = new Responsive();

    }
}

$(function() {
    window.proj = new Proj();
});
