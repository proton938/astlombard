'use strict';

import { YandexMetrika } from './counters/yandex-metrika';
import { GoogleAnalytics } from './counters/google-analytics';


const counters = [
    new YandexMetrika(),
    new GoogleAnalytics()
];


export {
    counters
};
