'use strict';


class AbstractCounter {
    constructor(options) {
        this.opt = options;
    }
    reachGoal() {
        throw new Error('You must define your own method');
    }
    get type() {
        throw new Error('You must define your own method');
    }
    get counter() {
        if (!this.__counter) {
            this.__counter = this.opt.counter || window[this.opt.counterName];
        }
        return this.__counter;
    }
}


export {
    AbstractCounter
};
