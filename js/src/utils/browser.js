'use strict';

/* global $ */


let browserType = $(document.body).data('browser');


class Browser {
    static get type() {
        return browserType;
    }
    static isDesktop() {
        return this.type === 'desktop';
    }
    static isTablet() {
        return this.type === 'tablet';
    }
    static isMobile() {
        return this.type === 'mobile';
    }
    static isDevice() {
        return this.isMobile() || this.isTablet();
    }
    static supportsAttribute(attr, el) {
        el = el || 'div';
        if (typeof el === 'string') {
            el = document.createElement(el);
        }
        return !!(attr in el);
    }
    static supportsStyle(prop, el) {
        el = el || 'div';
        if (typeof el === 'string') {
            el = document.createElement(el);
        }
        return el.style[prop] !== undefined;
    }
}


export {
    Browser
};
