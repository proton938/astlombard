'use strict';


function __detectSessionID() {
    let sessionId = '';
    if (window.location.toString().match(/SESS_ID=(\d+)/)) {
        sessionId = RegExp.$1;
    }
    return sessionId;
}

let fancybox = {};

let ajax = {
    type: 'post',
    url: '/json/',
    dataType: 'json',
    timeout: 10000,
    traditional: true,
    SESSION_ID: __detectSessionID()
};

let yaCounter = 'yaCounter000000';

let breakpoints = {
    xxl: Infinity,
    xl: 1620,
    l: 1280,
    m: 1000,
    s: 780,
    xs: 600,
    xxs: 420
};


let interfaceName = document.body.dataset['interface'];


export {
    fancybox,
    ajax,
    breakpoints,
    yaCounter,

    interfaceName
};
