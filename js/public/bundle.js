(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

/* global $ */

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _settings = require('./settings');

var _ajaxForm = require('./modules/ajax-form');

var _goal = require('./modules/goal');

var _responsive = require('./modules/responsive');

var _toggleMenu = require('./modules/toggle-menu');

var _stickyMenu = require('./modules/sticky-menu');

var _homeSlider = require('./modules/home-slider');

var _advantagesSlider = require('./modules/advantages-slider');

var _productsSlider = require('./modules/products-slider');

var _productGallery = require('./modules/product-gallery');

var _lombardMap = require('./modules/lombard-map');

var _locationSelect = require('./modules/location-select');

var _personalOffice = require('./modules/personal-office');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Proj = function () {
    function Proj() {
        _classCallCheck(this, Proj);

        this.init();
    }

    _createClass(Proj, [{
        key: 'init',
        value: function init() {
            this.interfaceName = _settings.interfaceName;

            if (_settings.interfaceName == 'home_page') {
                this.homeSlider = new _homeSlider.HomeSlider();
                this.advantagesSlider = new _advantagesSlider.AdvantagesSlider();
                this.stickyMenu = new _stickyMenu.StickyMenu();
            }

            if (_settings.interfaceName == 'lombard') {
                this.lombardMap = new _lombardMap.LombardMap();
            }
            if (_settings.interfaceName == 'personal_office') {
                this.personalOffice = new _personalOffice.PersonalOffice();
            }
            this.toggleMenu = new _toggleMenu.ToggleMenu();
            this.productsSlider = new _productsSlider.ProductsSlider();
            this.productGallery = new _productGallery.ProductGallery();
            this.locationSelect = new _locationSelect.LocationSelect();

            this.ajaxForm = new _ajaxForm.AjaxForm();
            this.goal = new _goal.Goal({
                goals: [{
                    selector: '.js-goal__link',
                    event: 'click',
                    handler: function handler() {
                        $(window).trigger('main:reachGoal', $(this).data());
                    }
                }, {
                    selector: '.js-goal__form',
                    event: 'submit',
                    handler: function handler() {
                        $(window).trigger('main:reachGoal', $(this).data());
                    }
                }, {
                    selector: window,
                    event: 'goal ajaxForm:goal',
                    handler: function handler(e, data) {
                        $(window).trigger('main:reachGoal', data);
                    }
                }]
            });

            this.responsive = new _responsive.Responsive();
        }
    }]);

    return Proj;
}();

$(function () {
    window.proj = new Proj();
});

},{"./modules/advantages-slider":2,"./modules/ajax-form":3,"./modules/goal":5,"./modules/home-slider":6,"./modules/location-select":7,"./modules/lombard-map":8,"./modules/personal-office":9,"./modules/product-gallery":10,"./modules/products-slider":11,"./modules/responsive":12,"./modules/sticky-menu":13,"./modules/toggle-menu":14,"./settings":15}],2:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    selector: '.js-advantages-slider',
    itemSelector: '.js-advantages-slider__item',
    prevSelector: '.js-advantages-slider__prev',
    nextSelector: '.js-advantages-slider__next',
    slider: {
        duration: 400,
        loop: true
    }
};

var AdvantagesSlider = function () {
    function AdvantagesSlider(options) {
        _classCallCheck(this, AdvantagesSlider);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(AdvantagesSlider, [{
        key: 'init',
        value: function init() {
            console.log('[ AdvantagesSlider/init ]');

            var $block = $(this.opt.selector);
            if ($block.length) {
                var $items = $block.find(this.opt.itemSelector),
                    $prev = $block.find(this.opt.prevSelector),
                    $next = $block.find(this.opt.nextSelector);

                $items.scroller($.extend({}, this.opt.slider, {
                    prev: $prev,
                    next: $next
                }));
            }
        }
    }]);

    return AdvantagesSlider;
}();

exports.AdvantagesSlider = AdvantagesSlider;

},{}],3:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.AjaxForm = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ajax = require('utils/ajax');

var _settings = require('settings');

var _form = require('./ajax-form/form');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    formSelector: '.js-ajax-form__form',
    linkSelector: '.js-ajax-form__link',
    fancybox: {},
    form: {}
};

var AjaxForm = function () {
    function AjaxForm(options) {
        _classCallCheck(this, AjaxForm);

        this.opt = $.extend(true, {}, settings, options);
        this.init();
    }

    _createClass(AjaxForm, [{
        key: 'init',
        value: function init() {
            var _this = this;

            console.log('[ AjaxForm/init ]');

            var self = this;

            $(document.body).on('click', this.opt.linkSelector, function (e) {
                e.preventDefault();

                var $el = $(this);
                self.getForm($.extend({}, { href: $el.attr('href') }, $el.data()));
            });

            $(this.opt.formSelector).each(function () {
                if (!this.__ajax_form) {
                    this.__ajax_form = new _form.Form(this, self.opt.form);
                }
            });

            $(window).on('main:initAjaxForm', function () {
                $(_this.opt.formSelector).each(function () {
                    if (!this.__ajax_form) {
                        this.__ajax_form = new _form.Form(this, self.opt.form);
                    }
                });
            });

            $(window).on('ajaxForm:form:goal', function (e, data) {
                $(window).trigger('ajaxForm:goal', data);
            });
        }
    }, {
        key: 'getForm',
        value: function getForm(params) {
            var _this2 = this;

            console.log('[ AjaxForm/getForm ]', params);

            _ajax.Ajax.get('get_form', params).then(function (data) {
                if (data.ok) {
                    _this2.showForm(data.content);
                } else {
                    console.error('Can\'t get form');
                }
            }).catch(function (error) {
                throw new Error(error);
            });
        }
    }, {
        key: 'showForm',
        value: function showForm(content) {
            console.log('[ AjaxForm/showForm ]');

            var self = this;

            $.fancybox($.extend(true, {}, _settings.fancybox, this.opt.fancybox, {
                type: 'html',
                content: content,
                afterShow: function afterShow() {
                    var form = this.inner.find(self.opt.formSelector).get(0);
                    form.__ajax_form = new _form.Form(form, self.opt.form);
                }
            }));
        }
    }]);

    return AjaxForm;
}();

exports.AjaxForm = AjaxForm;

},{"./ajax-form/form":4,"settings":15,"utils/ajax":16}],4:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Form = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ajax = require('utils/ajax');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    fieldSelector: '.js-ajax-form__field',
    fieldNamePrefix: '.js-ajax-form__field--',
    contentSelector: '.js-ajax-form__content',
    errorClass: 'is-error'
};

var Form = function () {
    function Form(el, options) {
        _classCallCheck(this, Form);

        this.el = el;
        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(Form, [{
        key: 'init',
        value: function init() {
            console.log('[ Form/init ]', this.el);

            this.$form = $(this.el);
            this.$fields = this.opt.$fields || $(this.opt.fieldSelector, this.el);
            this.$content = this.opt.$content || $(this.opt.contentSelector).filter('[data-form_id="' + this.$form.data('form_id') + '"]');

            this._initFields();
            this._initEvents();

            if ($.fancybox.isOpen) {
                $.fancybox.update();
            }
        }
    }, {
        key: 'submit',
        value: function submit() {
            console.log('[ Form/submit ]', this.el);

            if (!this.isLoading) {
                this._beforeSubmit();
                this._processSubmit();
                this._afterSubmit();
            }
        }
    }, {
        key: '_initFields',
        value: function _initFields() {
            this.$fields.filter(this.opt.fieldNamePrefix + 'captcha').remove();
        }
    }, {
        key: '_initEvents',
        value: function _initEvents() {
            var _this = this;

            this.$form.on('submit', function (e) {
                e.preventDefault();
                _this.submit();
            });
        }
    }, {
        key: '_beforeSubmit',
        value: function _beforeSubmit() {
            this.isLoading = true;

            this.$fields.removeClass(this.opt.errorClass);

            $.fancybox.showLoading();
        }
    }, {
        key: '_processSubmit',
        value: function _processSubmit() {
            var _this2 = this;

            var params = new FormData(this.el);

            _ajax.Ajax.get('send_form', params).then(function (data) {
                if (data.ok) {
                    console.log('[ Form/_processSubmit ] Sent successfully');

                    if (_this2.$form.data('goal')) {
                        $(window).trigger('ajaxForm:form:goal', _this2.$form.data());
                    }

                    if (_this2.$content.length) {
                        _this2.$content.html(data.content);
                        _this2.$form.remove();
                    } else {
                        _this2.$form.replaceWith(data.content);
                    }
                    _this2._afterSubmit();
                } else if (data.errors) {
                    console.log('[ Form/_processSubmit ] Got errors', data.errors);

                    for (var p in data.errors) {
                        _this2.$fields.filter(_this2.opt.fieldNamePrefix + p).addClass(_this2.opt.errorClass);
                    }
                    _this2._afterSubmit();
                }
            }).catch(function (error) {
                console.error('[ Form/_processSubmit ] Sending failed', error);

                this._afterSubmit();
            });
        }
    }, {
        key: '_afterSubmit',
        value: function _afterSubmit() {
            $.fancybox.hideLoading();

            if ($.fancybox.isOpen) {
                $.fancybox.update();
            }

            this.isLoading = false;
        }
    }]);

    return Form;
}();

exports.Form = Form;

},{"utils/ajax":16}],5:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Goal = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _counters = require('utils/counters');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Goal = function () {
    function Goal(options) {
        _classCallCheck(this, Goal);

        this.opt = $.extend({}, {
            goals: [],
            counters: _counters.counters
        }, options);
        this.init();
    }

    _createClass(Goal, [{
        key: 'init',
        value: function init() {
            var _this = this;

            console.log('[ Goal/init ]');

            this.opt.goals.forEach(function (goal) {
                _this.bind(goal);
            });
            $(window).on('main:reachGoal', function (e, data) {
                _this.reach(data.goal, data.params);
            });
        }
    }, {
        key: 'bind',
        value: function bind(goal) {
            var _this2 = this;

            var handler = function handler(e, data) {
                if (goal.goal) {
                    _this2.reach(goal.goal, goal.params);
                } else if (goal.handler) {
                    goal.handler.call(e.target, e, data);
                } else {
                    throw new Error('Neither goal nor handler are defined');
                }
            };

            if (this.__selectorIsDelegator(goal.selector)) {
                this.$body.on(goal.event, goal.selector, handler);
            } else {
                $(goal.selector).on(goal.event, handler);
            }
        }
    }, {
        key: 'reach',
        value: function reach(goal, params) {
            console.log('[ Goal/reach ] ' + goal);

            this.opt.counters.forEach(function (counter) {
                counter.reachGoal(goal, params);
            });
        }
    }, {
        key: '__selectorIsDelegator',
        value: function __selectorIsDelegator(selector) {
            return [window, document, document.documentElement, document.body, 'html', 'body'].indexOf(selector) === -1;
        }
    }, {
        key: '$body',
        get: function get() {
            if (!this.__$body) {
                this.__$body = $(document.body);
            }
            return this.__$body;
        }
    }]);

    return Goal;
}();

exports.Goal = Goal;

},{"utils/counters":17}],6:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    selector: '.js-home-slider',
    itemSelector: '.js-home-slider__item',
    navSelector: '.js-home-slider__nav',
    linkSelector: '.js-home-slider__link',
    prevSelector: '.js-home-slider__prev',
    nextSelector: '.js-home-slider__next',
    slider: {
        duration: 400,
        autoChange: true,
        delay: 5000,
        loop: true
    }
};

var HomeSlider = function () {
    function HomeSlider(options) {
        _classCallCheck(this, HomeSlider);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(HomeSlider, [{
        key: 'init',
        value: function init() {
            console.log('[ HomeSlider/init ]');

            var $block = $(this.opt.selector);
            if ($block.length) {
                var $items = $block.find(this.opt.itemSelector),
                    $nav = $block.find(this.opt.navSelector),
                    $links = $block.find(this.opt.linkSelector),
                    $prev = $block.find(this.opt.prevSelector),
                    $next = $block.find(this.opt.nextSelector);

                $items.slider($.extend({}, this.opt.slider, {
                    nav: $links,
                    prev: $prev,
                    next: $next
                }));
            }
        }
    }]);

    return HomeSlider;
}();

exports.HomeSlider = HomeSlider;

},{}],7:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.LocationSelect = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ajax = require('../utils/ajax');

var _settings = require('../settings');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    linkSelector: '.js-location-select__link',
    citySelector: '.js-location-select__city',
    fancybox: {}
};

var LocationSelect = function () {
    function LocationSelect(options) {
        _classCallCheck(this, LocationSelect);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(LocationSelect, [{
        key: 'init',
        value: function init() {
            var _this = this;

            console.log('[ LocationSelect/init ]');

            var $link = $(this.opt.linkSelector);

            $link.on('click', function (e) {
                e.preventDefault();
                _this.open();
            });
        }
    }, {
        key: 'open',
        value: function open() {
            var _this2 = this;

            console.log('CitySelect/open');

            _ajax.Ajax.get('get_location_select').then(function (data) {
                if (data.ok) {
                    _this2.show(data.content);
                } else {
                    throw new Error('Cannot get location select');
                }
            }).catch(function (error) {
                throw error;
            });
        }
    }, {
        key: 'show',
        value: function show(content) {
            console.log('CitySelect/show');

            var self = this;

            $.fancybox($.extend(true, {}, _settings.fancybox, this.opt.fancybox, {
                type: 'html',
                content: content,
                afterShow: function afterShow() {
                    this.inner.find(self.opt.citySelector).click(function (e) {
                        e.preventDefault();
                        self.select($(this).data()).then(function (data) {
                            $.fancybox.close();
                        }).catch(function (error) {
                            throw error;
                        });
                    });
                }
            }));
        }
    }, {
        key: 'select',
        value: function select(data) {
            return _ajax.Ajax.get('save_location', data).then(function (data) {
                if (data.ok) {
                    document.location.reload();
                    return data;
                } else {
                    throw new Error("Cannot save location select");
                }
            });
        }
    }]);

    return LocationSelect;
}();

exports.LocationSelect = LocationSelect;

},{"../settings":15,"../utils/ajax":16}],8:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    mapSelector: '.js-lombard__map',
    map: {
        zoom: 16,
        controls: ['default']
    },
    ymapsScriptSrc: 'https://api-maps.yandex.ru/2.1/?lang=ru_RU'
};

var LombardMap = function () {
    function LombardMap(options) {
        _classCallCheck(this, LombardMap);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(LombardMap, [{
        key: 'init',
        value: function init() {
            var _this = this;

            console.log('[ LombardMap/init ]');

            this.$map = $(this.opt.mapSelector);
            if (this.$map.length) {
                this.coords = this.$map.data('coords').split(/[^0-9.\-\+]+/);
                this.opt.map.center = this.coords;

                $.getScript(this.opt.ymapsScriptSrc).done(function () {
                    ymaps.ready(function () {
                        _this.initMap();
                    });
                }).fail(function () {
                    throw new Error('Can\'t load script ' + _this.opt.ymapsScriptSrc);
                });
            }
        }
    }, {
        key: 'initMap',
        value: function initMap() {
            var map = new ymaps.Map(this.$map.get(0), this.opt.map);

            var placemark = new ymaps.Placemark(this.opt.map.center, {}, {
                iconLayout: 'default#image',
                iconImageHref: '/i/icons/icon_circle.svg',
                iconImageSize: [24, 24]
            });
            map.geoObjects.add(placemark);

            map.behaviors.disable('scrollZoom');
        }
    }]);

    return LombardMap;
}();

exports.LombardMap = LombardMap;

},{}],9:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.PersonalOffice = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ajax = require('utils/ajax');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    rootWrap: '.js-po-wrap'
};
var $wrap = void 0;

var PersonalOffice = function () {
    function PersonalOffice(options) {
        _classCallCheck(this, PersonalOffice);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(PersonalOffice, [{
        key: 'init',
        value: function init() {
            console.log('[ PersonalOffice/init ]');
            $wrap = $(this.opt.rootWrap);
            this.getLogin();
        }
    }, {
        key: 'getLogin',
        value: function getLogin() {
            var _this = this;

            var self = this;
            var loginTemplate = '<div class="personal-office__toptext personal-office__paragraph">' + '  <h1 class="h1 m--block-yellow personal-office__paragraph">Войти в личный кабинет</h1>' + '</div>' + '<div class="personal-office__toptext personal-office__linksbottom personal-office__paragraph">' + '   <a id="registration" class="personal-office__link" href="#">Регистрация</a>' + '   <a id="recall-password" class="personal-office__link" href="#">Забыли пароль</a>' + '</div>' + '<div class="personal-office__height" >' + '   <div class="personal-office__textblock personal-office__offset">' + '       <div class="filter__subblock-header">В Личном кабинете:</div>' + '       <div class="news__item-preview "><p>' + '           &#8212; Вся информация по каждому вашему займу (действующим и погашенным залоговым билетам)<br>' + '           &#8212; Ваш статус лояльности<br>' + '           &#8212; Личные предложения<br>' + '           &#8212; Возможность пермонально обратиться в службу поддержки' + '       </p></div>' + '   </div>' + '   <div class="personal-office__fieldwidth">' + '      <form class="personal-office__form">' + '        <input type="hidden" value="Authenticate" name="request" />' + '        <div class="personal-office__offset">' + '           <label >' + '              <input name="phone" class="personal-office__field  personal-office__fieldwidth" placeholder="Введите номер телефона" />' + '           </label>' + '        </div>' + '        <div class="personal-office__offset">' + '           <label >' + '              <input name="iin" class="personal-office__field  personal-office__fieldwidth" placeholder="Введите номер ИИН" />' + '           </label>' + '        </div>' + '	     <div class="personal-office__offset">' + '		    <label>' + '			    <input name="password" type="password" class="personal-office__field  personal-office__fieldwidth"  placeholder="Введите Ваш пароль" /> ' + '               <div class="personal-office__icon"></div>' + '		    </label>' + '	     </div>' + '	     <div class = "personal-office__fieldwidth">' + '		    <button class="personal-office__button personal-office__center">ВОЙТИ</button>' + '	     </div>' + '      </form>' + '   </div>' + '</div>';

            var tmplt = _.template(loginTemplate);
            console.log(tmplt);
            $wrap.html(tmplt());
            $wrap.on('click', '#registration', function (e) {
                self.registration();
                e.preventDefault();
            });
            var $form = $wrap.find('form');
            $form.submit(function (e) {
                e.preventDefault();
                var params = $form.serializeObject();
                console.log('Before sendRequest', params);
                _this.sendRequest(params, function (data) {
                    _this.processAuthenticate(data);
                });
            });
        }
    }, {
        key: 'registration',
        value: function registration() {
            var _this2 = this;

            //		let $wrap = $(this.opt.rootWrap);
            var registrationTemplate = '<div class="personal-office__breadcrumbs">' + '   <a href="/" class="breadcrumbs__link">Главная страница></a>' + '   <a class="breadcrumbs__item  is-active" >Личный кабинет</a>' + '</div>' + '<div class="personal-office__toptext personal-office__paragraph">' + '  <h1 class="h1 m--block-yellow">Регистрация кабинета</h1>' + '</div>' + '<div class="personal-office__toptext personal-office__linksbottom">' + '  <a id="registration" class="personal-office__link" href="#">Вход в личный кабинет</a>' + '</div>' + '<div class="personal-office__height personal-office__offset" >' + '  <form class = "personal-office__form">' + '	 <input type="hidden" value="Register" name="request" />' + '	 <div class="personal-office__offset">' + '         <label >' + '            <input name="iin" class="personal-office__field  personal-office__fieldwidth" placeholder = "Введите номер ИНН" />' + '         </label>' + '         <div class="personal-office__warningincorrect  personal-office__warningdisplay">Некорректный номер ИНН</div>' + '	 </div>' + '    <div class="personal-office__offset">' + '         <label >' + '            <input name="phone" class="personal-office__field  personal-office__fieldwidth"  placeholder = "Введите номер Вашего телефона" />' + '         </label>' + '         <div class="personal-office__warningincorrect   personal-office__warningdisplay" >Некорректный номер телефона</div>' + '    </div>' + '    <div class = "personal-office__fieldwidth">' + '          <button class="personal-office__button  personal-office__center">Отправить данные</button>' + '    </div>' + '</form> ' + '</div>';
            var tmplt = _.template(registrationTemplate);
            $wrap.html(tmplt());

            // $wrap.on('register:sendCode', (e, data) => {
            // 	this.processRegisterCode( data );
            // });

            var $form = $wrap.find('form');
            $form.submit(function (e) {
                e.preventDefault();
                var params = $form.serializeObject();
                console.log('Before sendRequest', params);
                _this2.sendRequest(params, function (data) {
                    _this2.processRegisterCode(data);
                });
            });
        }
    }, {
        key: 'getCheckCode',
        value: function getCheckCode(params) {
            var _this3 = this;

            console.log('[ PersonalOffice/getCheckCode ]', params);

            var template = '<div class="personal-office__breadcrumbs">' + '   <a href="/" class="breadcrumbs__link">Главная страница></a>' + '   <a class="breadcrumbs__item  is-active" >Личный кабинет</a>' + '</div>' + '<div class="personal-office__toptext">' + '   <h1 class="h1 m--block-yellow">Введите код из СМС</h1>' + '</div>' + '<div class="personal-office__height" >' + '<div class="personal-office__textblock personal-office__fieldwidth personal-office__offset" ><p>' + '   На ваш номер мобильного телефона отправлено СМС-уведомление с кодом для активации заявки регистрации личного кабинета.<br>' + '   Для продолжения регистрации введите код из СМС в поле и нажмите кнопку "Далее"' + '</p></div>' + '   <div class="personal-office__fieldwidth" >' + '       <form class="personal-office__form">' + '           <input type="hidden" value="Register" name="request" />' + '           <div class="personal-office__offset">' + '               <label >' + '                   <input class="personal-office__field" name="code"  placeholder = "Код из СМС" />' + '               </label>' + '           </div>' + '           <div class="personal-office__offset">' + '               <label>' + '                   <input name="password" type="password" class="personal-office__field personal-office__fieldwidth" placeholder="Придумайте пароль" /> ' + '                   <div class="personal-office__icon"></div>' + '               </label>' + '           </div>' + '           <div class="personal-office__offset">' + '               <label>' + '                   <input name="password_dbl" type="password" class="personal-office__field  personal-office__fieldwidth" placeholder = "Повторите" /> ' + '                   <div class="personal-office__icon"></div>' + '               </label>' + '           </div>' + '           <div class = "personal-office__fieldwidth">' + '               <button class="personal-office__button personal-office__center">Далее</button>' + '           </div>' + '       </form>' + '   </div>' + '</div>';
            var tmplt = _.template(template);
            $wrap.html(tmplt());
            var $form = $wrap.find('form');
            $form.submit(function (e) {
                e.preventDefault();
                var params = $form.serializeObject();
                // console.log(params);
                _this3.sendRequest(params, function (data) {
                    _this3.processRegisterCode(data);
                });
            });
        }
    }, {
        key: 'registerSuccess',
        value: function registerSuccess() {
            var _this4 = this;

            console.log('[ PersonalOffice/registerSuccess ]');
            var template = '<p>' + '    Вы успешно зарегистрировались в личном кабинете' + '</p>' + '<form>' + '   <div class = "personal-office__fieldwidth">' + '       <button class="personal-office__button personal-office__center">Далее</button>' + '   </div>' + '</form>';
            var tmplt = _.template(template);
            $wrap.html(tmplt());
            var $form = $wrap.find('form');
            $form.submit(function (e) {
                e.preventDefault();
                _this4.getLogin();
            });
        }
    }, {
        key: 'processRegisterCode',
        value: function processRegisterCode(data) {
            console.log('[ PersonalOffice/processRegisterCode ]', data);
            switch (data.ok) {
                case "0":
                    console.log('Нет ошибки');
                    if (data.type == 1) {
                        this.getCheckCode('');
                    } else {
                        this.registerSuccess();
                    }
                    break;
                case "1":
                    console.log('Ошибка...');
                    break;
                case "2":
                    console.log('Неверный код');
                    break;
                case "3":
                    console.log('Ошибка в ИНН');
                    break;
                case "4":
                    console.log('Ошибка в номере телефона');
                    break;
                default:
                    console.error('Error in request');
                    break;
            }
        }
    }, {
        key: 'processAuthenticate',
        value: function processAuthenticate(data) {
            console.log('[ PersonalOffice/processAuthenticate ]', data);
            var self = this;
            switch (data.ok) {
                case "0":
                    console.log('Операция выполнена успешно');
                    var params = { request: 'GetTickets' };
                    this.sendRequest(params, function (data) {
                        self.processGetTickets(data);
                    });
                    break;
                case "1":
                    console.log('Неверный пароль');
                    break;
                case "2":
                    console.log('Клиент не зарегистрирован');
                    break;
            }
        }
    }, {
        key: 'processGetTickets',
        value: function processGetTickets(data) {
            console.log('[ PersonalOffice/processGetTickets ]', data);
            var len = data.Credits.length;
            console.log(len);
            var template = '<div class="loans__table">' + '   <div class="loans__row">' + '       <div class="loans__general-cell-active">Номер билета</div>' + '       <div class="loans__general-cell-active-even">Статус билета</div>' + '       <div class="loans__general-cell-active">Дата окончания займа</div>' + '       <div class="loans__general-cell-active-even">Осталось дней</div>' + '       <div class="loans__general-cell-active">Сумма займа, т</div>' + '       <div class="loans__general-cell-active-even">Процент займа</div>' + '       <div class="loans__general-cell-active">Общая сумма задолженности</div>' + '       <div class="loans__general-cell-active-even">&nbsp;</div>' + '   </div>' + '   <% items.forEach(function(item) { %>' + '       <div class="loans__row">' + '           <div class="loans__cell-active">' + '               <div class="loans__cell-active__block">' + '                   <span class="loans__number-active"><%= item.Ticket %></span>' + '               </div>' + '           </div>' + '           <div class="loans__cell-active-even">' + '                <div class="loans__cell-active-even__block">' + '                   <%= item.Status %>' + '                </div>' + '           </div>' + '           <div class="loans__cell-active">' + '                <div class="loans__cell-active__block">' + '                &nbsp;</div>' + '           </div>' + '           <div class="loans__cell-active-even">' + '                <div class="loans__cell-active-even__block">' + '                &nbsp;</div>' + '           </div>' + '           <div class="loans__cell-active">' + '                <div class="loans__cell-active__block">' + '                &nbsp;</div>' + '           </div>' + '           <div class="loans__cell-active-even">' + '                <div class="loans__cell-active-even__block">' + '                &nbsp;</div>' + '           </div>' + '           <div class="loans__cell-active">' + '                <div class="loans__cell-active__block">' + '                &nbsp;</div>' + '           </div>' + '           <div class="loans__cell-active-even">' + '                <div class="loans__cell-active-even__block">' + '                &nbsp;</div>' + '           </div>' + '       </div>' + '   <% }); %>' + '</div>';
            var tmplt = _.template(template);
            $wrap.html(tmplt({ items: data.Credits }));
        }
    }, {
        key: 'sendRequest',
        value: function sendRequest(params, resolve) {
            console.log('[ PersonalOffice/sendRequest ]', params);

            _ajax.Ajax.get(params.request, params).then(function (data) {
                resolve(data);
            }).catch(function (error) {
                throw new Error(error);
            });
        }
    }]);

    return PersonalOffice;
}();

exports.PersonalOffice = PersonalOffice;

},{"utils/ajax":16}],10:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    sliderSelector: '.js-product-gallery-slider',
    sliderContainerSelector: '.js-product-gallery-slider-container',
    sliderItemSelector: '.js-product-gallery-slider-item',
    sliderPrevSelector: '.js-product-gallery-slider-prev',
    sliderNextSelector: '.js-product-gallery-slider-next',
    sliderNavSelector: '.js-product-gallery-slider-nav',
    sliderOptions: {
        duration: 400,
        loop: true
    },
    scrollerSelector: '.js-product-gallery-scroller',
    scrollerContainerSelector: '.js-product-gallery-scroller-container',
    scrollerItemSelector: '.js-product-gallery-scroller-item',
    scrollerOptions: {
        duration: 200,
        hideArrs: true
    }
};

var ProductGallery = function () {
    function ProductGallery(options) {
        _classCallCheck(this, ProductGallery);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(ProductGallery, [{
        key: 'init',
        value: function init() {
            console.log('[ ProductGallery/init ]');

            var $slider = $(this.opt.sliderSelector);
            if ($slider.length) {
                var $container = $slider.find(this.opt.sliderContainerSelector),
                    $items = $slider.find(this.opt.sliderItemSelector),
                    $prev = $slider.find(this.opt.sliderPrevSelector),
                    $next = $slider.find(this.opt.sliderNextSelector),
                    $nav = $slider.find(this.opt.sliderNavSelector);

                $items.slider($.extend({}, this.opt.sliderOptions, {
                    container: $container,
                    prev: $prev,
                    next: $next,
                    nav: $nav
                }));
            }

            var $scroller = $(this.opt.scrollerSelector);
            if ($scroller.length) {
                var _$container = $scroller.find(this.opt.scrollerContainerSelector),
                    _$items = $scroller.find(this.opt.scrollerItemSelector);

                _$items.scroller($.extend({}, this.opt.scrollerOptions, {
                    container: _$container
                }));
            }
        }
    }]);

    return ProductGallery;
}();

exports.ProductGallery = ProductGallery;

},{}],11:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    selector: '.js-products-slider',
    itemSelector: '.js-products-slider__item',
    prevSelector: '.js-products-slider__prev',
    nextSelector: '.js-products-slider__next',
    slider: {
        duration: 400,
        loop: true
    }
};

var ProductsSlider = function () {
    function ProductsSlider(options) {
        _classCallCheck(this, ProductsSlider);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(ProductsSlider, [{
        key: 'init',
        value: function init() {
            console.log('[ ProductsSlider/init ]');

            var $blocks = $(this.opt.selector);

            for (var i = 0; i < $blocks.length; i++) {
                var $block = $blocks.eq(i);

                var $items = $block.find(this.opt.itemSelector),
                    $prev = $block.find(this.opt.prevSelector),
                    $next = $block.find(this.opt.nextSelector);

                $items.scroller($.extend({}, this.opt.slider, {
                    prev: $prev,
                    next: $next
                }));
            }
        }
    }]);

    return ProductsSlider;
}();

exports.ProductsSlider = ProductsSlider;

},{}],12:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Responsive = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _settings = require('settings');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Responsive = function () {
    function Responsive(options) {
        _classCallCheck(this, Responsive);

        this.opt = $.extend({}, { breakpoints: _settings.breakpoints }, options);
        this.currentPoint = null;
        this.init();
    }

    _createClass(Responsive, [{
        key: 'init',
        value: function init() {
            var _this = this;

            console.log('[ Responsive ] init');

            this.$window.on('resize', function () {
                _this._watchSize();
            });
            this._watchSize();
        }
    }, {
        key: '_watchSize',
        value: function _watchSize() {
            var width = this.getWidth(),
                point = this.getPoint(width);

            if (!this.currentPoint || point !== this.currentPoint) {
                if (this.currentPoint) {
                    console.log('[ Responsive ] Off: ' + this.currentPoint);
                    this.$window.trigger('responsive:off:' + this.currentPoint);
                }

                console.log('[ Responsive ] On: ' + point);
                this.$window.trigger('responsive:on:' + point);

                if (this.currentPoint) {
                    this.$window.trigger('responsive:change', { point: point });
                } else {
                    this.$window.trigger('responsive:init', { point: point });
                }

                this.currentPoint = point;
            }
        }
    }, {
        key: 'getWidth',
        value: function getWidth() {
            return this.$window.width();
        }
    }, {
        key: 'getPoint',
        value: function getPoint(width) {
            var point = this.points[0];
            for (var i = this.points.length - 1; i > 0; i--) {
                if (width > this.opt.breakpoints[this.points[i - 1]] && width <= this.opt.breakpoints[this.points[i]]) {
                    point = this.points[i];
                    break;
                }
            }
            return point;
        }
    }, {
        key: 'points',
        get: function get() {
            var _this2 = this;

            if (!this.__points) {
                this.__points = [];
                for (var k in this.opt.breakpoints) {
                    this.__points.push(k);
                }
                this.__points.sort(function (a, b) {
                    return _this2.opt.breakpoints[a] < _this2.opt.breakpoints[b] ? -1 : _this2.opt.breakpoints[a] > _this2.opt.breakpoints[b] ? 1 : 0;
                });
            }
            return this.__points;
        }
    }, {
        key: '$window',
        get: function get() {
            if (!this.__$window) {
                this.__$window = $(window);
            }
            return this.__$window;
        }
    }]);

    return Responsive;
}();

exports.Responsive = Responsive;

},{"settings":15}],13:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    stickyMenuSelector: '.js-sticky-menu',
    mainMenuSelector: '.js-main-menu'
};

var StickyMenu = function () {
    function StickyMenu(options) {
        _classCallCheck(this, StickyMenu);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(StickyMenu, [{
        key: 'init',
        value: function init() {
            console.log('[ StickyMenu/init ]');

            /*let $stickyMenu = $(this.opt.stickyMenuSelector),
                $mainMenu = $(this.opt.mainMenuSelector);*/

            var scrollIntervalID = setInterval(this.stickIt, 10);
        }
    }, {
        key: 'stickIt',
        value: function stickIt() {
            var $stickyMenu = $('.js-sticky-menu'),
                $mainMenu = $('.js-main-menu');

            var originalMenuPos = $mainMenu.offset(),
                originalMenuTop = originalMenuPos.top;

            if ($(window).scrollTop() >= originalMenuTop) {
                $mainMenu.css('visibility', 'hidden');
                $stickyMenu.removeClass('is-hidden');
            } else {
                $stickyMenu.addClass('is-hidden');
                $mainMenu.css('visibility', 'visible');
            }
        }
    }]);

    return StickyMenu;
}();

exports.StickyMenu = StickyMenu;

},{}],14:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var settings = {
    listSelector: '.js-toggle-menu__list',
    itemSelector: '.js-toggle-menu__item',
    sublistSelector: '.js-toggle-menu__sublist',
    triggerSelector: '.js-toggle-menu__trigger'
};

var ToggleMenu = function () {
    function ToggleMenu(options) {
        _classCallCheck(this, ToggleMenu);

        this.opt = $.extend({}, settings, options);
        this.init();
    }

    _createClass(ToggleMenu, [{
        key: 'init',
        value: function init() {
            console.log('[ ToggleMenu/init ]');

            var $trigger = $(this.opt.triggerSelector),
                $list = $(this.opt.listSelector),
                $item = $(this.opt.itemSelector);

            $trigger.on('click', function (e) {
                $list.toggleClass('is-opened');
                $trigger.toggleClass('is-active');
            });
        }
    }]);

    return ToggleMenu;
}();

exports.ToggleMenu = ToggleMenu;

},{}],15:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
function __detectSessionID() {
    var sessionId = '';
    if (window.location.toString().match(/SESS_ID=(\d+)/)) {
        sessionId = RegExp.$1;
    }
    return sessionId;
}

var fancybox = {};

var ajax = {
    type: 'post',
    url: '/json/',
    dataType: 'json',
    timeout: 10000,
    traditional: true,
    SESSION_ID: __detectSessionID()
};

var yaCounter = 'yaCounter000000';

var breakpoints = {
    xxl: Infinity,
    xl: 1620,
    l: 1280,
    m: 1000,
    s: 780,
    xs: 600,
    xxs: 420
};

var interfaceName = document.body.dataset['interface'];

exports.fancybox = fancybox;
exports.ajax = ajax;
exports.breakpoints = breakpoints;
exports.yaCounter = yaCounter;
exports.interfaceName = interfaceName;

},{}],16:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Ajax = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _settings = require('settings');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Ajax = function () {
    function Ajax() {
        _classCallCheck(this, Ajax);
    }

    _createClass(Ajax, null, [{
        key: 'get',
        value: function get(act, data, options) {
            console.log('[ Ajax/get ] ' + act, data);

            var opt = $.extend({}, _settings.ajax, options);

            if (opt.SESSION_ID) {
                if (data instanceof Array) {
                    data.push({ name: 'SESS_ID', value: opt.SESSION_ID });
                } else if (data instanceof FormData) {
                    data.append('SESS_ID', opt.SESSION_ID);
                } else if (data instanceof URLSearchParams) {
                    data.append('SESS_ID', opt.SESSION_ID);
                } else if (typeof data === 'string') {
                    if (data != '') data += '&';
                    data += 'SESS_ID=' + opt.SESSION_ID;
                } else if ((typeof data === 'undefined' ? 'undefined' : _typeof(data)) === 'object') {
                    data.SESS_ID = opt.SESSION_ID;
                } else {
                    throw new Error('Can\'t append SESSION_ID to this type of data');
                }
            }
            delete opt.SESSION_ID;

            if (!/^\//.test(act)) {
                opt.url += act;
            } else {
                opt.url = act;
            }
            if (!/\/$/.test(opt.url)) {
                opt.url += '/';
            }

            if (data instanceof FormData || data instanceof URLSearchParams) {
                opt.contentType = false;
                opt.processData = false;
            }

            opt.data = data;

            var handler = new Promise(function (resolve, reject) {
                opt.success = function (data, textStatus, jqXHR) {
                    resolve(data, jqXHR);
                };
                opt.error = function (jqXHR) {
                    reject(jqXHR.statusText, jqXHR);
                };
                $.ajax(opt);
            });

            return handler;
        }
    }]);

    return Ajax;
}();

exports.Ajax = Ajax;

},{"settings":15}],17:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.counters = undefined;

var _yandexMetrika = require('./counters/yandex-metrika');

var _googleAnalytics = require('./counters/google-analytics');

var counters = [new _yandexMetrika.YandexMetrika(), new _googleAnalytics.GoogleAnalytics()];

exports.counters = counters;

},{"./counters/google-analytics":19,"./counters/yandex-metrika":20}],18:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AbstractCounter = function () {
    function AbstractCounter(options) {
        _classCallCheck(this, AbstractCounter);

        this.opt = options;
    }

    _createClass(AbstractCounter, [{
        key: 'reachGoal',
        value: function reachGoal() {
            throw new Error('You must define your own method');
        }
    }, {
        key: 'type',
        get: function get() {
            throw new Error('You must define your own method');
        }
    }, {
        key: 'counter',
        get: function get() {
            if (!this.__counter) {
                this.__counter = this.opt.counter || window[this.opt.counterName];
            }
            return this.__counter;
        }
    }]);

    return AbstractCounter;
}();

exports.AbstractCounter = AbstractCounter;

},{}],19:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.GoogleAnalytics = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _abstractCounter = require('./abstract-counter');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GoogleAnalytics = function (_AbstractCounter) {
    _inherits(GoogleAnalytics, _AbstractCounter);

    function GoogleAnalytics(options) {
        _classCallCheck(this, GoogleAnalytics);

        return _possibleConstructorReturn(this, (GoogleAnalytics.__proto__ || Object.getPrototypeOf(GoogleAnalytics)).call(this, $.extend({}, { counterName: 'ga' }, options)));
    }

    _createClass(GoogleAnalytics, [{
        key: 'reachGoal',
        value: function reachGoal(goal) {
            var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            console.log('[ GoogleAnalytics/reachGoal ] ' + goal, params);

            try {
                this.counter('send', 'event', goal, params);
                console.info('[ GoogleAnalytics ] Goal ' + goal + ' is reached successfully');
            } catch (err) {
                console.error('[ GoogleAnalytics ] Can\'t reach goal ' + goal, err);
            }
        }
    }, {
        key: 'setParams',
        value: function setParams(params) {
            console.log('[ GoogleAnalytics/setParams ]', params);

            try {
                this.counter('set', params);
                console.info('[ GoogleAnalytics ] Params are set successfully');
            } catch (err) {
                console.error('[ GoogleAnalytics ] Can\'t set params', err);
            }
        }
    }, {
        key: 'type',
        get: function get() {
            return 'google-analytics';
        }
    }]);

    return GoogleAnalytics;
}(_abstractCounter.AbstractCounter);

exports.GoogleAnalytics = GoogleAnalytics;

},{"./abstract-counter":18}],20:[function(require,module,exports){
'use strict';

/* global $ */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.YandexMetrika = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _settings = require('settings');

var _abstractCounter = require('./abstract-counter');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var YandexMetrika = function (_AbstractCounter) {
    _inherits(YandexMetrika, _AbstractCounter);

    function YandexMetrika(options) {
        _classCallCheck(this, YandexMetrika);

        return _possibleConstructorReturn(this, (YandexMetrika.__proto__ || Object.getPrototypeOf(YandexMetrika)).call(this, $.extend({}, { counterName: _settings.yaCounter }, options)));
    }

    _createClass(YandexMetrika, [{
        key: 'reachGoal',
        value: function reachGoal(goal) {
            var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            console.log('[ YandexMetrika/reachGoal ] ' + goal, params);

            try {
                this.counter.reachGoal(goal, params);
                console.info('[ YandexMetrika ] Goal ' + goal + ' is reached successfully');
            } catch (err) {
                console.error('[ YandexMetrika ] Can\'t reach goal ' + goal, err);
            }
        }
    }, {
        key: 'setParams',
        value: function setParams(params) {
            console.log('[ YandexMetrika/setParams ]', params);

            try {
                this.counter.params(params);
                console.info('[ YandexMetrika ] Params are set successfully');
            } catch (err) {
                console.error('[ YandexMetrika ] Can\'t set params', err);
            }
        }
    }, {
        key: 'type',
        get: function get() {
            return 'yandex-metrika';
        }
    }]);

    return YandexMetrika;
}(_abstractCounter.AbstractCounter);

exports.YandexMetrika = YandexMetrika;

},{"./abstract-counter":18,"settings":15}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJqcy9zcmMvbWFpbi5qcyIsImpzL3NyYy9tb2R1bGVzL2FkdmFudGFnZXMtc2xpZGVyLmpzIiwianMvc3JjL21vZHVsZXMvYWpheC1mb3JtLmpzIiwianMvc3JjL21vZHVsZXMvYWpheC1mb3JtL2Zvcm0uanMiLCJqcy9zcmMvbW9kdWxlcy9nb2FsLmpzIiwianMvc3JjL21vZHVsZXMvaG9tZS1zbGlkZXIuanMiLCJqcy9zcmMvbW9kdWxlcy9sb2NhdGlvbi1zZWxlY3QuanMiLCJqcy9zcmMvbW9kdWxlcy9sb21iYXJkLW1hcC5qcyIsImpzL3NyYy9tb2R1bGVzL3BlcnNvbmFsLW9mZmljZS5qcyIsImpzL3NyYy9tb2R1bGVzL3Byb2R1Y3QtZ2FsbGVyeS5qcyIsImpzL3NyYy9tb2R1bGVzL3Byb2R1Y3RzLXNsaWRlci5qcyIsImpzL3NyYy9tb2R1bGVzL3Jlc3BvbnNpdmUuanMiLCJqcy9zcmMvbW9kdWxlcy9zdGlja3ktbWVudS5qcyIsImpzL3NyYy9tb2R1bGVzL3RvZ2dsZS1tZW51LmpzIiwianMvc3JjL3NldHRpbmdzLmpzIiwianMvc3JjL3V0aWxzL2FqYXguanMiLCJqcy9zcmMvdXRpbHMvY291bnRlcnMuanMiLCJqcy9zcmMvdXRpbHMvY291bnRlcnMvYWJzdHJhY3QtY291bnRlci5qcyIsImpzL3NyYy91dGlscy9jb3VudGVycy9nb29nbGUtYW5hbHl0aWNzLmpzIiwianMvc3JjL3V0aWxzL2NvdW50ZXJzL3lhbmRleC1tZXRyaWthLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7O0FBRUE7Ozs7QUFFQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7OztJQUVNLEk7QUFDRixvQkFBYztBQUFBOztBQUNWLGFBQUssSUFBTDtBQUNIOzs7OytCQUNNO0FBQ0gsaUJBQUssYUFBTCxHQUFxQix1QkFBckI7O0FBRUEsZ0JBQUksMkJBQWlCLFdBQXJCLEVBQWtDO0FBQzlCLHFCQUFLLFVBQUwsR0FBa0IsSUFBSSxzQkFBSixFQUFsQjtBQUNBLHFCQUFLLGdCQUFMLEdBQXdCLElBQUksa0NBQUosRUFBeEI7QUFDQSxxQkFBSyxVQUFMLEdBQWtCLElBQUksc0JBQUosRUFBbEI7QUFDSDs7QUFFRCxnQkFBSSwyQkFBaUIsU0FBckIsRUFBZ0M7QUFDNUIscUJBQUssVUFBTCxHQUFrQixJQUFJLHNCQUFKLEVBQWxCO0FBQ0g7QUFDRCxnQkFBSSwyQkFBaUIsaUJBQXJCLEVBQXdDO0FBQ3BDLHFCQUFLLGNBQUwsR0FBc0IsSUFBSSw4QkFBSixFQUF0QjtBQUNIO0FBQ0QsaUJBQUssVUFBTCxHQUFrQixJQUFJLHNCQUFKLEVBQWxCO0FBQ0EsaUJBQUssY0FBTCxHQUFzQixJQUFJLDhCQUFKLEVBQXRCO0FBQ0EsaUJBQUssY0FBTCxHQUFzQixJQUFJLDhCQUFKLEVBQXRCO0FBQ0EsaUJBQUssY0FBTCxHQUFzQixJQUFJLDhCQUFKLEVBQXRCOztBQUVBLGlCQUFLLFFBQUwsR0FBZ0IsSUFBSSxrQkFBSixFQUFoQjtBQUNBLGlCQUFLLElBQUwsR0FBWSxJQUFJLFVBQUosQ0FBUztBQUNqQix1QkFBTyxDQUNIO0FBQ0ksOEJBQVUsZ0JBRGQ7QUFFSSwyQkFBTyxPQUZYO0FBR0ksNkJBQVMsbUJBQVc7QUFDaEIsMEJBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsZ0JBQWxCLEVBQW9DLEVBQUUsSUFBRixFQUFRLElBQVIsRUFBcEM7QUFDSDtBQUxMLGlCQURHLEVBT0E7QUFDQyw4QkFBVSxnQkFEWDtBQUVDLDJCQUFPLFFBRlI7QUFHQyw2QkFBUyxtQkFBVztBQUNoQiwwQkFBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixnQkFBbEIsRUFBb0MsRUFBRSxJQUFGLEVBQVEsSUFBUixFQUFwQztBQUNIO0FBTEYsaUJBUEEsRUFhQTtBQUNDLDhCQUFVLE1BRFg7QUFFQywyQkFBTyxvQkFGUjtBQUdDLDZCQUFTLGlCQUFTLENBQVQsRUFBWSxJQUFaLEVBQWtCO0FBQ3ZCLDBCQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLGdCQUFsQixFQUFvQyxJQUFwQztBQUNIO0FBTEYsaUJBYkE7QUFEVSxhQUFULENBQVo7O0FBd0JBLGlCQUFLLFVBQUwsR0FBa0IsSUFBSSxzQkFBSixFQUFsQjtBQUVIOzs7Ozs7QUFHTCxFQUFFLFlBQVc7QUFDVCxXQUFPLElBQVAsR0FBYyxJQUFJLElBQUosRUFBZDtBQUNILENBRkQ7OztBQ3pFQTs7QUFFQTs7Ozs7Ozs7OztBQUVBLElBQUksV0FBVztBQUNYLGNBQVUsdUJBREM7QUFFWCxrQkFBYyw2QkFGSDtBQUdYLGtCQUFjLDZCQUhIO0FBSVgsa0JBQWMsNkJBSkg7QUFLWCxZQUFRO0FBQ0osa0JBQVUsR0FETjtBQUVKLGNBQU07QUFGRjtBQUxHLENBQWY7O0lBV00sZ0I7QUFDRiw4QkFBWSxPQUFaLEVBQXFCO0FBQUE7O0FBQ2pCLGFBQUssR0FBTCxHQUFXLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxRQUFiLEVBQXVCLE9BQXZCLENBQVg7QUFDQSxhQUFLLElBQUw7QUFDSDs7OzsrQkFDTTtBQUNILG9CQUFRLEdBQVIsQ0FBWSwyQkFBWjs7QUFFQSxnQkFBSSxTQUFTLEVBQUUsS0FBSyxHQUFMLENBQVMsUUFBWCxDQUFiO0FBQ0EsZ0JBQUksT0FBTyxNQUFYLEVBQW1CO0FBQ2Ysb0JBQUksU0FBUyxPQUFPLElBQVAsQ0FBWSxLQUFLLEdBQUwsQ0FBUyxZQUFyQixDQUFiO0FBQUEsb0JBQ0ksUUFBUSxPQUFPLElBQVAsQ0FBWSxLQUFLLEdBQUwsQ0FBUyxZQUFyQixDQURaO0FBQUEsb0JBRUksUUFBUSxPQUFPLElBQVAsQ0FBWSxLQUFLLEdBQUwsQ0FBUyxZQUFyQixDQUZaOztBQUlBLHVCQUFPLFFBQVAsQ0FBZ0IsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEtBQUssR0FBTCxDQUFTLE1BQXRCLEVBQThCO0FBQzFDLDBCQUFNLEtBRG9DO0FBRTFDLDBCQUFNO0FBRm9DLGlCQUE5QixDQUFoQjtBQUlIO0FBQ0o7Ozs7OztRQUlELGdCLEdBQUEsZ0I7OztBQ3RDSjs7QUFFQTs7Ozs7Ozs7O0FBRUE7O0FBQ0E7O0FBRUE7Ozs7QUFHQSxJQUFJLFdBQVc7QUFDWCxrQkFBYyxxQkFESDtBQUVYLGtCQUFjLHFCQUZIO0FBR1gsY0FBVSxFQUhDO0FBSVgsVUFBTTtBQUpLLENBQWY7O0lBUU0sUTtBQUNGLHNCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFDakIsYUFBSyxHQUFMLEdBQVcsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsUUFBbkIsRUFBNkIsT0FBN0IsQ0FBWDtBQUNBLGFBQUssSUFBTDtBQUNIOzs7OytCQUNNO0FBQUE7O0FBQ0gsb0JBQVEsR0FBUixDQUFZLG1CQUFaOztBQUVBLGdCQUFJLE9BQU8sSUFBWDs7QUFFQSxjQUFFLFNBQVMsSUFBWCxFQUFpQixFQUFqQixDQUFvQixPQUFwQixFQUE2QixLQUFLLEdBQUwsQ0FBUyxZQUF0QyxFQUFvRCxVQUFTLENBQVQsRUFBWTtBQUM1RCxrQkFBRSxjQUFGOztBQUVBLG9CQUFJLE1BQU0sRUFBRSxJQUFGLENBQVY7QUFDQSxxQkFBSyxPQUFMLENBQWEsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsTUFBTSxJQUFJLElBQUosQ0FBUyxNQUFULENBQVIsRUFBYixFQUF5QyxJQUFJLElBQUosRUFBekMsQ0FBYjtBQUNILGFBTEQ7O0FBT0EsY0FBRSxLQUFLLEdBQUwsQ0FBUyxZQUFYLEVBQXlCLElBQXpCLENBQThCLFlBQVc7QUFDckMsb0JBQUksQ0FBQyxLQUFLLFdBQVYsRUFBdUI7QUFDbkIseUJBQUssV0FBTCxHQUFtQixJQUFJLFVBQUosQ0FBUyxJQUFULEVBQWUsS0FBSyxHQUFMLENBQVMsSUFBeEIsQ0FBbkI7QUFDSDtBQUNKLGFBSkQ7O0FBTUEsY0FBRSxNQUFGLEVBQVUsRUFBVixDQUFhLG1CQUFiLEVBQWtDLFlBQU07QUFDcEMsa0JBQUUsTUFBSyxHQUFMLENBQVMsWUFBWCxFQUF5QixJQUF6QixDQUE4QixZQUFXO0FBQ3JDLHdCQUFJLENBQUMsS0FBSyxXQUFWLEVBQXVCO0FBQ25CLDZCQUFLLFdBQUwsR0FBbUIsSUFBSSxVQUFKLENBQVMsSUFBVCxFQUFlLEtBQUssR0FBTCxDQUFTLElBQXhCLENBQW5CO0FBQ0g7QUFDSixpQkFKRDtBQUtILGFBTkQ7O0FBUUEsY0FBRSxNQUFGLEVBQVUsRUFBVixDQUFhLG9CQUFiLEVBQW1DLFVBQUMsQ0FBRCxFQUFJLElBQUosRUFBYTtBQUM1QyxrQkFBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixlQUFsQixFQUFtQyxJQUFuQztBQUNILGFBRkQ7QUFHSDs7O2dDQUNPLE0sRUFBUTtBQUFBOztBQUNaLG9CQUFRLEdBQVIsQ0FBWSxzQkFBWixFQUFvQyxNQUFwQzs7QUFFQSx1QkFBSyxHQUFMLENBQVMsVUFBVCxFQUFxQixNQUFyQixFQUE2QixJQUE3QixDQUFrQyxVQUFDLElBQUQsRUFBVTtBQUN4QyxvQkFBSSxLQUFLLEVBQVQsRUFBYTtBQUNULDJCQUFLLFFBQUwsQ0FBYyxLQUFLLE9BQW5CO0FBQ0gsaUJBRkQsTUFFTztBQUNILDRCQUFRLEtBQVIsQ0FBYyxpQkFBZDtBQUNIO0FBQ0osYUFORCxFQU1HLEtBTkgsQ0FNUyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIsc0JBQU0sSUFBSSxLQUFKLENBQVUsS0FBVixDQUFOO0FBQ0gsYUFSRDtBQVNIOzs7aUNBQ1EsTyxFQUFTO0FBQ2Qsb0JBQVEsR0FBUixDQUFZLHVCQUFaOztBQUVBLGdCQUFJLE9BQU8sSUFBWDs7QUFFQSxjQUFFLFFBQUYsQ0FBVyxFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixrQkFBbkIsRUFBNkIsS0FBSyxHQUFMLENBQVMsUUFBdEMsRUFBZ0Q7QUFDdkQsc0JBQU0sTUFEaUQ7QUFFdkQseUJBQVMsT0FGOEM7QUFHdkQsMkJBQVcscUJBQVc7QUFDbEIsd0JBQUksT0FBTyxLQUFLLEtBQUwsQ0FBVyxJQUFYLENBQWdCLEtBQUssR0FBTCxDQUFTLFlBQXpCLEVBQXVDLEdBQXZDLENBQTJDLENBQTNDLENBQVg7QUFDQSx5QkFBSyxXQUFMLEdBQW1CLElBQUksVUFBSixDQUFTLElBQVQsRUFBZSxLQUFLLEdBQUwsQ0FBUyxJQUF4QixDQUFuQjtBQUNIO0FBTnNELGFBQWhELENBQVg7QUFRSDs7Ozs7O1FBS0QsUSxHQUFBLFE7OztBQ3BGSjs7QUFFQTs7Ozs7Ozs7O0FBRUE7Ozs7QUFHQSxJQUFJLFdBQVc7QUFDWCxtQkFBZSxzQkFESjtBQUVYLHFCQUFpQix3QkFGTjtBQUdYLHFCQUFpQix3QkFITjtBQUlYLGdCQUFZO0FBSkQsQ0FBZjs7SUFRTSxJO0FBQ0Ysa0JBQVksRUFBWixFQUFnQixPQUFoQixFQUF5QjtBQUFBOztBQUNyQixhQUFLLEVBQUwsR0FBVSxFQUFWO0FBQ0EsYUFBSyxHQUFMLEdBQVcsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLFFBQWIsRUFBdUIsT0FBdkIsQ0FBWDtBQUNBLGFBQUssSUFBTDtBQUNIOzs7OytCQUNNO0FBQ0gsb0JBQVEsR0FBUixDQUFZLGVBQVosRUFBNkIsS0FBSyxFQUFsQzs7QUFFQSxpQkFBSyxLQUFMLEdBQWEsRUFBRSxLQUFLLEVBQVAsQ0FBYjtBQUNBLGlCQUFLLE9BQUwsR0FBZSxLQUFLLEdBQUwsQ0FBUyxPQUFULElBQW9CLEVBQUUsS0FBSyxHQUFMLENBQVMsYUFBWCxFQUEwQixLQUFLLEVBQS9CLENBQW5DO0FBQ0EsaUJBQUssUUFBTCxHQUFnQixLQUFLLEdBQUwsQ0FBUyxRQUFULElBQXFCLEVBQUUsS0FBSyxHQUFMLENBQVMsZUFBWCxFQUE0QixNQUE1QixDQUFtQyxvQkFBb0IsS0FBSyxLQUFMLENBQVcsSUFBWCxDQUFnQixTQUFoQixDQUFwQixHQUFpRCxJQUFwRixDQUFyQzs7QUFFQSxpQkFBSyxXQUFMO0FBQ0EsaUJBQUssV0FBTDs7QUFFQSxnQkFBSSxFQUFFLFFBQUYsQ0FBVyxNQUFmLEVBQXVCO0FBQ25CLGtCQUFFLFFBQUYsQ0FBVyxNQUFYO0FBQ0g7QUFDSjs7O2lDQUNRO0FBQ0wsb0JBQVEsR0FBUixDQUFZLGlCQUFaLEVBQStCLEtBQUssRUFBcEM7O0FBRUEsZ0JBQUksQ0FBQyxLQUFLLFNBQVYsRUFBcUI7QUFDakIscUJBQUssYUFBTDtBQUNBLHFCQUFLLGNBQUw7QUFDQSxxQkFBSyxZQUFMO0FBQ0g7QUFDSjs7O3NDQUNhO0FBQ1YsaUJBQUssT0FBTCxDQUFhLE1BQWIsQ0FBb0IsS0FBSyxHQUFMLENBQVMsZUFBVCxHQUEyQixTQUEvQyxFQUEwRCxNQUExRDtBQUNIOzs7c0NBQ2E7QUFBQTs7QUFDVixpQkFBSyxLQUFMLENBQVcsRUFBWCxDQUFjLFFBQWQsRUFBd0IsVUFBQyxDQUFELEVBQU87QUFDM0Isa0JBQUUsY0FBRjtBQUNBLHNCQUFLLE1BQUw7QUFDSCxhQUhEO0FBSUg7Ozt3Q0FDZTtBQUNaLGlCQUFLLFNBQUwsR0FBaUIsSUFBakI7O0FBRUEsaUJBQUssT0FBTCxDQUFhLFdBQWIsQ0FBeUIsS0FBSyxHQUFMLENBQVMsVUFBbEM7O0FBRUEsY0FBRSxRQUFGLENBQVcsV0FBWDtBQUNIOzs7eUNBQ2dCO0FBQUE7O0FBQ2IsZ0JBQUksU0FBUyxJQUFJLFFBQUosQ0FBYSxLQUFLLEVBQWxCLENBQWI7O0FBRUEsdUJBQUssR0FBTCxDQUFTLFdBQVQsRUFBc0IsTUFBdEIsRUFBOEIsSUFBOUIsQ0FBbUMsVUFBQyxJQUFELEVBQVU7QUFDekMsb0JBQUksS0FBSyxFQUFULEVBQWE7QUFDVCw0QkFBUSxHQUFSLENBQVksMkNBQVo7O0FBRUEsd0JBQUksT0FBSyxLQUFMLENBQVcsSUFBWCxDQUFnQixNQUFoQixDQUFKLEVBQTZCO0FBQ3pCLDBCQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLG9CQUFsQixFQUF3QyxPQUFLLEtBQUwsQ0FBVyxJQUFYLEVBQXhDO0FBQ0g7O0FBRUQsd0JBQUksT0FBSyxRQUFMLENBQWMsTUFBbEIsRUFBMEI7QUFDdEIsK0JBQUssUUFBTCxDQUFjLElBQWQsQ0FBbUIsS0FBSyxPQUF4QjtBQUNBLCtCQUFLLEtBQUwsQ0FBVyxNQUFYO0FBQ0gscUJBSEQsTUFHTztBQUNILCtCQUFLLEtBQUwsQ0FBVyxXQUFYLENBQXVCLEtBQUssT0FBNUI7QUFDSDtBQUNELDJCQUFLLFlBQUw7QUFDSCxpQkFkRCxNQWNPLElBQUksS0FBSyxNQUFULEVBQWlCO0FBQ3BCLDRCQUFRLEdBQVIsQ0FBWSxvQ0FBWixFQUFrRCxLQUFLLE1BQXZEOztBQUVBLHlCQUFLLElBQUksQ0FBVCxJQUFjLEtBQUssTUFBbkIsRUFBMkI7QUFDdkIsK0JBQUssT0FBTCxDQUFhLE1BQWIsQ0FBb0IsT0FBSyxHQUFMLENBQVMsZUFBVCxHQUEyQixDQUEvQyxFQUFrRCxRQUFsRCxDQUEyRCxPQUFLLEdBQUwsQ0FBUyxVQUFwRTtBQUNIO0FBQ0QsMkJBQUssWUFBTDtBQUNIO0FBQ0osYUF2QkQsRUF1QkcsS0F2QkgsQ0F1QlMsVUFBUyxLQUFULEVBQWdCO0FBQ3JCLHdCQUFRLEtBQVIsQ0FBYyx3Q0FBZCxFQUF3RCxLQUF4RDs7QUFFQSxxQkFBSyxZQUFMO0FBQ0gsYUEzQkQ7QUE0Qkg7Ozt1Q0FDYztBQUNYLGNBQUUsUUFBRixDQUFXLFdBQVg7O0FBRUEsZ0JBQUksRUFBRSxRQUFGLENBQVcsTUFBZixFQUF1QjtBQUNuQixrQkFBRSxRQUFGLENBQVcsTUFBWDtBQUNIOztBQUVELGlCQUFLLFNBQUwsR0FBaUIsS0FBakI7QUFDSDs7Ozs7O1FBS0QsSSxHQUFBLEk7OztBQ3pHSjs7QUFFQTs7Ozs7Ozs7O0FBRUE7Ozs7SUFHTSxJO0FBQ0Ysa0JBQVksT0FBWixFQUFxQjtBQUFBOztBQUNqQixhQUFLLEdBQUwsR0FBVyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWE7QUFDcEIsbUJBQU8sRUFEYTtBQUVwQixzQkFBVTtBQUZVLFNBQWIsRUFHUixPQUhRLENBQVg7QUFJQSxhQUFLLElBQUw7QUFDSDs7OzsrQkFDTTtBQUFBOztBQUNILG9CQUFRLEdBQVIsQ0FBWSxlQUFaOztBQUVBLGlCQUFLLEdBQUwsQ0FBUyxLQUFULENBQWUsT0FBZixDQUF1QixVQUFDLElBQUQsRUFBVTtBQUM3QixzQkFBSyxJQUFMLENBQVUsSUFBVjtBQUNILGFBRkQ7QUFHQSxjQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsZ0JBQWIsRUFBK0IsVUFBQyxDQUFELEVBQUksSUFBSixFQUFhO0FBQ3hDLHNCQUFLLEtBQUwsQ0FBVyxLQUFLLElBQWhCLEVBQXNCLEtBQUssTUFBM0I7QUFDSCxhQUZEO0FBR0g7Ozs2QkFDSSxJLEVBQU07QUFBQTs7QUFDUCxnQkFBSSxVQUFVLFNBQVYsT0FBVSxDQUFDLENBQUQsRUFBSSxJQUFKLEVBQWE7QUFDdkIsb0JBQUksS0FBSyxJQUFULEVBQWU7QUFDWCwyQkFBSyxLQUFMLENBQVcsS0FBSyxJQUFoQixFQUFzQixLQUFLLE1BQTNCO0FBQ0gsaUJBRkQsTUFFTyxJQUFJLEtBQUssT0FBVCxFQUFrQjtBQUNyQix5QkFBSyxPQUFMLENBQWEsSUFBYixDQUFrQixFQUFFLE1BQXBCLEVBQTRCLENBQTVCLEVBQStCLElBQS9CO0FBQ0gsaUJBRk0sTUFFQTtBQUNILDBCQUFNLElBQUksS0FBSixDQUFVLHNDQUFWLENBQU47QUFDSDtBQUNKLGFBUkQ7O0FBVUEsZ0JBQUksS0FBSyxxQkFBTCxDQUEyQixLQUFLLFFBQWhDLENBQUosRUFBK0M7QUFDM0MscUJBQUssS0FBTCxDQUFXLEVBQVgsQ0FBYyxLQUFLLEtBQW5CLEVBQTBCLEtBQUssUUFBL0IsRUFBeUMsT0FBekM7QUFDSCxhQUZELE1BRU87QUFDSCxrQkFBRSxLQUFLLFFBQVAsRUFBaUIsRUFBakIsQ0FBb0IsS0FBSyxLQUF6QixFQUFnQyxPQUFoQztBQUNIO0FBQ0o7Ozs4QkFDSyxJLEVBQU0sTSxFQUFRO0FBQ2hCLG9CQUFRLEdBQVIscUJBQThCLElBQTlCOztBQUVBLGlCQUFLLEdBQUwsQ0FBUyxRQUFULENBQWtCLE9BQWxCLENBQTBCLFVBQUMsT0FBRCxFQUFhO0FBQ25DLHdCQUFRLFNBQVIsQ0FBa0IsSUFBbEIsRUFBd0IsTUFBeEI7QUFDSCxhQUZEO0FBR0g7Ozs4Q0FPcUIsUSxFQUFVO0FBQzVCLG1CQUFPLENBQUMsTUFBRCxFQUFTLFFBQVQsRUFBbUIsU0FBUyxlQUE1QixFQUE2QyxTQUFTLElBQXRELEVBQTRELE1BQTVELEVBQW9FLE1BQXBFLEVBQTRFLE9BQTVFLENBQW9GLFFBQXBGLE1BQWtHLENBQUMsQ0FBMUc7QUFDSDs7OzRCQVJXO0FBQ1IsZ0JBQUksQ0FBQyxLQUFLLE9BQVYsRUFBbUI7QUFDZixxQkFBSyxPQUFMLEdBQWUsRUFBRSxTQUFTLElBQVgsQ0FBZjtBQUNIO0FBQ0QsbUJBQU8sS0FBSyxPQUFaO0FBQ0g7Ozs7OztRQVFELEksR0FBQSxJOzs7QUM5REo7O0FBRUE7Ozs7Ozs7Ozs7QUFFQSxJQUFJLFdBQVc7QUFDWCxjQUFVLGlCQURDO0FBRVgsa0JBQWMsdUJBRkg7QUFHWCxpQkFBYSxzQkFIRjtBQUlYLGtCQUFjLHVCQUpIO0FBS1gsa0JBQWMsdUJBTEg7QUFNWCxrQkFBYyx1QkFOSDtBQU9YLFlBQVE7QUFDSixrQkFBVSxHQUROO0FBRUosb0JBQVksSUFGUjtBQUdKLGVBQU8sSUFISDtBQUlKLGNBQU07QUFKRjtBQVBHLENBQWY7O0lBZU0sVTtBQUNGLHdCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFDakIsYUFBSyxHQUFMLEdBQVcsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLFFBQWIsRUFBdUIsT0FBdkIsQ0FBWDtBQUNBLGFBQUssSUFBTDtBQUNIOzs7OytCQUNNO0FBQ0gsb0JBQVEsR0FBUixDQUFZLHFCQUFaOztBQUVBLGdCQUFJLFNBQVMsRUFBRSxLQUFLLEdBQUwsQ0FBUyxRQUFYLENBQWI7QUFDQSxnQkFBSSxPQUFPLE1BQVgsRUFBbUI7QUFDZixvQkFBSSxTQUFTLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFlBQXJCLENBQWI7QUFBQSxvQkFDSSxPQUFPLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFdBQXJCLENBRFg7QUFBQSxvQkFFSSxTQUFTLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFlBQXJCLENBRmI7QUFBQSxvQkFHSSxRQUFRLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFlBQXJCLENBSFo7QUFBQSxvQkFJSSxRQUFRLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFlBQXJCLENBSlo7O0FBTUEsdUJBQU8sTUFBUCxDQUFjLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxLQUFLLEdBQUwsQ0FBUyxNQUF0QixFQUE4QjtBQUN4Qyx5QkFBSyxNQURtQztBQUV4QywwQkFBTSxLQUZrQztBQUd4QywwQkFBTTtBQUhrQyxpQkFBOUIsQ0FBZDtBQUtIO0FBQ0o7Ozs7OztRQUlKLFUsR0FBQSxVOzs7QUM3Q0Q7O0FBRUE7Ozs7Ozs7OztBQUVBOztBQUNBOzs7O0FBRUEsSUFBSSxXQUFXO0FBQ2Qsa0JBQWMsMkJBREE7QUFFZCxrQkFBYywyQkFGQTtBQUdkLGNBQVU7QUFISSxDQUFmOztJQU1NLGM7QUFDRiw0QkFBWSxPQUFaLEVBQXFCO0FBQUE7O0FBQ2pCLGFBQUssR0FBTCxHQUFXLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxRQUFiLEVBQXVCLE9BQXZCLENBQVg7QUFDQSxhQUFLLElBQUw7QUFDSDs7OzsrQkFDTTtBQUFBOztBQUNILG9CQUFRLEdBQVIsQ0FBWSx5QkFBWjs7QUFFQSxnQkFBSSxRQUFRLEVBQUUsS0FBSyxHQUFMLENBQVMsWUFBWCxDQUFaOztBQUVBLGtCQUFNLEVBQU4sQ0FBUyxPQUFULEVBQWtCLFVBQUMsQ0FBRCxFQUFPO0FBQ3hCLGtCQUFFLGNBQUY7QUFDQSxzQkFBSyxJQUFMO0FBQ0gsYUFIRTtBQUlIOzs7K0JBQ007QUFBQTs7QUFDSCxvQkFBUSxHQUFSLENBQVksaUJBQVo7O0FBRUEsdUJBQUssR0FBTCxDQUFTLHFCQUFULEVBQWdDLElBQWhDLENBQXFDLFVBQUMsSUFBRCxFQUFVO0FBQzNDLG9CQUFJLEtBQUssRUFBVCxFQUFhO0FBQ1QsMkJBQUssSUFBTCxDQUFVLEtBQUssT0FBZjtBQUNILGlCQUZELE1BRU87QUFDSCwwQkFBTSxJQUFJLEtBQUosQ0FBVSw0QkFBVixDQUFOO0FBQ0g7QUFDSixhQU5ELEVBTUcsS0FOSCxDQU1TLFVBQUMsS0FBRCxFQUFXO0FBQ2hCLHNCQUFNLEtBQU47QUFDSCxhQVJEO0FBU0g7Ozs2QkFDSSxPLEVBQVM7QUFDVixvQkFBUSxHQUFSLENBQVksaUJBQVo7O0FBRUEsZ0JBQUksT0FBTyxJQUFYOztBQUVBLGNBQUUsUUFBRixDQUFXLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLGtCQUFuQixFQUE2QixLQUFLLEdBQUwsQ0FBUyxRQUF0QyxFQUFnRDtBQUN2RCxzQkFBTSxNQURpRDtBQUV2RCx5QkFBUyxPQUY4QztBQUd2RCwyQkFBVyxxQkFBVztBQUNsQix5QkFBSyxLQUFMLENBQVcsSUFBWCxDQUFnQixLQUFLLEdBQUwsQ0FBUyxZQUF6QixFQUF1QyxLQUF2QyxDQUE2QyxVQUFTLENBQVQsRUFBWTtBQUNyRCwwQkFBRSxjQUFGO0FBQ0EsNkJBQUssTUFBTCxDQUFZLEVBQUUsSUFBRixFQUFRLElBQVIsRUFBWixFQUE0QixJQUE1QixDQUFpQyxVQUFDLElBQUQsRUFBVTtBQUMxQyw4QkFBRSxRQUFGLENBQVcsS0FBWDtBQUNBLHlCQUZELEVBRUcsS0FGSCxDQUVTLFVBQUMsS0FBRCxFQUFXO0FBQ25CLGtDQUFNLEtBQU47QUFDQSx5QkFKRDtBQUtILHFCQVBEO0FBUUg7QUFac0QsYUFBaEQsQ0FBWDtBQWNIOzs7K0JBQ00sSSxFQUFNO0FBQ1osbUJBQU8sV0FBSyxHQUFMLENBQVMsZUFBVCxFQUEwQixJQUExQixFQUFnQyxJQUFoQyxDQUFxQyxVQUFDLElBQUQsRUFBVTtBQUMvQyxvQkFBSSxLQUFLLEVBQVQsRUFBYTtBQUNULDZCQUFTLFFBQVQsQ0FBa0IsTUFBbEI7QUFDQSwyQkFBTyxJQUFQO0FBQ0gsaUJBSEQsTUFHTztBQUNILDBCQUFNLElBQUksS0FBSixDQUFVLDZCQUFWLENBQU47QUFDSDtBQUNKLGFBUEcsQ0FBUDtBQVFBOzs7Ozs7UUFJRCxjLEdBQUEsYzs7O0FDMUVKOztBQUVBOzs7Ozs7Ozs7O0FBRUEsSUFBSSxXQUFXO0FBQ1gsaUJBQWEsa0JBREY7QUFFWCxTQUFLO0FBQ0QsY0FBTSxFQURMO0FBRUQsa0JBQVUsQ0FBQyxTQUFEO0FBRlQsS0FGTTtBQU1YLG9CQUFnQjtBQU5MLENBQWY7O0lBU00sVTtBQUNGLHdCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFDakIsYUFBSyxHQUFMLEdBQVcsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLFFBQWIsRUFBdUIsT0FBdkIsQ0FBWDtBQUNBLGFBQUssSUFBTDtBQUNIOzs7OytCQUNNO0FBQUE7O0FBQ0gsb0JBQVEsR0FBUixDQUFZLHFCQUFaOztBQUVBLGlCQUFLLElBQUwsR0FBWSxFQUFFLEtBQUssR0FBTCxDQUFTLFdBQVgsQ0FBWjtBQUNBLGdCQUFJLEtBQUssSUFBTCxDQUFVLE1BQWQsRUFBc0I7QUFDbEIscUJBQUssTUFBTCxHQUFjLEtBQUssSUFBTCxDQUFVLElBQVYsQ0FBZSxRQUFmLEVBQXlCLEtBQXpCLENBQStCLGNBQS9CLENBQWQ7QUFDQSxxQkFBSyxHQUFMLENBQVMsR0FBVCxDQUFhLE1BQWIsR0FBc0IsS0FBSyxNQUEzQjs7QUFFQSxrQkFBRSxTQUFGLENBQVksS0FBSyxHQUFMLENBQVMsY0FBckIsRUFBcUMsSUFBckMsQ0FBMEMsWUFBTTtBQUM1QywwQkFBTSxLQUFOLENBQVksWUFBTTtBQUNkLDhCQUFLLE9BQUw7QUFDSCxxQkFGRDtBQUdILGlCQUpELEVBSUcsSUFKSCxDQUlRLFlBQU07QUFDViwwQkFBTSxJQUFJLEtBQUoseUJBQWdDLE1BQUssR0FBTCxDQUFTLGNBQXpDLENBQU47QUFDSCxpQkFORDtBQU9IO0FBQ0o7OztrQ0FDUztBQUNOLGdCQUFJLE1BQU0sSUFBSSxNQUFNLEdBQVYsQ0FBYyxLQUFLLElBQUwsQ0FBVSxHQUFWLENBQWMsQ0FBZCxDQUFkLEVBQWdDLEtBQUssR0FBTCxDQUFTLEdBQXpDLENBQVY7O0FBRUEsZ0JBQUksWUFBWSxJQUFJLE1BQU0sU0FBVixDQUFvQixLQUFLLEdBQUwsQ0FBUyxHQUFULENBQWEsTUFBakMsRUFBeUMsRUFBekMsRUFBNkM7QUFDekQsNEJBQVksZUFENkM7QUFFekQsK0JBQWUsMEJBRjBDO0FBR3pELCtCQUFlLENBQUMsRUFBRCxFQUFLLEVBQUw7QUFIMEMsYUFBN0MsQ0FBaEI7QUFLQSxnQkFBSSxVQUFKLENBQWUsR0FBZixDQUFtQixTQUFuQjs7QUFFQSxnQkFBSSxTQUFKLENBQWMsT0FBZCxDQUFzQixZQUF0QjtBQUNIOzs7Ozs7UUFJRCxVLEdBQUEsVTs7O0FDbERKOztBQUVBOzs7Ozs7Ozs7QUFFQTs7OztBQUVBLElBQUksV0FBVztBQUNYLGNBQVU7QUFEQyxDQUFmO0FBR0EsSUFBSSxjQUFKOztJQUNNLGM7QUFDRiw0QkFBWSxPQUFaLEVBQXFCO0FBQUE7O0FBQ2pCLGFBQUssR0FBTCxHQUFXLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxRQUFiLEVBQXVCLE9BQXZCLENBQVg7QUFDQSxhQUFLLElBQUw7QUFDSDs7OzsrQkFDTTtBQUNILG9CQUFRLEdBQVIsQ0FBWSx5QkFBWjtBQUNBLG9CQUFRLEVBQUUsS0FBSyxHQUFMLENBQVMsUUFBWCxDQUFSO0FBQ0EsaUJBQUssUUFBTDtBQUNIOzs7bUNBQ1U7QUFBQTs7QUFDVixnQkFBSSxPQUFPLElBQVg7QUFDQSxnQkFBSSxnQkFFRyxzRUFDSCx5RkFERyxHQUVBLFFBRkEsR0FHQSxnR0FIQSxHQUlILGdGQUpHLEdBS0gscUZBTEcsR0FNQSxRQU5BLEdBT0Esd0NBUEEsR0FRQSxxRUFSQSxHQVNBLHNFQVRBLEdBVUEsNkNBVkEsR0FXQSw0R0FYQSxHQVlBLDhDQVpBLEdBYUEsMkNBYkEsR0FjQSwwRUFkQSxHQWVBLG1CQWZBLEdBZ0JBLFdBaEJBLEdBaUJBLDhDQWpCQSxHQWtCSCw0Q0FsQkcsR0FtQkEscUVBbkJBLEdBb0JBLCtDQXBCQSxHQXFCQSxxQkFyQkEsR0FzQkEsdUlBdEJBLEdBdUJBLHFCQXZCQSxHQXdCQSxnQkF4QkEsR0F5QkEsK0NBekJBLEdBMEJBLHFCQTFCQSxHQTJCQSxnSUEzQkEsR0E0QkEscUJBNUJBLEdBNkJBLGdCQTdCQSxHQThCSCw2Q0E5QkcsR0ErQkgsZUEvQkcsR0FnQ0gsaUpBaENHLEdBaUNBLDBEQWpDQSxHQWtDSCxnQkFsQ0csR0FtQ0gsY0FuQ0csR0FvQ0gsbURBcENHLEdBcUNILHNGQXJDRyxHQXNDSCxjQXRDRyxHQXVDSCxlQXZDRyxHQXdDQSxXQXhDQSxHQXlDQSxRQTNDUDs7QUErQ0EsZ0JBQUksUUFBUSxFQUFFLFFBQUYsQ0FBVyxhQUFYLENBQVo7QUFDRyxvQkFBUSxHQUFSLENBQVksS0FBWjtBQUNILGtCQUFNLElBQU4sQ0FBVyxPQUFYO0FBQ0Esa0JBQU0sRUFBTixDQUFTLE9BQVQsRUFBa0IsZUFBbEIsRUFBbUMsVUFBVSxDQUFWLEVBQWE7QUFDekMscUJBQUssWUFBTDtBQUNBLGtCQUFFLGNBQUY7QUFDSCxhQUhKO0FBSUcsZ0JBQUksUUFBUSxNQUFNLElBQU4sQ0FBVyxNQUFYLENBQVo7QUFDQSxrQkFBTSxNQUFOLENBQWEsVUFBQyxDQUFELEVBQU87QUFDaEIsa0JBQUUsY0FBRjtBQUNBLG9CQUFJLFNBQVMsTUFBTSxlQUFOLEVBQWI7QUFDQSx3QkFBUSxHQUFSLENBQVksb0JBQVosRUFBa0MsTUFBbEM7QUFDQSxzQkFBSyxXQUFMLENBQWlCLE1BQWpCLEVBQXlCLFVBQUMsSUFBRCxFQUFVO0FBQUMsMEJBQUssbUJBQUwsQ0FBeUIsSUFBekI7QUFBaUMsaUJBQXJFO0FBQ0gsYUFMRDtBQU1OOzs7dUNBQ2M7QUFBQTs7QUFDaEI7QUFDRSxnQkFBSSx1QkFFTSwrQ0FDQSxnRUFEQSxHQUVBLGdFQUZBLEdBR0EsUUFIQSxHQUlBLG1FQUpBLEdBS0EsNERBTEEsR0FNQSxRQU5BLEdBT0EscUVBUEEsR0FRQSx5RkFSQSxHQVNBLFFBVEEsR0FVQSxnRUFWQSxHQVdBLDBDQVhBLEdBWUgsMkRBWkcsR0FhSCx5Q0FiRyxHQWNBLG1CQWRBLEdBZUEsZ0lBZkEsR0FnQkEsbUJBaEJBLEdBaUJBLHVIQWpCQSxHQWtCSCxVQWxCRyxHQW1CQSwyQ0FuQkEsR0FvQkEsbUJBcEJBLEdBcUJBLCtJQXJCQSxHQXNCQSxtQkF0QkEsR0F1QkEsOEhBdkJBLEdBd0JBLFlBeEJBLEdBeUJBLGlEQXpCQSxHQTBCQSxzR0ExQkEsR0EyQkEsWUEzQkEsR0E0QkgsVUE1QkcsR0E2QkEsUUEvQlY7QUFrQ0csZ0JBQUksUUFBUSxFQUFFLFFBQUYsQ0FBVyxvQkFBWCxDQUFaO0FBQ0Esa0JBQU0sSUFBTixDQUFXLE9BQVg7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGdCQUFJLFFBQVEsTUFBTSxJQUFOLENBQVcsTUFBWCxDQUFaO0FBQ0csa0JBQU0sTUFBTixDQUFhLFVBQUMsQ0FBRCxFQUFPO0FBQ2hCLGtCQUFFLGNBQUY7QUFDQSxvQkFBSSxTQUFTLE1BQU0sZUFBTixFQUFiO0FBQ0Esd0JBQVEsR0FBUixDQUFZLG9CQUFaLEVBQWtDLE1BQWxDO0FBQ0EsdUJBQUssV0FBTCxDQUFpQixNQUFqQixFQUF5QixVQUFDLElBQUQsRUFBVTtBQUFDLDJCQUFLLG1CQUFMLENBQXlCLElBQXpCO0FBQWlDLGlCQUFyRTtBQUNILGFBTEQ7QUFNTjs7O3FDQUNlLE0sRUFBUTtBQUFBOztBQUNqQixvQkFBUSxHQUFSLENBQVksaUNBQVosRUFBK0MsTUFBL0M7O0FBRUgsZ0JBQUksV0FFRywrQ0FDQSxnRUFEQSxHQUVBLGdFQUZBLEdBR0EsUUFIQSxHQUlOLHdDQUpNLEdBS0EsMkRBTEEsR0FNQSxRQU5BLEdBT0Esd0NBUEEsR0FRQSxrR0FSQSxHQVNBLCtIQVRBLEdBVUEsbUZBVkEsR0FXQSxZQVhBLEdBWUEsK0NBWkEsR0FhQSw2Q0FiQSxHQWNBLG9FQWRBLEdBZUEsa0RBZkEsR0FnQkEseUJBaEJBLEdBaUJBLHFHQWpCQSxHQWtCQSx5QkFsQkEsR0FtQkEsbUJBbkJBLEdBb0JBLGtEQXBCQSxHQXFCQSx3QkFyQkEsR0FzQkEsMEpBdEJBLEdBdUJBLDhEQXZCQSxHQXdCQSx5QkF4QkEsR0F5QkEsbUJBekJBLEdBMEJBLGtEQTFCQSxHQTJCQSx3QkEzQkEsR0E0QkEseUpBNUJBLEdBNkJBLDhEQTdCQSxHQThCQSx5QkE5QkEsR0ErQkEsbUJBL0JBLEdBZ0NBLHdEQWhDQSxHQWlDQSwrRkFqQ0EsR0FrQ0EsbUJBbENBLEdBbUNBLGdCQW5DQSxHQW9DQSxXQXBDQSxHQXFDQSxRQXZDUDtBQXlDQSxnQkFBSSxRQUFRLEVBQUUsUUFBRixDQUFXLFFBQVgsQ0FBWjtBQUNBLGtCQUFNLElBQU4sQ0FBVyxPQUFYO0FBQ0EsZ0JBQUksUUFBUSxNQUFNLElBQU4sQ0FBVyxNQUFYLENBQVo7QUFDRyxrQkFBTSxNQUFOLENBQWEsVUFBQyxDQUFELEVBQU87QUFDaEIsa0JBQUUsY0FBRjtBQUNBLG9CQUFJLFNBQVMsTUFBTSxlQUFOLEVBQWI7QUFDQTtBQUNBLHVCQUFLLFdBQUwsQ0FBaUIsTUFBakIsRUFBeUIsVUFBQyxJQUFELEVBQVU7QUFBQywyQkFBSyxtQkFBTCxDQUF5QixJQUF6QjtBQUFpQyxpQkFBckU7QUFDSCxhQUxEO0FBTUg7OzswQ0FDaUI7QUFBQTs7QUFDZCxvQkFBUSxHQUFSLENBQVksb0NBQVo7QUFDQSxnQkFBSSxXQUNBLFFBQ0EscURBREEsR0FFQSxNQUZBLEdBR0EsUUFIQSxHQUlBLGdEQUpBLEdBS0EsdUZBTEEsR0FNQSxXQU5BLEdBT0EsU0FSSjtBQVNBLGdCQUFJLFFBQVEsRUFBRSxRQUFGLENBQVcsUUFBWCxDQUFaO0FBQ0Esa0JBQU0sSUFBTixDQUFXLE9BQVg7QUFDQSxnQkFBSSxRQUFRLE1BQU0sSUFBTixDQUFXLE1BQVgsQ0FBWjtBQUNBLGtCQUFNLE1BQU4sQ0FBYSxVQUFDLENBQUQsRUFBTztBQUNoQixrQkFBRSxjQUFGO0FBQ0EsdUJBQUssUUFBTDtBQUNILGFBSEQ7QUFJSDs7OzRDQUNvQixJLEVBQU87QUFDM0Isb0JBQVEsR0FBUixDQUFZLHdDQUFaLEVBQXNELElBQXREO0FBQ0csb0JBQVEsS0FBSyxFQUFiO0FBQ0MscUJBQUssR0FBTDtBQUNJLDRCQUFRLEdBQVIsQ0FBWSxZQUFaO0FBQ0csd0JBQUksS0FBSyxJQUFMLElBQWEsQ0FBakIsRUFBb0I7QUFDdEIsNkJBQUssWUFBTCxDQUFrQixFQUFsQjtBQUNHLHFCQUZELE1BRU87QUFDSCw2QkFBSyxlQUFMO0FBQ0g7QUFDUDtBQUNELHFCQUFLLEdBQUw7QUFDQyw0QkFBUSxHQUFSLENBQVksV0FBWjtBQUNBO0FBQ0QscUJBQUssR0FBTDtBQUNDLDRCQUFRLEdBQVIsQ0FBWSxjQUFaO0FBQ0E7QUFDRCxxQkFBSyxHQUFMO0FBQ0MsNEJBQVEsR0FBUixDQUFZLGNBQVo7QUFDQTtBQUNELHFCQUFLLEdBQUw7QUFDQyw0QkFBUSxHQUFSLENBQVksMEJBQVo7QUFDQTtBQUNEO0FBQ0MsNEJBQVEsS0FBUixDQUFjLGtCQUFkO0FBQ0E7QUF2QkY7QUF5Qkg7Ozs0Q0FDb0IsSSxFQUFPO0FBQ3hCLG9CQUFRLEdBQVIsQ0FBWSx3Q0FBWixFQUFzRCxJQUF0RDtBQUNBLGdCQUFJLE9BQU8sSUFBWDtBQUNBLG9CQUFRLEtBQUssRUFBYjtBQUNJLHFCQUFLLEdBQUw7QUFDSSw0QkFBUSxHQUFSLENBQVksNEJBQVo7QUFDQSx3QkFBSSxTQUFTLEVBQUMsU0FBUyxZQUFWLEVBQWI7QUFDQSx5QkFBSyxXQUFMLENBQWlCLE1BQWpCLEVBQXlCLFVBQUMsSUFBRCxFQUFVO0FBQUMsNkJBQUssaUJBQUwsQ0FBdUIsSUFBdkI7QUFBK0IscUJBQW5FO0FBQ0E7QUFDSixxQkFBSyxHQUFMO0FBQ0ksNEJBQVEsR0FBUixDQUFZLGlCQUFaO0FBQ0E7QUFDSixxQkFBSyxHQUFMO0FBQ0ksNEJBQVEsR0FBUixDQUFZLDJCQUFaO0FBQ0E7QUFYUjtBQWFIOzs7MENBQ2tCLEksRUFBTztBQUN0QixvQkFBUSxHQUFSLENBQVksc0NBQVosRUFBb0QsSUFBcEQ7QUFDQSxnQkFBSSxNQUFNLEtBQUssT0FBTCxDQUFhLE1BQXZCO0FBQ0Esb0JBQVEsR0FBUixDQUFZLEdBQVo7QUFDQSxnQkFBSSxXQUNKLCtCQUNBLDZCQURBLEdBRUEsbUVBRkEsR0FHQSx5RUFIQSxHQUlBLDJFQUpBLEdBS0EseUVBTEEsR0FNQSxxRUFOQSxHQU9BLHlFQVBBLEdBUUEsZ0ZBUkEsR0FTQSxrRUFUQSxHQVVBLFdBVkEsR0FXQSx5Q0FYQSxHQVlBLGlDQVpBLEdBYUEsNkNBYkEsR0FjQSx3REFkQSxHQWVBLGlGQWZBLEdBZ0JBLHVCQWhCQSxHQWlCQSxtQkFqQkEsR0FrQkEsa0RBbEJBLEdBbUJBLDhEQW5CQSxHQW9CQSx1Q0FwQkEsR0FxQkEsd0JBckJBLEdBc0JBLG1CQXRCQSxHQXVCQSw2Q0F2QkEsR0F3QkEseURBeEJBLEdBeUJBLDhCQXpCQSxHQTBCQSxtQkExQkEsR0EyQkEsa0RBM0JBLEdBNEJBLDhEQTVCQSxHQTZCQSw4QkE3QkEsR0E4QkEsbUJBOUJBLEdBK0JBLDZDQS9CQSxHQWdDQSx5REFoQ0EsR0FpQ0EsOEJBakNBLEdBa0NBLG1CQWxDQSxHQW1DQSxrREFuQ0EsR0FvQ0EsOERBcENBLEdBcUNBLDhCQXJDQSxHQXNDQSxtQkF0Q0EsR0F1Q0EsNkNBdkNBLEdBd0NBLHlEQXhDQSxHQXlDQSw4QkF6Q0EsR0EwQ0EsbUJBMUNBLEdBMkNBLGtEQTNDQSxHQTRDQSw4REE1Q0EsR0E2Q0EsOEJBN0NBLEdBOENBLG1CQTlDQSxHQStDQSxlQS9DQSxHQWdEQSxjQWhEQSxHQWlEQSxRQWxEQTtBQW1EQSxnQkFBSSxRQUFRLEVBQUUsUUFBRixDQUFXLFFBQVgsQ0FBWjtBQUNBLGtCQUFNLElBQU4sQ0FBVyxNQUFNLEVBQUMsT0FBTyxLQUFLLE9BQWIsRUFBTixDQUFYO0FBQ0g7OztvQ0FDUSxNLEVBQVEsTyxFQUFTO0FBQ3RCLG9CQUFRLEdBQVIsQ0FBWSxnQ0FBWixFQUE4QyxNQUE5Qzs7QUFFQSx1QkFBSyxHQUFMLENBQVMsT0FBTyxPQUFoQixFQUF5QixNQUF6QixFQUFpQyxJQUFqQyxDQUFzQyxVQUFDLElBQUQsRUFBVTtBQUNyRCx3QkFBUSxJQUFSO0FBQ00sYUFGRCxFQUVHLEtBRkgsQ0FFUyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIsc0JBQU0sSUFBSSxLQUFKLENBQVUsS0FBVixDQUFOO0FBQ0gsYUFKRDtBQUtIOzs7Ozs7UUFJRCxjLEdBQUEsYzs7O0FDblVKOztBQUVBOzs7Ozs7Ozs7O0FBRUEsSUFBSSxXQUFXO0FBQ1gsb0JBQWdCLDRCQURMO0FBRVgsNkJBQXlCLHNDQUZkO0FBR1gsd0JBQW9CLGlDQUhUO0FBSVgsd0JBQW9CLGlDQUpUO0FBS1gsd0JBQW9CLGlDQUxUO0FBTVgsdUJBQW1CLGdDQU5SO0FBT1gsbUJBQWU7QUFDWCxrQkFBVSxHQURDO0FBRVgsY0FBTTtBQUZLLEtBUEo7QUFXWCxzQkFBa0IsOEJBWFA7QUFZWCwrQkFBMkIsd0NBWmhCO0FBYVgsMEJBQXNCLG1DQWJYO0FBY1gscUJBQWlCO0FBQ2Isa0JBQVUsR0FERztBQUViLGtCQUFVO0FBRkc7QUFkTixDQUFmOztJQW9CTSxjO0FBQ0YsNEJBQVksT0FBWixFQUFxQjtBQUFBOztBQUNwQixhQUFLLEdBQUwsR0FBVyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsUUFBYixFQUF1QixPQUF2QixDQUFYO0FBQ0csYUFBSyxJQUFMO0FBQ047Ozs7K0JBQ1M7QUFDSCxvQkFBUSxHQUFSLENBQVkseUJBQVo7O0FBRUEsZ0JBQUksVUFBVSxFQUFFLEtBQUssR0FBTCxDQUFTLGNBQVgsQ0FBZDtBQUNBLGdCQUFJLFFBQVEsTUFBWixFQUFvQjtBQUNoQixvQkFBSSxhQUFhLFFBQVEsSUFBUixDQUFhLEtBQUssR0FBTCxDQUFTLHVCQUF0QixDQUFqQjtBQUFBLG9CQUNDLFNBQVMsUUFBUSxJQUFSLENBQWEsS0FBSyxHQUFMLENBQVMsa0JBQXRCLENBRFY7QUFBQSxvQkFFSSxRQUFRLFFBQVEsSUFBUixDQUFhLEtBQUssR0FBTCxDQUFTLGtCQUF0QixDQUZaO0FBQUEsb0JBR0ksUUFBUSxRQUFRLElBQVIsQ0FBYSxLQUFLLEdBQUwsQ0FBUyxrQkFBdEIsQ0FIWjtBQUFBLG9CQUlJLE9BQU8sUUFBUSxJQUFSLENBQWEsS0FBSyxHQUFMLENBQVMsaUJBQXRCLENBSlg7O0FBTUEsdUJBQU8sTUFBUCxDQUFjLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxLQUFLLEdBQUwsQ0FBUyxhQUF0QixFQUFxQztBQUMvQywrQkFBVyxVQURvQztBQUUvQywwQkFBTSxLQUZ5QztBQUcvQywwQkFBTSxLQUh5QztBQUkvQyx5QkFBSztBQUowQyxpQkFBckMsQ0FBZDtBQU1IOztBQUVELGdCQUFJLFlBQVksRUFBRSxLQUFLLEdBQUwsQ0FBUyxnQkFBWCxDQUFoQjtBQUNBLGdCQUFJLFVBQVUsTUFBZCxFQUFzQjtBQUNyQixvQkFBSSxjQUFhLFVBQVUsSUFBVixDQUFlLEtBQUssR0FBTCxDQUFTLHlCQUF4QixDQUFqQjtBQUFBLG9CQUNPLFVBQVMsVUFBVSxJQUFWLENBQWUsS0FBSyxHQUFMLENBQVMsb0JBQXhCLENBRGhCOztBQUdHLHdCQUFPLFFBQVAsQ0FBZ0IsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEtBQUssR0FBTCxDQUFTLGVBQXRCLEVBQXVDO0FBQ3RELCtCQUFXO0FBRDJDLGlCQUF2QyxDQUFoQjtBQUdIO0FBQ0o7Ozs7OztRQUlELGMsR0FBQSxjOzs7QUM3REo7O0FBRUE7Ozs7Ozs7Ozs7QUFFQSxJQUFJLFdBQVc7QUFDWCxjQUFVLHFCQURDO0FBRVgsa0JBQWMsMkJBRkg7QUFHWCxrQkFBYywyQkFISDtBQUlYLGtCQUFjLDJCQUpIO0FBS1gsWUFBUTtBQUNKLGtCQUFVLEdBRE47QUFFSixjQUFNO0FBRkY7QUFMRyxDQUFmOztJQVdNLGM7QUFDRiw0QkFBWSxPQUFaLEVBQXFCO0FBQUE7O0FBQ2pCLGFBQUssR0FBTCxHQUFXLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxRQUFiLEVBQXVCLE9BQXZCLENBQVg7QUFDQSxhQUFLLElBQUw7QUFDSDs7OzsrQkFDTTtBQUNILG9CQUFRLEdBQVIsQ0FBWSx5QkFBWjs7QUFFQSxnQkFBSSxVQUFVLEVBQUUsS0FBSyxHQUFMLENBQVMsUUFBWCxDQUFkOztBQUVBLGlCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksUUFBUSxNQUE1QixFQUFvQyxHQUFwQyxFQUEwQztBQUN0QyxvQkFBSSxTQUFTLFFBQVEsRUFBUixDQUFXLENBQVgsQ0FBYjs7QUFFQSxvQkFBSSxTQUFTLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFlBQXJCLENBQWI7QUFBQSxvQkFDSSxRQUFRLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFlBQXJCLENBRFo7QUFBQSxvQkFFSSxRQUFRLE9BQU8sSUFBUCxDQUFZLEtBQUssR0FBTCxDQUFTLFlBQXJCLENBRlo7O0FBSUEsdUJBQU8sUUFBUCxDQUFnQixFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsS0FBSyxHQUFMLENBQVMsTUFBdEIsRUFBOEI7QUFDMUMsMEJBQU0sS0FEb0M7QUFFMUMsMEJBQU07QUFGb0MsaUJBQTlCLENBQWhCO0FBSUg7QUFDSjs7Ozs7O1FBSUQsYyxHQUFBLGM7OztBQ3pDSjs7QUFFQTs7Ozs7Ozs7O0FBRUE7Ozs7SUFHTSxVO0FBQ0Ysd0JBQVksT0FBWixFQUFxQjtBQUFBOztBQUNqQixhQUFLLEdBQUwsR0FBVyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxrQ0FBRixFQUFiLEVBQThCLE9BQTlCLENBQVg7QUFDQSxhQUFLLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxhQUFLLElBQUw7QUFDSDs7OzsrQkFDTTtBQUFBOztBQUNILG9CQUFRLEdBQVIsQ0FBWSxxQkFBWjs7QUFFQSxpQkFBSyxPQUFMLENBQWEsRUFBYixDQUFnQixRQUFoQixFQUEwQixZQUFNO0FBQzVCLHNCQUFLLFVBQUw7QUFDSCxhQUZEO0FBR0EsaUJBQUssVUFBTDtBQUNIOzs7cUNBQ1k7QUFDVCxnQkFBSSxRQUFRLEtBQUssUUFBTCxFQUFaO0FBQUEsZ0JBQ0ksUUFBUSxLQUFLLFFBQUwsQ0FBYyxLQUFkLENBRFo7O0FBR0EsZ0JBQUksQ0FBQyxLQUFLLFlBQU4sSUFBc0IsVUFBVSxLQUFLLFlBQXpDLEVBQXVEO0FBQ25ELG9CQUFJLEtBQUssWUFBVCxFQUF1QjtBQUNuQiw0QkFBUSxHQUFSLDBCQUFtQyxLQUFLLFlBQXhDO0FBQ0EseUJBQUssT0FBTCxDQUFhLE9BQWIsQ0FBcUIsb0JBQW9CLEtBQUssWUFBOUM7QUFDSDs7QUFFRCx3QkFBUSxHQUFSLHlCQUFrQyxLQUFsQztBQUNBLHFCQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLG1CQUFtQixLQUF4Qzs7QUFFQSxvQkFBSSxLQUFLLFlBQVQsRUFBdUI7QUFDbkIseUJBQUssT0FBTCxDQUFhLE9BQWIsQ0FBcUIsbUJBQXJCLEVBQTBDLEVBQUUsT0FBTyxLQUFULEVBQTFDO0FBQ0gsaUJBRkQsTUFFTztBQUNILHlCQUFLLE9BQUwsQ0FBYSxPQUFiLENBQXFCLGlCQUFyQixFQUF3QyxFQUFFLE9BQU8sS0FBVCxFQUF4QztBQUNIOztBQUVELHFCQUFLLFlBQUwsR0FBb0IsS0FBcEI7QUFDSDtBQUNKOzs7bUNBQ1U7QUFDUCxtQkFBTyxLQUFLLE9BQUwsQ0FBYSxLQUFiLEVBQVA7QUFDSDs7O2lDQUNRLEssRUFBTztBQUNaLGdCQUFJLFFBQVEsS0FBSyxNQUFMLENBQVksQ0FBWixDQUFaO0FBQ0EsaUJBQUssSUFBSSxJQUFJLEtBQUssTUFBTCxDQUFZLE1BQVosR0FBcUIsQ0FBbEMsRUFBcUMsSUFBSSxDQUF6QyxFQUE0QyxHQUE1QyxFQUFrRDtBQUM5QyxvQkFBSSxRQUFRLEtBQUssR0FBTCxDQUFTLFdBQVQsQ0FBcUIsS0FBSyxNQUFMLENBQVksSUFBSSxDQUFoQixDQUFyQixDQUFSLElBQW9ELFNBQVMsS0FBSyxHQUFMLENBQVMsV0FBVCxDQUFxQixLQUFLLE1BQUwsQ0FBWSxDQUFaLENBQXJCLENBQWpFLEVBQXVHO0FBQ25HLDRCQUFRLEtBQUssTUFBTCxDQUFZLENBQVosQ0FBUjtBQUNBO0FBQ0g7QUFDSjtBQUNELG1CQUFPLEtBQVA7QUFDSDs7OzRCQUNZO0FBQUE7O0FBQ1QsZ0JBQUksQ0FBQyxLQUFLLFFBQVYsRUFBb0I7QUFDaEIscUJBQUssUUFBTCxHQUFnQixFQUFoQjtBQUNBLHFCQUFLLElBQUksQ0FBVCxJQUFjLEtBQUssR0FBTCxDQUFTLFdBQXZCLEVBQW9DO0FBQ2hDLHlCQUFLLFFBQUwsQ0FBYyxJQUFkLENBQW1CLENBQW5CO0FBQ0g7QUFDRCxxQkFBSyxRQUFMLENBQWMsSUFBZCxDQUFtQixVQUFDLENBQUQsRUFBSSxDQUFKLEVBQVU7QUFDekIsMkJBQVEsT0FBSyxHQUFMLENBQVMsV0FBVCxDQUFxQixDQUFyQixJQUEwQixPQUFLLEdBQUwsQ0FBUyxXQUFULENBQXFCLENBQXJCLENBQTNCLEdBQXNELENBQUMsQ0FBdkQsR0FBNkQsT0FBSyxHQUFMLENBQVMsV0FBVCxDQUFxQixDQUFyQixJQUEwQixPQUFLLEdBQUwsQ0FBUyxXQUFULENBQXFCLENBQXJCLENBQTNCLEdBQXNELENBQXRELEdBQTBELENBQTdIO0FBQ0gsaUJBRkQ7QUFHSDtBQUNELG1CQUFPLEtBQUssUUFBWjtBQUNIOzs7NEJBQ2E7QUFDVixnQkFBSSxDQUFDLEtBQUssU0FBVixFQUFxQjtBQUNqQixxQkFBSyxTQUFMLEdBQWlCLEVBQUUsTUFBRixDQUFqQjtBQUNIO0FBQ0QsbUJBQU8sS0FBSyxTQUFaO0FBQ0g7Ozs7OztRQUtELFUsR0FBQSxVOzs7QUM5RUo7O0FBRUE7Ozs7Ozs7Ozs7QUFFQSxJQUFJLFdBQVc7QUFDWCx3QkFBb0IsaUJBRFQ7QUFFWCxzQkFBa0I7QUFGUCxDQUFmOztJQUtNLFU7QUFDRix3QkFBWSxPQUFaLEVBQXFCO0FBQUE7O0FBQ2pCLGFBQUssR0FBTCxHQUFXLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxRQUFiLEVBQXVCLE9BQXZCLENBQVg7QUFDQSxhQUFLLElBQUw7QUFDSDs7OzsrQkFDTTtBQUNILG9CQUFRLEdBQVIsQ0FBWSxxQkFBWjs7QUFFQTs7O0FBR0EsZ0JBQUksbUJBQW1CLFlBQVksS0FBSyxPQUFqQixFQUEwQixFQUExQixDQUF2QjtBQUNIOzs7a0NBQ1M7QUFDTixnQkFBSSxjQUFjLEVBQUUsaUJBQUYsQ0FBbEI7QUFBQSxnQkFDSSxZQUFZLEVBQUUsZUFBRixDQURoQjs7QUFHQSxnQkFBSSxrQkFBa0IsVUFBVSxNQUFWLEVBQXRCO0FBQUEsZ0JBQ0ksa0JBQWtCLGdCQUFnQixHQUR0Qzs7QUFHQSxnQkFBSSxFQUFFLE1BQUYsRUFBVSxTQUFWLE1BQTBCLGVBQTlCLEVBQWlEO0FBQzdDLDBCQUFVLEdBQVYsQ0FBYyxZQUFkLEVBQTRCLFFBQTVCO0FBQ0EsNEJBQVksV0FBWixDQUF3QixXQUF4QjtBQUNILGFBSEQsTUFHTztBQUNILDRCQUFZLFFBQVosQ0FBcUIsV0FBckI7QUFDQSwwQkFBVSxHQUFWLENBQWMsWUFBZCxFQUE0QixTQUE1QjtBQUNIO0FBQ0o7Ozs7OztRQUlELFUsR0FBQSxVOzs7QUN4Q0o7O0FBRUE7Ozs7Ozs7Ozs7QUFFQSxJQUFJLFdBQVc7QUFDWCxrQkFBYyx1QkFESDtBQUVYLGtCQUFjLHVCQUZIO0FBR1gscUJBQWlCLDBCQUhOO0FBSVgscUJBQWlCO0FBSk4sQ0FBZjs7SUFPTSxVO0FBQ0Ysd0JBQVksT0FBWixFQUFxQjtBQUFBOztBQUNqQixhQUFLLEdBQUwsR0FBVyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsUUFBYixFQUF1QixPQUF2QixDQUFYO0FBQ0EsYUFBSyxJQUFMO0FBQ0g7Ozs7K0JBQ007QUFDSCxvQkFBUSxHQUFSLENBQVkscUJBQVo7O0FBRUEsZ0JBQUksV0FBVyxFQUFFLEtBQUssR0FBTCxDQUFTLGVBQVgsQ0FBZjtBQUFBLGdCQUNJLFFBQVEsRUFBRSxLQUFLLEdBQUwsQ0FBUyxZQUFYLENBRFo7QUFBQSxnQkFFSSxRQUFRLEVBQUUsS0FBSyxHQUFMLENBQVMsWUFBWCxDQUZaOztBQUlBLHFCQUFTLEVBQVQsQ0FBWSxPQUFaLEVBQXFCLFVBQVMsQ0FBVCxFQUFZO0FBQzdCLHNCQUFNLFdBQU4sQ0FBa0IsV0FBbEI7QUFDQSx5QkFBUyxXQUFULENBQXFCLFdBQXJCO0FBQ0gsYUFIRDtBQUlIOzs7Ozs7UUFJRCxVLEdBQUEsVTs7O0FDL0JKOzs7OztBQUdBLFNBQVMsaUJBQVQsR0FBNkI7QUFDekIsUUFBSSxZQUFZLEVBQWhCO0FBQ0EsUUFBSSxPQUFPLFFBQVAsQ0FBZ0IsUUFBaEIsR0FBMkIsS0FBM0IsQ0FBaUMsZUFBakMsQ0FBSixFQUF1RDtBQUNuRCxvQkFBWSxPQUFPLEVBQW5CO0FBQ0g7QUFDRCxXQUFPLFNBQVA7QUFDSDs7QUFFRCxJQUFJLFdBQVcsRUFBZjs7QUFFQSxJQUFJLE9BQU87QUFDUCxVQUFNLE1BREM7QUFFUCxTQUFLLFFBRkU7QUFHUCxjQUFVLE1BSEg7QUFJUCxhQUFTLEtBSkY7QUFLUCxpQkFBYSxJQUxOO0FBTVAsZ0JBQVk7QUFOTCxDQUFYOztBQVNBLElBQUksWUFBWSxpQkFBaEI7O0FBRUEsSUFBSSxjQUFjO0FBQ2QsU0FBSyxRQURTO0FBRWQsUUFBSSxJQUZVO0FBR2QsT0FBRyxJQUhXO0FBSWQsT0FBRyxJQUpXO0FBS2QsT0FBRyxHQUxXO0FBTWQsUUFBSSxHQU5VO0FBT2QsU0FBSztBQVBTLENBQWxCOztBQVdBLElBQUksZ0JBQWdCLFNBQVMsSUFBVCxDQUFjLE9BQWQsQ0FBc0IsV0FBdEIsQ0FBcEI7O1FBSUksUSxHQUFBLFE7UUFDQSxJLEdBQUEsSTtRQUNBLFcsR0FBQSxXO1FBQ0EsUyxHQUFBLFM7UUFFQSxhLEdBQUEsYTs7O0FDNUNKOztBQUVBOzs7Ozs7Ozs7OztBQUVBOzs7O0lBR00sSTs7Ozs7Ozs0QkFDUyxHLEVBQUssSSxFQUFNLE8sRUFBUztBQUMzQixvQkFBUSxHQUFSLG1CQUE0QixHQUE1QixFQUFtQyxJQUFuQzs7QUFFQSxnQkFBSSxNQUFNLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxjQUFiLEVBQW1CLE9BQW5CLENBQVY7O0FBRUEsZ0JBQUksSUFBSSxVQUFSLEVBQW9CO0FBQ2hCLG9CQUFJLGdCQUFnQixLQUFwQixFQUEyQjtBQUN2Qix5QkFBSyxJQUFMLENBQVUsRUFBRSxNQUFNLFNBQVIsRUFBbUIsT0FBTyxJQUFJLFVBQTlCLEVBQVY7QUFDSCxpQkFGRCxNQUVPLElBQUksZ0JBQWdCLFFBQXBCLEVBQThCO0FBQ2pDLHlCQUFLLE1BQUwsQ0FBWSxTQUFaLEVBQXVCLElBQUksVUFBM0I7QUFDSCxpQkFGTSxNQUVBLElBQUksZ0JBQWdCLGVBQXBCLEVBQXFDO0FBQ3hDLHlCQUFLLE1BQUwsQ0FBWSxTQUFaLEVBQXVCLElBQUksVUFBM0I7QUFDSCxpQkFGTSxNQUVBLElBQUksT0FBTyxJQUFQLEtBQWdCLFFBQXBCLEVBQThCO0FBQ2pDLHdCQUFJLFFBQVEsRUFBWixFQUFnQixRQUFRLEdBQVI7QUFDaEIsNEJBQVEsYUFBVyxJQUFJLFVBQXZCO0FBQ0gsaUJBSE0sTUFHQSxJQUFJLFFBQU8sSUFBUCx5Q0FBTyxJQUFQLE9BQWdCLFFBQXBCLEVBQThCO0FBQ2pDLHlCQUFLLE9BQUwsR0FBZSxJQUFJLFVBQW5CO0FBQ0gsaUJBRk0sTUFFQTtBQUNILDBCQUFNLElBQUksS0FBSixDQUFVLCtDQUFWLENBQU47QUFDSDtBQUNKO0FBQ0QsbUJBQU8sSUFBSSxVQUFYOztBQUVBLGdCQUFJLENBQUMsTUFBTSxJQUFOLENBQVcsR0FBWCxDQUFMLEVBQXNCO0FBQ2xCLG9CQUFJLEdBQUosSUFBVyxHQUFYO0FBQ0gsYUFGRCxNQUVPO0FBQ0gsb0JBQUksR0FBSixHQUFVLEdBQVY7QUFDSDtBQUNELGdCQUFJLENBQUMsTUFBTSxJQUFOLENBQVcsSUFBSSxHQUFmLENBQUwsRUFBMEI7QUFDdEIsb0JBQUksR0FBSixJQUFXLEdBQVg7QUFDSDs7QUFFRCxnQkFBSSxnQkFBZ0IsUUFBaEIsSUFBNEIsZ0JBQWdCLGVBQWhELEVBQWlFO0FBQzdELG9CQUFJLFdBQUosR0FBa0IsS0FBbEI7QUFDQSxvQkFBSSxXQUFKLEdBQWtCLEtBQWxCO0FBQ0g7O0FBRUQsZ0JBQUksSUFBSixHQUFXLElBQVg7O0FBRUEsZ0JBQUksVUFBVSxJQUFJLE9BQUosQ0FBWSxVQUFTLE9BQVQsRUFBa0IsTUFBbEIsRUFBMEI7QUFDaEQsb0JBQUksT0FBSixHQUFjLFVBQVMsSUFBVCxFQUFlLFVBQWYsRUFBMkIsS0FBM0IsRUFBa0M7QUFDNUMsNEJBQVEsSUFBUixFQUFjLEtBQWQ7QUFDSCxpQkFGRDtBQUdBLG9CQUFJLEtBQUosR0FBWSxVQUFTLEtBQVQsRUFBZ0I7QUFDeEIsMkJBQU8sTUFBTSxVQUFiLEVBQXlCLEtBQXpCO0FBQ0gsaUJBRkQ7QUFHQSxrQkFBRSxJQUFGLENBQU8sR0FBUDtBQUNILGFBUmEsQ0FBZDs7QUFVQSxtQkFBTyxPQUFQO0FBQ0g7Ozs7OztRQUlJLEksR0FBQSxJOzs7QUM5RFQ7Ozs7Ozs7QUFFQTs7QUFDQTs7QUFHQSxJQUFNLFdBQVcsQ0FDYixJQUFJLDRCQUFKLEVBRGEsRUFFYixJQUFJLGdDQUFKLEVBRmEsQ0FBakI7O1FBT0ksUSxHQUFBLFE7OztBQ2JKOzs7Ozs7Ozs7O0lBR00sZTtBQUNGLDZCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFDakIsYUFBSyxHQUFMLEdBQVcsT0FBWDtBQUNIOzs7O29DQUNXO0FBQ1Isa0JBQU0sSUFBSSxLQUFKLENBQVUsaUNBQVYsQ0FBTjtBQUNIOzs7NEJBQ1U7QUFDUCxrQkFBTSxJQUFJLEtBQUosQ0FBVSxpQ0FBVixDQUFOO0FBQ0g7Ozs0QkFDYTtBQUNWLGdCQUFJLENBQUMsS0FBSyxTQUFWLEVBQXFCO0FBQ2pCLHFCQUFLLFNBQUwsR0FBaUIsS0FBSyxHQUFMLENBQVMsT0FBVCxJQUFvQixPQUFPLEtBQUssR0FBTCxDQUFTLFdBQWhCLENBQXJDO0FBQ0g7QUFDRCxtQkFBTyxLQUFLLFNBQVo7QUFDSDs7Ozs7O1FBS0QsZSxHQUFBLGU7OztBQ3ZCSjs7QUFFQTs7Ozs7Ozs7O0FBRUE7Ozs7Ozs7O0lBR00sZTs7O0FBQ0YsNkJBQVksT0FBWixFQUFxQjtBQUFBOztBQUFBLGlJQUNYLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLGFBQWEsSUFBZixFQUFiLEVBQW9DLE9BQXBDLENBRFc7QUFFcEI7Ozs7a0NBQ1MsSSxFQUFpQjtBQUFBLGdCQUFYLE1BQVcsdUVBQUosRUFBSTs7QUFDdkIsb0JBQVEsR0FBUixvQ0FBNkMsSUFBN0MsRUFBcUQsTUFBckQ7O0FBRUEsZ0JBQUk7QUFDQSxxQkFBSyxPQUFMLENBQWEsTUFBYixFQUFxQixPQUFyQixFQUE4QixJQUE5QixFQUFvQyxNQUFwQztBQUNBLHdCQUFRLElBQVIsK0JBQXlDLElBQXpDO0FBQ0gsYUFIRCxDQUdFLE9BQU8sR0FBUCxFQUFZO0FBQ1Ysd0JBQVEsS0FBUiw0Q0FBc0QsSUFBdEQsRUFBOEQsR0FBOUQ7QUFDSDtBQUNKOzs7a0NBQ1MsTSxFQUFRO0FBQ2Qsb0JBQVEsR0FBUixDQUFZLCtCQUFaLEVBQTZDLE1BQTdDOztBQUVBLGdCQUFJO0FBQ0EscUJBQUssT0FBTCxDQUFhLEtBQWIsRUFBb0IsTUFBcEI7QUFDQSx3QkFBUSxJQUFSLENBQWEsaURBQWI7QUFDSCxhQUhELENBR0UsT0FBTyxHQUFQLEVBQVk7QUFDVix3QkFBUSxLQUFSLENBQWMsdUNBQWQsRUFBdUQsR0FBdkQ7QUFDSDtBQUNKOzs7NEJBQ1U7QUFDUCxtQkFBTyxrQkFBUDtBQUNIOzs7O0VBMUJ5QixnQzs7UUErQjFCLGUsR0FBQSxlOzs7QUN0Q0o7O0FBRUE7Ozs7Ozs7OztBQUVBOztBQUNBOzs7Ozs7OztJQUdNLGE7OztBQUNGLDJCQUFZLE9BQVosRUFBcUI7QUFBQTs7QUFBQSw2SEFDWCxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxhQUFhLG1CQUFmLEVBQWIsRUFBeUMsT0FBekMsQ0FEVztBQUVwQjs7OztrQ0FDUyxJLEVBQWlCO0FBQUEsZ0JBQVgsTUFBVyx1RUFBSixFQUFJOztBQUN2QixvQkFBUSxHQUFSLGtDQUEyQyxJQUEzQyxFQUFtRCxNQUFuRDs7QUFFQSxnQkFBSTtBQUNBLHFCQUFLLE9BQUwsQ0FBYSxTQUFiLENBQXVCLElBQXZCLEVBQTZCLE1BQTdCO0FBQ0Esd0JBQVEsSUFBUiw2QkFBdUMsSUFBdkM7QUFDSCxhQUhELENBR0UsT0FBTyxHQUFQLEVBQVk7QUFDVix3QkFBUSxLQUFSLDBDQUFvRCxJQUFwRCxFQUE0RCxHQUE1RDtBQUNIO0FBQ0o7OztrQ0FDUyxNLEVBQVE7QUFDZCxvQkFBUSxHQUFSLENBQVksNkJBQVosRUFBMkMsTUFBM0M7O0FBRUEsZ0JBQUk7QUFDQSxxQkFBSyxPQUFMLENBQWEsTUFBYixDQUFvQixNQUFwQjtBQUNBLHdCQUFRLElBQVIsQ0FBYSwrQ0FBYjtBQUNILGFBSEQsQ0FHRSxPQUFPLEdBQVAsRUFBWTtBQUNWLHdCQUFRLEtBQVIsQ0FBYyxxQ0FBZCxFQUFxRCxHQUFyRDtBQUNIO0FBQ0o7Ozs0QkFDVTtBQUNQLG1CQUFPLGdCQUFQO0FBQ0g7Ozs7RUExQnVCLGdDOztRQStCeEIsYSxHQUFBLGEiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIndXNlIHN0cmljdCc7XG5cbi8qIGdsb2JhbCAkICovXG5cbmltcG9ydCB7IGludGVyZmFjZU5hbWUgfSBmcm9tICcuL3NldHRpbmdzJztcblxuaW1wb3J0IHsgQWpheEZvcm0gfSBmcm9tICcuL21vZHVsZXMvYWpheC1mb3JtJztcbmltcG9ydCB7IEdvYWwgfSBmcm9tICcuL21vZHVsZXMvZ29hbCc7XG5pbXBvcnQgeyBSZXNwb25zaXZlIH0gZnJvbSAnLi9tb2R1bGVzL3Jlc3BvbnNpdmUnO1xuaW1wb3J0IHsgVG9nZ2xlTWVudSB9IGZyb20gJy4vbW9kdWxlcy90b2dnbGUtbWVudSc7XG5pbXBvcnQgeyBTdGlja3lNZW51IH0gZnJvbSAnLi9tb2R1bGVzL3N0aWNreS1tZW51JztcbmltcG9ydCB7IEhvbWVTbGlkZXIgfSBmcm9tICcuL21vZHVsZXMvaG9tZS1zbGlkZXInO1xuaW1wb3J0IHsgQWR2YW50YWdlc1NsaWRlciB9IGZyb20gJy4vbW9kdWxlcy9hZHZhbnRhZ2VzLXNsaWRlcic7XG5pbXBvcnQgeyBQcm9kdWN0c1NsaWRlciB9IGZyb20gJy4vbW9kdWxlcy9wcm9kdWN0cy1zbGlkZXInO1xuaW1wb3J0IHsgUHJvZHVjdEdhbGxlcnkgfSBmcm9tICcuL21vZHVsZXMvcHJvZHVjdC1nYWxsZXJ5JztcbmltcG9ydCB7IExvbWJhcmRNYXAgfSBmcm9tICcuL21vZHVsZXMvbG9tYmFyZC1tYXAnO1xuaW1wb3J0IHsgTG9jYXRpb25TZWxlY3QgfSBmcm9tICcuL21vZHVsZXMvbG9jYXRpb24tc2VsZWN0JztcbmltcG9ydCB7IFBlcnNvbmFsT2ZmaWNlIH0gZnJvbSAnLi9tb2R1bGVzL3BlcnNvbmFsLW9mZmljZSc7XG5cbmNsYXNzIFByb2oge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLmluaXQoKTtcbiAgICB9XG4gICAgaW5pdCgpIHtcbiAgICAgICAgdGhpcy5pbnRlcmZhY2VOYW1lID0gaW50ZXJmYWNlTmFtZTtcblxuICAgICAgICBpZiAoaW50ZXJmYWNlTmFtZSA9PSAnaG9tZV9wYWdlJykge1xuICAgICAgICAgICAgdGhpcy5ob21lU2xpZGVyID0gbmV3IEhvbWVTbGlkZXIoKTtcbiAgICAgICAgICAgIHRoaXMuYWR2YW50YWdlc1NsaWRlciA9IG5ldyBBZHZhbnRhZ2VzU2xpZGVyKCk7XG4gICAgICAgICAgICB0aGlzLnN0aWNreU1lbnUgPSBuZXcgU3RpY2t5TWVudSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGludGVyZmFjZU5hbWUgPT0gJ2xvbWJhcmQnKSB7XG4gICAgICAgICAgICB0aGlzLmxvbWJhcmRNYXAgPSBuZXcgTG9tYmFyZE1hcCgpO1xuICAgICAgICB9IFxuICAgICAgICBpZiAoaW50ZXJmYWNlTmFtZSA9PSAncGVyc29uYWxfb2ZmaWNlJykge1xuICAgICAgICAgICAgdGhpcy5wZXJzb25hbE9mZmljZSA9IG5ldyBQZXJzb25hbE9mZmljZSgpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMudG9nZ2xlTWVudSA9IG5ldyBUb2dnbGVNZW51KCk7XG4gICAgICAgIHRoaXMucHJvZHVjdHNTbGlkZXIgPSBuZXcgUHJvZHVjdHNTbGlkZXIoKTtcbiAgICAgICAgdGhpcy5wcm9kdWN0R2FsbGVyeSA9IG5ldyBQcm9kdWN0R2FsbGVyeSgpO1xuICAgICAgICB0aGlzLmxvY2F0aW9uU2VsZWN0ID0gbmV3IExvY2F0aW9uU2VsZWN0KCk7XG5cbiAgICAgICAgdGhpcy5hamF4Rm9ybSA9IG5ldyBBamF4Rm9ybSgpO1xuICAgICAgICB0aGlzLmdvYWwgPSBuZXcgR29hbCh7XG4gICAgICAgICAgICBnb2FsczogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0b3I6ICcuanMtZ29hbF9fbGluaycsXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50OiAnY2xpY2snLFxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQod2luZG93KS50cmlnZ2VyKCdtYWluOnJlYWNoR29hbCcsICQodGhpcykuZGF0YSgpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0b3I6ICcuanMtZ29hbF9fZm9ybScsXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50OiAnc3VibWl0JyxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHdpbmRvdykudHJpZ2dlcignbWFpbjpyZWFjaEdvYWwnLCAkKHRoaXMpLmRhdGEoKSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdG9yOiB3aW5kb3csXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50OiAnZ29hbCBhamF4Rm9ybTpnb2FsJyxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlcjogZnVuY3Rpb24oZSwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJCh3aW5kb3cpLnRyaWdnZXIoJ21haW46cmVhY2hHb2FsJywgZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMucmVzcG9uc2l2ZSA9IG5ldyBSZXNwb25zaXZlKCk7XG5cbiAgICB9XG59XG5cbiQoZnVuY3Rpb24oKSB7XG4gICAgd2luZG93LnByb2ogPSBuZXcgUHJvaigpO1xufSk7XG4iLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKiBnbG9iYWwgJCAqL1xyXG5cclxubGV0IHNldHRpbmdzID0ge1xyXG4gICAgc2VsZWN0b3I6ICcuanMtYWR2YW50YWdlcy1zbGlkZXInLFxyXG4gICAgaXRlbVNlbGVjdG9yOiAnLmpzLWFkdmFudGFnZXMtc2xpZGVyX19pdGVtJyxcclxuICAgIHByZXZTZWxlY3RvcjogJy5qcy1hZHZhbnRhZ2VzLXNsaWRlcl9fcHJldicsXHJcbiAgICBuZXh0U2VsZWN0b3I6ICcuanMtYWR2YW50YWdlcy1zbGlkZXJfX25leHQnLFxyXG4gICAgc2xpZGVyOiB7XHJcbiAgICAgICAgZHVyYXRpb246IDQwMCxcclxuICAgICAgICBsb29wOiB0cnVlXHJcbiAgICB9XHJcbn07XHJcblxyXG5jbGFzcyBBZHZhbnRhZ2VzU2xpZGVyIHtcclxuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMpIHtcclxuICAgICAgICB0aGlzLm9wdCA9ICQuZXh0ZW5kKHt9LCBzZXR0aW5ncywgb3B0aW9ucyk7XHJcbiAgICAgICAgdGhpcy5pbml0KCk7XHJcbiAgICB9XHJcbiAgICBpbml0KCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdbIEFkdmFudGFnZXNTbGlkZXIvaW5pdCBdJyk7XHJcblxyXG4gICAgICAgIGxldCAkYmxvY2sgPSAkKHRoaXMub3B0LnNlbGVjdG9yKTtcclxuICAgICAgICBpZiAoJGJsb2NrLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBsZXQgJGl0ZW1zID0gJGJsb2NrLmZpbmQodGhpcy5vcHQuaXRlbVNlbGVjdG9yKSxcclxuICAgICAgICAgICAgICAgICRwcmV2ID0gJGJsb2NrLmZpbmQodGhpcy5vcHQucHJldlNlbGVjdG9yKSxcclxuICAgICAgICAgICAgICAgICRuZXh0ID0gJGJsb2NrLmZpbmQodGhpcy5vcHQubmV4dFNlbGVjdG9yKTtcclxuXHJcbiAgICAgICAgICAgICRpdGVtcy5zY3JvbGxlcigkLmV4dGVuZCh7fSwgdGhpcy5vcHQuc2xpZGVyLCB7XHJcbiAgICAgICAgICAgICAgICBwcmV2OiAkcHJldixcclxuICAgICAgICAgICAgICAgIG5leHQ6ICRuZXh0XHJcbiAgICAgICAgICAgIH0pKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCB7XHJcbiAgICBBZHZhbnRhZ2VzU2xpZGVyXHJcbn07XHJcbiIsIid1c2Ugc3RyaWN0JztcblxuLyogZ2xvYmFsICQgKi9cblxuaW1wb3J0IHsgQWpheCB9IGZyb20gJ3V0aWxzL2FqYXgnO1xuaW1wb3J0IHsgZmFuY3lib3ggfSBmcm9tICdzZXR0aW5ncyc7XG5cbmltcG9ydCB7IEZvcm0gfSBmcm9tICcuL2FqYXgtZm9ybS9mb3JtJztcblxuXG5sZXQgc2V0dGluZ3MgPSB7XG4gICAgZm9ybVNlbGVjdG9yOiAnLmpzLWFqYXgtZm9ybV9fZm9ybScsXG4gICAgbGlua1NlbGVjdG9yOiAnLmpzLWFqYXgtZm9ybV9fbGluaycsXG4gICAgZmFuY3lib3g6IHt9LFxuICAgIGZvcm06IHt9XG59O1xuXG5cbmNsYXNzIEFqYXhGb3JtIHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0ID0gJC5leHRlbmQodHJ1ZSwge30sIHNldHRpbmdzLCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuICAgIGluaXQoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIEFqYXhGb3JtL2luaXQgXScpO1xuXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcblxuICAgICAgICAkKGRvY3VtZW50LmJvZHkpLm9uKCdjbGljaycsIHRoaXMub3B0LmxpbmtTZWxlY3RvciwgZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICBsZXQgJGVsID0gJCh0aGlzKTtcbiAgICAgICAgICAgIHNlbGYuZ2V0Rm9ybSgkLmV4dGVuZCh7fSwgeyBocmVmOiAkZWwuYXR0cignaHJlZicpIH0sICRlbC5kYXRhKCkpKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJCh0aGlzLm9wdC5mb3JtU2VsZWN0b3IpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiAoIXRoaXMuX19hamF4X2Zvcm0pIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9fYWpheF9mb3JtID0gbmV3IEZvcm0odGhpcywgc2VsZi5vcHQuZm9ybSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQod2luZG93KS5vbignbWFpbjppbml0QWpheEZvcm0nLCAoKSA9PiB7XG4gICAgICAgICAgICAkKHRoaXMub3B0LmZvcm1TZWxlY3RvcikuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuX19hamF4X2Zvcm0pIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fX2FqYXhfZm9ybSA9IG5ldyBGb3JtKHRoaXMsIHNlbGYub3B0LmZvcm0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICAkKHdpbmRvdykub24oJ2FqYXhGb3JtOmZvcm06Z29hbCcsIChlLCBkYXRhKSA9PiB7XG4gICAgICAgICAgICAkKHdpbmRvdykudHJpZ2dlcignYWpheEZvcm06Z29hbCcsIGRhdGEpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgZ2V0Rm9ybShwYXJhbXMpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1sgQWpheEZvcm0vZ2V0Rm9ybSBdJywgcGFyYW1zKTtcblxuICAgICAgICBBamF4LmdldCgnZ2V0X2Zvcm0nLCBwYXJhbXMpLnRoZW4oKGRhdGEpID0+IHtcbiAgICAgICAgICAgIGlmIChkYXRhLm9rKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zaG93Rm9ybShkYXRhLmNvbnRlbnQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdDYW5cXCd0IGdldCBmb3JtJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pLmNhdGNoKGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3IpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgc2hvd0Zvcm0oY29udGVudCkge1xuICAgICAgICBjb25zb2xlLmxvZygnWyBBamF4Rm9ybS9zaG93Rm9ybSBdJyk7XG5cbiAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuXG4gICAgICAgICQuZmFuY3lib3goJC5leHRlbmQodHJ1ZSwge30sIGZhbmN5Ym94LCB0aGlzLm9wdC5mYW5jeWJveCwge1xuICAgICAgICAgICAgdHlwZTogJ2h0bWwnLFxuICAgICAgICAgICAgY29udGVudDogY29udGVudCxcbiAgICAgICAgICAgIGFmdGVyU2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgbGV0IGZvcm0gPSB0aGlzLmlubmVyLmZpbmQoc2VsZi5vcHQuZm9ybVNlbGVjdG9yKS5nZXQoMCk7XG4gICAgICAgICAgICAgICAgZm9ybS5fX2FqYXhfZm9ybSA9IG5ldyBGb3JtKGZvcm0sIHNlbGYub3B0LmZvcm0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KSk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCB7XG4gICAgQWpheEZvcm1cbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbi8qIGdsb2JhbCAkICovXG5cbmltcG9ydCB7IEFqYXggfSBmcm9tICd1dGlscy9hamF4JztcblxuXG5sZXQgc2V0dGluZ3MgPSB7XG4gICAgZmllbGRTZWxlY3RvcjogJy5qcy1hamF4LWZvcm1fX2ZpZWxkJyxcbiAgICBmaWVsZE5hbWVQcmVmaXg6ICcuanMtYWpheC1mb3JtX19maWVsZC0tJyxcbiAgICBjb250ZW50U2VsZWN0b3I6ICcuanMtYWpheC1mb3JtX19jb250ZW50JyxcbiAgICBlcnJvckNsYXNzOiAnaXMtZXJyb3InXG59O1xuXG5cbmNsYXNzIEZvcm0ge1xuICAgIGNvbnN0cnVjdG9yKGVsLCBvcHRpb25zKSB7XG4gICAgICAgIHRoaXMuZWwgPSBlbDtcbiAgICAgICAgdGhpcy5vcHQgPSAkLmV4dGVuZCh7fSwgc2V0dGluZ3MsIG9wdGlvbnMpO1xuICAgICAgICB0aGlzLmluaXQoKTtcbiAgICB9XG4gICAgaW5pdCgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1sgRm9ybS9pbml0IF0nLCB0aGlzLmVsKTtcblxuICAgICAgICB0aGlzLiRmb3JtID0gJCh0aGlzLmVsKTtcbiAgICAgICAgdGhpcy4kZmllbGRzID0gdGhpcy5vcHQuJGZpZWxkcyB8fCAkKHRoaXMub3B0LmZpZWxkU2VsZWN0b3IsIHRoaXMuZWwpO1xuICAgICAgICB0aGlzLiRjb250ZW50ID0gdGhpcy5vcHQuJGNvbnRlbnQgfHwgJCh0aGlzLm9wdC5jb250ZW50U2VsZWN0b3IpLmZpbHRlcignW2RhdGEtZm9ybV9pZD1cIicgKyB0aGlzLiRmb3JtLmRhdGEoJ2Zvcm1faWQnKSArICdcIl0nKTtcblxuICAgICAgICB0aGlzLl9pbml0RmllbGRzKCk7XG4gICAgICAgIHRoaXMuX2luaXRFdmVudHMoKTtcblxuICAgICAgICBpZiAoJC5mYW5jeWJveC5pc09wZW4pIHtcbiAgICAgICAgICAgICQuZmFuY3lib3gudXBkYXRlKCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgc3VibWl0KCkge1xuICAgICAgICBjb25zb2xlLmxvZygnWyBGb3JtL3N1Ym1pdCBdJywgdGhpcy5lbCk7XG5cbiAgICAgICAgaWYgKCF0aGlzLmlzTG9hZGluZykge1xuICAgICAgICAgICAgdGhpcy5fYmVmb3JlU3VibWl0KCk7XG4gICAgICAgICAgICB0aGlzLl9wcm9jZXNzU3VibWl0KCk7XG4gICAgICAgICAgICB0aGlzLl9hZnRlclN1Ym1pdCgpO1xuICAgICAgICB9XG4gICAgfVxuICAgIF9pbml0RmllbGRzKCkge1xuICAgICAgICB0aGlzLiRmaWVsZHMuZmlsdGVyKHRoaXMub3B0LmZpZWxkTmFtZVByZWZpeCArICdjYXB0Y2hhJykucmVtb3ZlKCk7XG4gICAgfVxuICAgIF9pbml0RXZlbnRzKCkge1xuICAgICAgICB0aGlzLiRmb3JtLm9uKCdzdWJtaXQnLCAoZSkgPT4ge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgdGhpcy5zdWJtaXQoKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIF9iZWZvcmVTdWJtaXQoKSB7XG4gICAgICAgIHRoaXMuaXNMb2FkaW5nID0gdHJ1ZTtcblxuICAgICAgICB0aGlzLiRmaWVsZHMucmVtb3ZlQ2xhc3ModGhpcy5vcHQuZXJyb3JDbGFzcyk7XG5cbiAgICAgICAgJC5mYW5jeWJveC5zaG93TG9hZGluZygpO1xuICAgIH1cbiAgICBfcHJvY2Vzc1N1Ym1pdCgpIHtcbiAgICAgICAgbGV0IHBhcmFtcyA9IG5ldyBGb3JtRGF0YSh0aGlzLmVsKTtcblxuICAgICAgICBBamF4LmdldCgnc2VuZF9mb3JtJywgcGFyYW1zKS50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICBpZiAoZGF0YS5vaykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdbIEZvcm0vX3Byb2Nlc3NTdWJtaXQgXSBTZW50IHN1Y2Nlc3NmdWxseScpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuJGZvcm0uZGF0YSgnZ29hbCcpKSB7XG4gICAgICAgICAgICAgICAgICAgICQod2luZG93KS50cmlnZ2VyKCdhamF4Rm9ybTpmb3JtOmdvYWwnLCB0aGlzLiRmb3JtLmRhdGEoKSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuJGNvbnRlbnQubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnRlbnQuaHRtbChkYXRhLmNvbnRlbnQpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRmb3JtLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGZvcm0ucmVwbGFjZVdpdGgoZGF0YS5jb250ZW50KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5fYWZ0ZXJTdWJtaXQoKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZGF0YS5lcnJvcnMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnWyBGb3JtL19wcm9jZXNzU3VibWl0IF0gR290IGVycm9ycycsIGRhdGEuZXJyb3JzKTtcblxuICAgICAgICAgICAgICAgIGZvciAobGV0IHAgaW4gZGF0YS5lcnJvcnMpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZmllbGRzLmZpbHRlcih0aGlzLm9wdC5maWVsZE5hbWVQcmVmaXggKyBwKS5hZGRDbGFzcyh0aGlzLm9wdC5lcnJvckNsYXNzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5fYWZ0ZXJTdWJtaXQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSkuY2F0Y2goZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ1sgRm9ybS9fcHJvY2Vzc1N1Ym1pdCBdIFNlbmRpbmcgZmFpbGVkJywgZXJyb3IpO1xuXG4gICAgICAgICAgICB0aGlzLl9hZnRlclN1Ym1pdCgpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgX2FmdGVyU3VibWl0KCkge1xuICAgICAgICAkLmZhbmN5Ym94LmhpZGVMb2FkaW5nKCk7XG5cbiAgICAgICAgaWYgKCQuZmFuY3lib3guaXNPcGVuKSB7XG4gICAgICAgICAgICAkLmZhbmN5Ym94LnVwZGF0ZSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICB9XG59XG5cblxuZXhwb3J0IHtcbiAgICBGb3JtXG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKiBnbG9iYWwgJCAqL1xuXG5pbXBvcnQgeyBjb3VudGVycyB9IGZyb20gJ3V0aWxzL2NvdW50ZXJzJztcblxuXG5jbGFzcyBHb2FsIHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0ID0gJC5leHRlbmQoe30sIHtcbiAgICAgICAgICAgIGdvYWxzOiBbXSxcbiAgICAgICAgICAgIGNvdW50ZXJzOiBjb3VudGVyc1xuICAgICAgICB9LCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuICAgIGluaXQoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIEdvYWwvaW5pdCBdJyk7XG5cbiAgICAgICAgdGhpcy5vcHQuZ29hbHMuZm9yRWFjaCgoZ29hbCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5iaW5kKGdvYWwpO1xuICAgICAgICB9KTtcbiAgICAgICAgJCh3aW5kb3cpLm9uKCdtYWluOnJlYWNoR29hbCcsIChlLCBkYXRhKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnJlYWNoKGRhdGEuZ29hbCwgZGF0YS5wYXJhbXMpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgYmluZChnb2FsKSB7XG4gICAgICAgIGxldCBoYW5kbGVyID0gKGUsIGRhdGEpID0+IHtcbiAgICAgICAgICAgIGlmIChnb2FsLmdvYWwpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlYWNoKGdvYWwuZ29hbCwgZ29hbC5wYXJhbXMpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChnb2FsLmhhbmRsZXIpIHtcbiAgICAgICAgICAgICAgICBnb2FsLmhhbmRsZXIuY2FsbChlLnRhcmdldCwgZSwgZGF0YSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTmVpdGhlciBnb2FsIG5vciBoYW5kbGVyIGFyZSBkZWZpbmVkJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHRoaXMuX19zZWxlY3RvcklzRGVsZWdhdG9yKGdvYWwuc2VsZWN0b3IpKSB7XG4gICAgICAgICAgICB0aGlzLiRib2R5Lm9uKGdvYWwuZXZlbnQsIGdvYWwuc2VsZWN0b3IsIGhhbmRsZXIpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJChnb2FsLnNlbGVjdG9yKS5vbihnb2FsLmV2ZW50LCBoYW5kbGVyKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZWFjaChnb2FsLCBwYXJhbXMpIHtcbiAgICAgICAgY29uc29sZS5sb2coYFsgR29hbC9yZWFjaCBdICR7Z29hbH1gKTtcblxuICAgICAgICB0aGlzLm9wdC5jb3VudGVycy5mb3JFYWNoKChjb3VudGVyKSA9PiB7XG4gICAgICAgICAgICBjb3VudGVyLnJlYWNoR29hbChnb2FsLCBwYXJhbXMpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgZ2V0ICRib2R5KCkge1xuICAgICAgICBpZiAoIXRoaXMuX18kYm9keSkge1xuICAgICAgICAgICAgdGhpcy5fXyRib2R5ID0gJChkb2N1bWVudC5ib2R5KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fXyRib2R5O1xuICAgIH1cbiAgICBfX3NlbGVjdG9ySXNEZWxlZ2F0b3Ioc2VsZWN0b3IpIHtcbiAgICAgICAgcmV0dXJuIFt3aW5kb3csIGRvY3VtZW50LCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQsIGRvY3VtZW50LmJvZHksICdodG1sJywgJ2JvZHknXS5pbmRleE9mKHNlbGVjdG9yKSA9PT0gLTE7XG4gICAgfVxufVxuXG5cbmV4cG9ydCB7XG4gICAgR29hbFxufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyogZ2xvYmFsICQgKi9cblxubGV0IHNldHRpbmdzID0ge1xuICAgIHNlbGVjdG9yOiAnLmpzLWhvbWUtc2xpZGVyJyxcbiAgICBpdGVtU2VsZWN0b3I6ICcuanMtaG9tZS1zbGlkZXJfX2l0ZW0nLFxuICAgIG5hdlNlbGVjdG9yOiAnLmpzLWhvbWUtc2xpZGVyX19uYXYnLFxuICAgIGxpbmtTZWxlY3RvcjogJy5qcy1ob21lLXNsaWRlcl9fbGluaycsXG4gICAgcHJldlNlbGVjdG9yOiAnLmpzLWhvbWUtc2xpZGVyX19wcmV2JyxcbiAgICBuZXh0U2VsZWN0b3I6ICcuanMtaG9tZS1zbGlkZXJfX25leHQnLFxuICAgIHNsaWRlcjoge1xuICAgICAgICBkdXJhdGlvbjogNDAwLFxuICAgICAgICBhdXRvQ2hhbmdlOiB0cnVlLFxuICAgICAgICBkZWxheTogNTAwMCxcbiAgICAgICAgbG9vcDogdHJ1ZVxuICAgIH1cbn07XG5cbmNsYXNzIEhvbWVTbGlkZXIge1xuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMpIHtcbiAgICAgICAgdGhpcy5vcHQgPSAkLmV4dGVuZCh7fSwgc2V0dGluZ3MsIG9wdGlvbnMpO1xuICAgICAgICB0aGlzLmluaXQoKTtcbiAgICB9XG4gICAgaW5pdCgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1sgSG9tZVNsaWRlci9pbml0IF0nKTtcblxuICAgICAgICBsZXQgJGJsb2NrID0gJCh0aGlzLm9wdC5zZWxlY3Rvcik7XG4gICAgICAgIGlmICgkYmxvY2subGVuZ3RoKSB7XG4gICAgICAgICAgICBsZXQgJGl0ZW1zID0gJGJsb2NrLmZpbmQodGhpcy5vcHQuaXRlbVNlbGVjdG9yKSxcbiAgICAgICAgICAgICAgICAkbmF2ID0gJGJsb2NrLmZpbmQodGhpcy5vcHQubmF2U2VsZWN0b3IpLFxuICAgICAgICAgICAgICAgICRsaW5rcyA9ICRibG9jay5maW5kKHRoaXMub3B0LmxpbmtTZWxlY3RvciksXG4gICAgICAgICAgICAgICAgJHByZXYgPSAkYmxvY2suZmluZCh0aGlzLm9wdC5wcmV2U2VsZWN0b3IpLFxuICAgICAgICAgICAgICAgICRuZXh0ID0gJGJsb2NrLmZpbmQodGhpcy5vcHQubmV4dFNlbGVjdG9yKTtcblxuICAgICAgICAgICAgJGl0ZW1zLnNsaWRlcigkLmV4dGVuZCh7fSwgdGhpcy5vcHQuc2xpZGVyLCB7XG4gICAgICAgICAgICAgICAgbmF2OiAkbGlua3MsXG4gICAgICAgICAgICAgICAgcHJldjogJHByZXYsXG4gICAgICAgICAgICAgICAgbmV4dDogJG5leHRcbiAgICAgICAgICAgIH0pKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuZXhwb3J0IHtcblx0SG9tZVNsaWRlclxufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyogZ2xvYmFsICQgKi9cblxuaW1wb3J0IHsgQWpheCB9IGZyb20gJy4uL3V0aWxzL2FqYXgnO1xuaW1wb3J0IHsgZmFuY3lib3ggfSBmcm9tICcuLi9zZXR0aW5ncyc7XG5cbmxldCBzZXR0aW5ncyA9IHtcblx0bGlua1NlbGVjdG9yOiAnLmpzLWxvY2F0aW9uLXNlbGVjdF9fbGluaycsXG5cdGNpdHlTZWxlY3RvcjogJy5qcy1sb2NhdGlvbi1zZWxlY3RfX2NpdHknLFxuXHRmYW5jeWJveDoge31cbn07XG5cbmNsYXNzIExvY2F0aW9uU2VsZWN0IHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0ID0gJC5leHRlbmQoe30sIHNldHRpbmdzLCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuICAgIGluaXQoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIExvY2F0aW9uU2VsZWN0L2luaXQgXScpO1xuXG4gICAgICAgIGxldCAkbGluayA9ICQodGhpcy5vcHQubGlua1NlbGVjdG9yKTtcblxuICAgICAgICAkbGluay5vbignY2xpY2snLCAoZSkgPT4ge1xuXHQgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblx0ICAgICAgICB0aGlzLm9wZW4oKTtcblx0ICAgIH0pO1xuICAgIH1cbiAgICBvcGVuKCkge1xuICAgICAgICBjb25zb2xlLmxvZygnQ2l0eVNlbGVjdC9vcGVuJyk7XG5cbiAgICAgICAgQWpheC5nZXQoJ2dldF9sb2NhdGlvbl9zZWxlY3QnKS50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICBpZiAoZGF0YS5vaykge1xuICAgICAgICAgICAgICAgIHRoaXMuc2hvdyhkYXRhLmNvbnRlbnQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Nhbm5vdCBnZXQgbG9jYXRpb24gc2VsZWN0Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBzaG93KGNvbnRlbnQpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0NpdHlTZWxlY3Qvc2hvdycpO1xuXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcblxuICAgICAgICAkLmZhbmN5Ym94KCQuZXh0ZW5kKHRydWUsIHt9LCBmYW5jeWJveCwgdGhpcy5vcHQuZmFuY3lib3gsIHtcbiAgICAgICAgICAgIHR5cGU6ICdodG1sJyxcbiAgICAgICAgICAgIGNvbnRlbnQ6IGNvbnRlbnQsXG4gICAgICAgICAgICBhZnRlclNob3c6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHRoaXMuaW5uZXIuZmluZChzZWxmLm9wdC5jaXR5U2VsZWN0b3IpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnNlbGVjdCgkKHRoaXMpLmRhdGEoKSkudGhlbigoZGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBcdCQuZmFuY3lib3guY2xvc2UoKTtcbiAgICAgICAgICAgICAgICAgICAgfSkuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIFx0dGhyb3cgZXJyb3I7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KSk7XG4gICAgfVxuICAgIHNlbGVjdChkYXRhKSB7XG4gICAgXHRyZXR1cm4gQWpheC5nZXQoJ3NhdmVfbG9jYXRpb24nLCBkYXRhKS50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICBpZiAoZGF0YS5vaykge1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LmxvY2F0aW9uLnJlbG9hZCgpO1xuICAgICAgICAgICAgICAgIHJldHVybiBkYXRhO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3Qgc2F2ZSBsb2NhdGlvbiBzZWxlY3RcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IHtcbiAgICBMb2NhdGlvblNlbGVjdFxufTtcbiIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qIGdsb2JhbCAkICovXHJcblxyXG5sZXQgc2V0dGluZ3MgPSB7XHJcbiAgICBtYXBTZWxlY3RvcjogJy5qcy1sb21iYXJkX19tYXAnLFxyXG4gICAgbWFwOiB7XHJcbiAgICAgICAgem9vbTogMTYsXHJcbiAgICAgICAgY29udHJvbHM6IFsnZGVmYXVsdCddXHJcbiAgICB9LFxyXG4gICAgeW1hcHNTY3JpcHRTcmM6ICdodHRwczovL2FwaS1tYXBzLnlhbmRleC5ydS8yLjEvP2xhbmc9cnVfUlUnXHJcbn07XHJcblxyXG5jbGFzcyBMb21iYXJkTWFwIHtcclxuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMpIHtcclxuICAgICAgICB0aGlzLm9wdCA9ICQuZXh0ZW5kKHt9LCBzZXR0aW5ncywgb3B0aW9ucyk7XHJcbiAgICAgICAgdGhpcy5pbml0KCk7XHJcbiAgICB9XHJcbiAgICBpbml0KCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdbIExvbWJhcmRNYXAvaW5pdCBdJyk7XHJcblxyXG4gICAgICAgIHRoaXMuJG1hcCA9ICQodGhpcy5vcHQubWFwU2VsZWN0b3IpO1xyXG4gICAgICAgIGlmICh0aGlzLiRtYXAubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29vcmRzID0gdGhpcy4kbWFwLmRhdGEoJ2Nvb3JkcycpLnNwbGl0KC9bXjAtOS5cXC1cXCtdKy8pO1xyXG4gICAgICAgICAgICB0aGlzLm9wdC5tYXAuY2VudGVyID0gdGhpcy5jb29yZHM7XHJcblxyXG4gICAgICAgICAgICAkLmdldFNjcmlwdCh0aGlzLm9wdC55bWFwc1NjcmlwdFNyYykuZG9uZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB5bWFwcy5yZWFkeSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbml0TWFwKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSkuZmFpbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYENhbid0IGxvYWQgc2NyaXB0ICR7IHRoaXMub3B0LnltYXBzU2NyaXB0U3JjIH1gKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgaW5pdE1hcCgpIHtcclxuICAgICAgICBsZXQgbWFwID0gbmV3IHltYXBzLk1hcCh0aGlzLiRtYXAuZ2V0KDApLCB0aGlzLm9wdC5tYXApO1xyXG5cclxuICAgICAgICBsZXQgcGxhY2VtYXJrID0gbmV3IHltYXBzLlBsYWNlbWFyayh0aGlzLm9wdC5tYXAuY2VudGVyLCB7fSwge1xyXG4gICAgICAgICAgICBpY29uTGF5b3V0OiAnZGVmYXVsdCNpbWFnZScsXHJcbiAgICAgICAgICAgIGljb25JbWFnZUhyZWY6ICcvaS9pY29ucy9pY29uX2NpcmNsZS5zdmcnLFxyXG4gICAgICAgICAgICBpY29uSW1hZ2VTaXplOiBbMjQsIDI0XVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIG1hcC5nZW9PYmplY3RzLmFkZChwbGFjZW1hcmspO1xyXG4gICAgICAgIFxyXG4gICAgICAgIG1hcC5iZWhhdmlvcnMuZGlzYWJsZSgnc2Nyb2xsWm9vbScpO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQge1xyXG4gICAgTG9tYmFyZE1hcFxyXG59O1xyXG4iLCIndXNlIHN0cmljdCc7XG5cbi8qIGdsb2JhbCAkICovXG5cbmltcG9ydCB7IEFqYXggfSBmcm9tICd1dGlscy9hamF4JztcblxubGV0IHNldHRpbmdzID0ge1xuICAgIHJvb3RXcmFwOiAnLmpzLXBvLXdyYXAnLFxufTtcbmxldCAkd3JhcDtcbmNsYXNzIFBlcnNvbmFsT2ZmaWNlIHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0ID0gJC5leHRlbmQoe30sIHNldHRpbmdzLCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuICAgIGluaXQoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIFBlcnNvbmFsT2ZmaWNlL2luaXQgXScpO1xuICAgICAgICAkd3JhcCA9ICQodGhpcy5vcHQucm9vdFdyYXApO1xuICAgICAgICB0aGlzLmdldExvZ2luKCk7XG4gICAgfVxuICAgIGdldExvZ2luKCkge1xuICAgIFx0bGV0IHNlbGYgPSB0aGlzO1xuICAgIFx0bGV0IGxvZ2luVGVtcGxhdGUgPSBcblxuICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX3RvcHRleHQgcGVyc29uYWwtb2ZmaWNlX19wYXJhZ3JhcGhcIj4nICtcbiAgICBcdCAgICAnICA8aDEgY2xhc3M9XCJoMSBtLS1ibG9jay15ZWxsb3cgcGVyc29uYWwtb2ZmaWNlX19wYXJhZ3JhcGhcIj7QktC+0LnRgtC4INCyINC70LjRh9C90YvQuSDQutCw0LHQuNC90LXRgjwvaDE+JyArXG4gICAgICAgICAgICAnPC9kaXY+JyArXG4gICAgICAgICAgICAnPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fdG9wdGV4dCBwZXJzb25hbC1vZmZpY2VfX2xpbmtzYm90dG9tIHBlcnNvbmFsLW9mZmljZV9fcGFyYWdyYXBoXCI+JyArXG4gICAgXHQgICAgJyAgIDxhIGlkPVwicmVnaXN0cmF0aW9uXCIgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX2xpbmtcIiBocmVmPVwiI1wiPtCg0LXQs9C40YHRgtGA0LDRhtC40Y88L2E+JyArXG4gICAgXHQgICAgJyAgIDxhIGlkPVwicmVjYWxsLXBhc3N3b3JkXCIgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX2xpbmtcIiBocmVmPVwiI1wiPtCX0LDQsdGL0LvQuCDQv9Cw0YDQvtC70Yw8L2E+JyArXG4gICAgICAgICAgICAnPC9kaXY+JyArXG4gICAgICAgICAgICAnPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9faGVpZ2h0XCIgPicgK1xuICAgICAgICAgICAgJyAgIDxkaXYgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX3RleHRibG9jayBwZXJzb25hbC1vZmZpY2VfX29mZnNldFwiPicgK1xuICAgICAgICAgICAgJyAgICAgICA8ZGl2IGNsYXNzPVwiZmlsdGVyX19zdWJibG9jay1oZWFkZXJcIj7QkiDQm9C40YfQvdC+0Lwg0LrQsNCx0LjQvdC10YLQtTo8L2Rpdj4nICtcbiAgICAgICAgICAgICcgICAgICAgPGRpdiBjbGFzcz1cIm5ld3NfX2l0ZW0tcHJldmlldyBcIj48cD4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgICYjODIxMjsg0JLRgdGPINC40L3RhNC+0YDQvNCw0YbQuNGPINC/0L4g0LrQsNC20LTQvtC80YMg0LLQsNGI0LXQvNGDINC30LDQudC80YMgKNC00LXQudGB0YLQstGD0Y7RidC40Lwg0Lgg0L/QvtCz0LDRiNC10L3QvdGL0Lwg0LfQsNC70L7Qs9C+0LLRi9C8INCx0LjQu9C10YLQsNC8KTxicj4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgICYjODIxMjsg0JLQsNGIINGB0YLQsNGC0YPRgSDQu9C+0Y/Qu9GM0L3QvtGB0YLQuDxicj4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgICYjODIxMjsg0JvQuNGH0L3Ri9C1INC/0YDQtdC00LvQvtC20LXQvdC40Y88YnI+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICAmIzgyMTI7INCS0L7Qt9C80L7QttC90L7RgdGC0Ywg0L/QtdGA0LzQvtC90LDQu9GM0L3QviDQvtCx0YDQsNGC0LjRgtGM0YHRjyDQsiDRgdC70YPQttCx0YMg0L/QvtC00LTQtdGA0LbQutC4JyArXG4gICAgICAgICAgICAnICAgICAgIDwvcD48L2Rpdj4nICtcbiAgICAgICAgICAgICcgICA8L2Rpdj4nICtcbiAgICAgICAgICAgICcgICA8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19maWVsZHdpZHRoXCI+JyArXG4gICAgXHQgICAgJyAgICAgIDxmb3JtIGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19mb3JtXCI+JyArXG4gICAgICAgICAgICAnICAgICAgICA8aW5wdXQgdHlwZT1cImhpZGRlblwiIHZhbHVlPVwiQXV0aGVudGljYXRlXCIgbmFtZT1cInJlcXVlc3RcIiAvPicgK1xuICAgICAgICAgICAgJyAgICAgICAgPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fb2Zmc2V0XCI+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICA8bGFiZWwgPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgICAgPGlucHV0IG5hbWU9XCJwaG9uZVwiIGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19maWVsZCAgcGVyc29uYWwtb2ZmaWNlX19maWVsZHdpZHRoXCIgcGxhY2Vob2xkZXI9XCLQktCy0LXQtNC40YLQtSDQvdC+0LzQtdGAINGC0LXQu9C10YTQvtC90LBcIiAvPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgPC9sYWJlbD4nICtcbiAgICAgICAgICAgICcgICAgICAgIDwvZGl2PicgK1xuICAgICAgICAgICAgJyAgICAgICAgPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fb2Zmc2V0XCI+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICA8bGFiZWwgPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgICAgPGlucHV0IG5hbWU9XCJpaW5cIiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fZmllbGQgIHBlcnNvbmFsLW9mZmljZV9fZmllbGR3aWR0aFwiIHBsYWNlaG9sZGVyPVwi0JLQstC10LTQuNGC0LUg0L3QvtC80LXRgCDQmNCY0J1cIiAvPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgPC9sYWJlbD4nICtcbiAgICAgICAgICAgICcgICAgICAgIDwvZGl2PicgK1xuICAgIFx0ICAgICdcdCAgICAgPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fb2Zmc2V0XCI+JyArXG4gICAgXHQgICAgJ1x0XHQgICAgPGxhYmVsPicgK1xuICAgIFx0ICAgICdcdFx0XHQgICAgPGlucHV0IG5hbWU9XCJwYXNzd29yZFwiIHR5cGU9XCJwYXNzd29yZFwiIGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19maWVsZCAgcGVyc29uYWwtb2ZmaWNlX19maWVsZHdpZHRoXCIgIHBsYWNlaG9sZGVyPVwi0JLQstC10LTQuNGC0LUg0JLQsNGIINC/0LDRgNC+0LvRjFwiIC8+ICcgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX2ljb25cIj48L2Rpdj4nICsgXG4gICAgXHQgICAgJ1x0XHQgICAgPC9sYWJlbD4nICtcbiAgICBcdCAgICAnXHQgICAgIDwvZGl2PicgK1xuICAgIFx0ICAgICdcdCAgICAgPGRpdiBjbGFzcyA9IFwicGVyc29uYWwtb2ZmaWNlX19maWVsZHdpZHRoXCI+JyArXG4gICAgXHQgICAgJ1x0XHQgICAgPGJ1dHRvbiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fYnV0dG9uIHBlcnNvbmFsLW9mZmljZV9fY2VudGVyXCI+0JLQntCZ0KLQmDwvYnV0dG9uPicgKyBcbiAgICBcdCAgICAnXHQgICAgIDwvZGl2PicgK1xuICAgIFx0ICAgICcgICAgICA8L2Zvcm0+JyArIFxuICAgICAgICAgICAgJyAgIDwvZGl2PicgK1xuICAgICAgICAgICAgJzwvZGl2PidcblxuICAgICAgICAgICAgO1xuXG4gICAgXHRsZXQgdG1wbHQgPSBfLnRlbXBsYXRlKGxvZ2luVGVtcGxhdGUpO1xuICAgICAgICBjb25zb2xlLmxvZyh0bXBsdCk7XG4gICAgXHQkd3JhcC5odG1sKHRtcGx0KCkpO1xuICAgIFx0JHdyYXAub24oJ2NsaWNrJywgJyNyZWdpc3RyYXRpb24nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgc2VsZi5yZWdpc3RyYXRpb24oKTtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSk7XG4gICAgICAgIGxldCAkZm9ybSA9ICR3cmFwLmZpbmQoJ2Zvcm0nKTtcbiAgICAgICAgJGZvcm0uc3VibWl0KChlKSA9PiB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBsZXQgcGFyYW1zID0gJGZvcm0uc2VyaWFsaXplT2JqZWN0KCk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnQmVmb3JlIHNlbmRSZXF1ZXN0JywgcGFyYW1zKTtcbiAgICAgICAgICAgIHRoaXMuc2VuZFJlcXVlc3QocGFyYW1zLCAoZGF0YSkgPT4ge3RoaXMucHJvY2Vzc0F1dGhlbnRpY2F0ZShkYXRhKTsgfSk7XG4gICAgICAgIH0pO1xuXHR9XG5cdHJlZ2lzdHJhdGlvbigpIHtcbi8vXHRcdGxldCAkd3JhcCA9ICQodGhpcy5vcHQucm9vdFdyYXApO1xuXHRcdGxldCByZWdpc3RyYXRpb25UZW1wbGF0ZSA9IFxuXG4gICAgICAgICAgICAnPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fYnJlYWRjcnVtYnNcIj4nICtcbiAgICAgICAgICAgICcgICA8YSBocmVmPVwiL1wiIGNsYXNzPVwiYnJlYWRjcnVtYnNfX2xpbmtcIj7Qk9C70LDQstC90LDRjyDRgdGC0YDQsNC90LjRhtCwPjwvYT4nICtcbiAgICAgICAgICAgICcgICA8YSBjbGFzcz1cImJyZWFkY3J1bWJzX19pdGVtICBpcy1hY3RpdmVcIiA+0JvQuNGH0L3Ri9C5INC60LDQsdC40L3QtdGCPC9hPicgK1xuICAgICAgICAgICAgJzwvZGl2PicgK1xuICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX3RvcHRleHQgcGVyc29uYWwtb2ZmaWNlX19wYXJhZ3JhcGhcIj4nICtcbiAgICAgICAgICAgICcgIDxoMSBjbGFzcz1cImgxIG0tLWJsb2NrLXllbGxvd1wiPtCg0LXQs9C40YHRgtGA0LDRhtC40Y8g0LrQsNCx0LjQvdC10YLQsDwvaDE+JyArXG4gICAgICAgICAgICAnPC9kaXY+JyArXG4gICAgICAgICAgICAnPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fdG9wdGV4dCBwZXJzb25hbC1vZmZpY2VfX2xpbmtzYm90dG9tXCI+JyArXG4gICAgICAgICAgICAnICA8YSBpZD1cInJlZ2lzdHJhdGlvblwiIGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19saW5rXCIgaHJlZj1cIiNcIj7QktGF0L7QtCDQsiDQu9C40YfQvdGL0Lkg0LrQsNCx0LjQvdC10YI8L2E+JyArXG4gICAgICAgICAgICAnPC9kaXY+JyArXG4gICAgICAgICAgICAnPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9faGVpZ2h0IHBlcnNvbmFsLW9mZmljZV9fb2Zmc2V0XCIgPicgK1xuICAgICAgICAgICAgJyAgPGZvcm0gY2xhc3MgPSBcInBlcnNvbmFsLW9mZmljZV9fZm9ybVwiPicgK1xuICAgIFx0ICAgICdcdCA8aW5wdXQgdHlwZT1cImhpZGRlblwiIHZhbHVlPVwiUmVnaXN0ZXJcIiBuYW1lPVwicmVxdWVzdFwiIC8+JyArXG4gICAgXHQgICAgJ1x0IDxkaXYgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX29mZnNldFwiPicgK1xuICAgICAgICAgICAgJyAgICAgICAgIDxsYWJlbCA+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICAgPGlucHV0IG5hbWU9XCJpaW5cIiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fZmllbGQgIHBlcnNvbmFsLW9mZmljZV9fZmllbGR3aWR0aFwiIHBsYWNlaG9sZGVyID0gXCLQktCy0LXQtNC40YLQtSDQvdC+0LzQtdGAINCY0J3QnVwiIC8+JyArXG4gICAgICAgICAgICAnICAgICAgICAgPC9sYWJlbD4nICtcbiAgICAgICAgICAgICcgICAgICAgICA8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX193YXJuaW5naW5jb3JyZWN0ICBwZXJzb25hbC1vZmZpY2VfX3dhcm5pbmdkaXNwbGF5XCI+0J3QtdC60L7RgNGA0LXQutGC0L3Ri9C5INC90L7QvNC10YAg0JjQndCdPC9kaXY+JyArXG4gICAgXHQgICAgJ1x0IDwvZGl2PicgK1xuICAgICAgICAgICAgJyAgICA8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19vZmZzZXRcIj4nICtcbiAgICAgICAgICAgICcgICAgICAgICA8bGFiZWwgPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgIDxpbnB1dCBuYW1lPVwicGhvbmVcIiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fZmllbGQgIHBlcnNvbmFsLW9mZmljZV9fZmllbGR3aWR0aFwiICBwbGFjZWhvbGRlciA9IFwi0JLQstC10LTQuNGC0LUg0L3QvtC80LXRgCDQktCw0YjQtdCz0L4g0YLQtdC70LXRhNC+0L3QsFwiIC8+JyArXG4gICAgICAgICAgICAnICAgICAgICAgPC9sYWJlbD4nICtcbiAgICAgICAgICAgICcgICAgICAgICA8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX193YXJuaW5naW5jb3JyZWN0ICAgcGVyc29uYWwtb2ZmaWNlX193YXJuaW5nZGlzcGxheVwiID7QndC10LrQvtGA0YDQtdC60YLQvdGL0Lkg0L3QvtC80LXRgCDRgtC10LvQtdGE0L7QvdCwPC9kaXY+JyArXG4gICAgICAgICAgICAnICAgIDwvZGl2PicgK1xuICAgICAgICAgICAgJyAgICA8ZGl2IGNsYXNzID0gXCJwZXJzb25hbC1vZmZpY2VfX2ZpZWxkd2lkdGhcIj4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fYnV0dG9uICBwZXJzb25hbC1vZmZpY2VfX2NlbnRlclwiPtCe0YLQv9GA0LDQstC40YLRjCDQtNCw0L3QvdGL0LU8L2J1dHRvbj4nICsgXG4gICAgICAgICAgICAnICAgIDwvZGl2PicgK1xuICAgIFx0ICAgICc8L2Zvcm0+ICcgK1xuICAgICAgICAgICAgJzwvZGl2PicgXG4gICAgICAgICBcblx0XHQ7XG4gICAgXHRsZXQgdG1wbHQgPSBfLnRlbXBsYXRlKHJlZ2lzdHJhdGlvblRlbXBsYXRlKTtcbiAgICBcdCR3cmFwLmh0bWwodG1wbHQoKSk7XG5cbiAgICBcdC8vICR3cmFwLm9uKCdyZWdpc3RlcjpzZW5kQ29kZScsIChlLCBkYXRhKSA9PiB7XG4gICAgXHQvLyBcdHRoaXMucHJvY2Vzc1JlZ2lzdGVyQ29kZSggZGF0YSApO1xuICAgIFx0Ly8gfSk7XG4gICAgXHRcbiAgICBcdGxldCAkZm9ybSA9ICR3cmFwLmZpbmQoJ2Zvcm0nKTtcbiAgICAgICAgJGZvcm0uc3VibWl0KChlKSA9PiB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBsZXQgcGFyYW1zID0gJGZvcm0uc2VyaWFsaXplT2JqZWN0KCk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnQmVmb3JlIHNlbmRSZXF1ZXN0JywgcGFyYW1zKTtcbiAgICAgICAgICAgIHRoaXMuc2VuZFJlcXVlc3QocGFyYW1zLCAoZGF0YSkgPT4ge3RoaXMucHJvY2Vzc1JlZ2lzdGVyQ29kZShkYXRhKTsgfSk7XG4gICAgICAgIH0pO1xuXHR9XG4gICAgZ2V0Q2hlY2tDb2RlKHBhcmFtcykge1xuICAgICAgICBjb25zb2xlLmxvZygnWyBQZXJzb25hbE9mZmljZS9nZXRDaGVja0NvZGUgXScsIHBhcmFtcyk7XG4gICAgICAgIFxuICAgIFx0bGV0IHRlbXBsYXRlID0gXG5cbiAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19icmVhZGNydW1ic1wiPicgK1xuICAgICAgICAgICAgJyAgIDxhIGhyZWY9XCIvXCIgY2xhc3M9XCJicmVhZGNydW1ic19fbGlua1wiPtCT0LvQsNCy0L3QsNGPINGB0YLRgNCw0L3QuNGG0LA+PC9hPicgK1xuICAgICAgICAgICAgJyAgIDxhIGNsYXNzPVwiYnJlYWRjcnVtYnNfX2l0ZW0gIGlzLWFjdGl2ZVwiID7Qm9C40YfQvdGL0Lkg0LrQsNCx0LjQvdC10YI8L2E+JyArXG4gICAgICAgICAgICAnPC9kaXY+JyArXG4gICAgXHRcdCc8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX190b3B0ZXh0XCI+JyArXG4gICAgICAgICAgICAnICAgPGgxIGNsYXNzPVwiaDEgbS0tYmxvY2steWVsbG93XCI+0JLQstC10LTQuNGC0LUg0LrQvtC0INC40Lcg0KHQnNChPC9oMT4nICtcbiAgICAgICAgICAgICc8L2Rpdj4nICtcbiAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19oZWlnaHRcIiA+JyArXG4gICAgICAgICAgICAnPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fdGV4dGJsb2NrIHBlcnNvbmFsLW9mZmljZV9fZmllbGR3aWR0aCBwZXJzb25hbC1vZmZpY2VfX29mZnNldFwiID48cD4nICtcbiAgICAgICAgICAgICcgICDQndCwINCy0LDRiCDQvdC+0LzQtdGAINC80L7QsdC40LvRjNC90L7Qs9C+INGC0LXQu9C10YTQvtC90LAg0L7RgtC/0YDQsNCy0LvQtdC90L4g0KHQnNChLdGD0LLQtdC00L7QvNC70LXQvdC40LUg0YEg0LrQvtC00L7QvCDQtNC70Y8g0LDQutGC0LjQstCw0YbQuNC4INC30LDRj9Cy0LrQuCDRgNC10LPQuNGB0YLRgNCw0YbQuNC4INC70LjRh9C90L7Qs9C+INC60LDQsdC40L3QtdGC0LAuPGJyPicgK1xuICAgICAgICAgICAgJyAgINCU0LvRjyDQv9GA0L7QtNC+0LvQttC10L3QuNGPINGA0LXQs9C40YHRgtGA0LDRhtC40Lgg0LLQstC10LTQuNGC0LUg0LrQvtC0INC40Lcg0KHQnNChINCyINC/0L7Qu9C1INC4INC90LDQttC80LjRgtC1INC60L3QvtC/0LrRgyBcItCU0LDQu9C10LVcIicgK1xuICAgICAgICAgICAgJzwvcD48L2Rpdj4nICtcbiAgICAgICAgICAgICcgICA8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19maWVsZHdpZHRoXCIgPicgKyAgICBcbiAgICAgICAgICAgICcgICAgICAgPGZvcm0gY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX2Zvcm1cIj4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiaGlkZGVuXCIgdmFsdWU9XCJSZWdpc3RlclwiIG5hbWU9XCJyZXF1ZXN0XCIgLz4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX29mZnNldFwiPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgICAgIDxsYWJlbCA+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fZmllbGRcIiBuYW1lPVwiY29kZVwiICBwbGFjZWhvbGRlciA9IFwi0JrQvtC0INC40Lcg0KHQnNChXCIgLz4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgICAgICA8L2xhYmVsPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgPC9kaXY+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19vZmZzZXRcIj4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgICAgICA8bGFiZWw+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICAgICAgICAgIDxpbnB1dCBuYW1lPVwicGFzc3dvcmRcIiB0eXBlPVwicGFzc3dvcmRcIiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fZmllbGQgcGVyc29uYWwtb2ZmaWNlX19maWVsZHdpZHRoXCIgcGxhY2Vob2xkZXI9XCLQn9GA0LjQtNGD0LzQsNC50YLQtSDQv9Cw0YDQvtC70YxcIiAvPiAnICtcbiAgICAgICAgICAgICcgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9faWNvblwiPjwvZGl2PicgKyBcbiAgICAgICAgICAgICcgICAgICAgICAgICAgICA8L2xhYmVsPicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgPC9kaXY+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19vZmZzZXRcIj4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgICAgICA8bGFiZWw+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICAgICAgICAgIDxpbnB1dCBuYW1lPVwicGFzc3dvcmRfZGJsXCIgdHlwZT1cInBhc3N3b3JkXCIgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX2ZpZWxkICBwZXJzb25hbC1vZmZpY2VfX2ZpZWxkd2lkdGhcIiBwbGFjZWhvbGRlciA9IFwi0J/QvtCy0YLQvtGA0LjRgtC1XCIgLz4gJyArXG4gICAgICAgICAgICAnICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwZXJzb25hbC1vZmZpY2VfX2ljb25cIj48L2Rpdj4nICsgXG4gICAgICAgICAgICAnICAgICAgICAgICAgICAgPC9sYWJlbD4nICtcbiAgICAgICAgICAgICcgICAgICAgICAgIDwvZGl2PicgK1xuICAgICAgICAgICAgJyAgICAgICAgICAgPGRpdiBjbGFzcyA9IFwicGVyc29uYWwtb2ZmaWNlX19maWVsZHdpZHRoXCI+JyArXG4gICAgICAgICAgICAnICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInBlcnNvbmFsLW9mZmljZV9fYnV0dG9uIHBlcnNvbmFsLW9mZmljZV9fY2VudGVyXCI+0JTQsNC70LXQtTwvYnV0dG9uPicgKyBcbiAgICAgICAgICAgICcgICAgICAgICAgIDwvZGl2PicgK1xuICAgICAgICAgICAgJyAgICAgICA8L2Zvcm0+JyAgK1xuICAgICAgICAgICAgJyAgIDwvZGl2PicgK1xuICAgICAgICAgICAgJzwvZGl2PidcbiAgICBcdDtcbiAgICBcdGxldCB0bXBsdCA9IF8udGVtcGxhdGUodGVtcGxhdGUpO1xuICAgIFx0JHdyYXAuaHRtbCh0bXBsdCgpKTtcbiAgICBcdGxldCAkZm9ybSA9ICR3cmFwLmZpbmQoJ2Zvcm0nKTtcbiAgICAgICAgJGZvcm0uc3VibWl0KChlKSA9PiB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBsZXQgcGFyYW1zID0gJGZvcm0uc2VyaWFsaXplT2JqZWN0KCk7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhwYXJhbXMpO1xuICAgICAgICAgICAgdGhpcy5zZW5kUmVxdWVzdChwYXJhbXMsIChkYXRhKSA9PiB7dGhpcy5wcm9jZXNzUmVnaXN0ZXJDb2RlKGRhdGEpOyB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIHJlZ2lzdGVyU3VjY2VzcygpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1sgUGVyc29uYWxPZmZpY2UvcmVnaXN0ZXJTdWNjZXNzIF0nKTtcbiAgICAgICAgbGV0IHRlbXBsYXRlID0gXG4gICAgICAgICAgICAnPHA+JyArXG4gICAgICAgICAgICAnICAgINCS0Ysg0YPRgdC/0LXRiNC90L4g0LfQsNGA0LXQs9C40YHRgtGA0LjRgNC+0LLQsNC70LjRgdGMINCyINC70LjRh9C90L7QvCDQutCw0LHQuNC90LXRgtC1JyArXG4gICAgICAgICAgICAnPC9wPicgK1xuICAgICAgICAgICAgJzxmb3JtPicgK1xuICAgICAgICAgICAgJyAgIDxkaXYgY2xhc3MgPSBcInBlcnNvbmFsLW9mZmljZV9fZmllbGR3aWR0aFwiPicgK1xuICAgICAgICAgICAgJyAgICAgICA8YnV0dG9uIGNsYXNzPVwicGVyc29uYWwtb2ZmaWNlX19idXR0b24gcGVyc29uYWwtb2ZmaWNlX19jZW50ZXJcIj7QlNCw0LvQtdC1PC9idXR0b24+JyArIFxuICAgICAgICAgICAgJyAgIDwvZGl2PicgK1xuICAgICAgICAgICAgJzwvZm9ybT4nO1xuICAgICAgICBsZXQgdG1wbHQgPSBfLnRlbXBsYXRlKHRlbXBsYXRlKTtcbiAgICAgICAgJHdyYXAuaHRtbCh0bXBsdCgpKTtcbiAgICAgICAgbGV0ICRmb3JtID0gJHdyYXAuZmluZCgnZm9ybScpO1xuICAgICAgICAkZm9ybS5zdWJtaXQoKGUpID0+IHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHRoaXMuZ2V0TG9naW4oKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIHByb2Nlc3NSZWdpc3RlckNvZGUoIGRhdGEgKSB7XG4gICAgXHRjb25zb2xlLmxvZygnWyBQZXJzb25hbE9mZmljZS9wcm9jZXNzUmVnaXN0ZXJDb2RlIF0nLCBkYXRhKTtcbiAgICAgICAgc3dpdGNoIChkYXRhLm9rKSB7XG4gICAgICAgIFx0Y2FzZSBcIjBcIiA6XG4gICAgICAgIFx0ICAgIGNvbnNvbGUubG9nKCfQndC10YIg0L7RiNC40LHQutC4Jyk7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGEudHlwZSA9PSAxKSB7XG4gICAgICAgIFx0XHQgICAgdGhpcy5nZXRDaGVja0NvZGUoJycpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVnaXN0ZXJTdWNjZXNzKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICBcdFx0YnJlYWs7XG4gICAgICAgIFx0Y2FzZSBcIjFcIiA6XG4gICAgICAgIFx0XHRjb25zb2xlLmxvZygn0J7RiNC40LHQutCwLi4uJyk7XG4gICAgICAgIFx0XHRicmVhaztcbiAgICAgICAgXHRjYXNlIFwiMlwiIDpcbiAgICAgICAgXHRcdGNvbnNvbGUubG9nKCfQndC10LLQtdGA0L3Ri9C5INC60L7QtCcpO1xuICAgICAgICBcdFx0YnJlYWs7XG4gICAgICAgIFx0Y2FzZSBcIjNcIiA6XG4gICAgICAgIFx0XHRjb25zb2xlLmxvZygn0J7RiNC40LHQutCwINCyINCY0J3QnScpO1xuICAgICAgICBcdFx0YnJlYWs7XG4gICAgICAgIFx0Y2FzZSBcIjRcIiA6XG4gICAgICAgIFx0XHRjb25zb2xlLmxvZygn0J7RiNC40LHQutCwINCyINC90L7QvNC10YDQtSDRgtC10LvQtdGE0L7QvdCwJyk7XG4gICAgICAgIFx0XHRicmVhaztcbiAgICAgICAgXHRkZWZhdWx0IDpcbiAgICAgICAgXHRcdGNvbnNvbGUuZXJyb3IoJ0Vycm9yIGluIHJlcXVlc3QnKTtcbiAgICAgICAgXHRcdGJyZWFrO1xuICAgICAgICB9XG4gICAgfVxuICAgIHByb2Nlc3NBdXRoZW50aWNhdGUoIGRhdGEgKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIFBlcnNvbmFsT2ZmaWNlL3Byb2Nlc3NBdXRoZW50aWNhdGUgXScsIGRhdGEpO1xuICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICAgIHN3aXRjaCAoZGF0YS5vaykge1xuICAgICAgICAgICAgY2FzZSBcIjBcIiA6XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ9Ce0L/QtdGA0LDRhtC40Y8g0LLRi9C/0L7Qu9C90LXQvdCwINGD0YHQv9C10YjQvdC+Jyk7XG4gICAgICAgICAgICAgICAgbGV0IHBhcmFtcyA9IHtyZXF1ZXN0OiAnR2V0VGlja2V0cyd9O1xuICAgICAgICAgICAgICAgIHRoaXMuc2VuZFJlcXVlc3QocGFyYW1zLCAoZGF0YSkgPT4ge3NlbGYucHJvY2Vzc0dldFRpY2tldHMoZGF0YSk7IH0pO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBcIjFcIiA6XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ9Cd0LXQstC10YDQvdGL0Lkg0L/QsNGA0L7Qu9GMJyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiMlwiIDpcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygn0JrQu9C40LXQvdGCINC90LUg0LfQsNGA0LXQs9C40YHRgtGA0LjRgNC+0LLQsNC9Jyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcHJvY2Vzc0dldFRpY2tldHMoIGRhdGEgKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIFBlcnNvbmFsT2ZmaWNlL3Byb2Nlc3NHZXRUaWNrZXRzIF0nLCBkYXRhKTtcbiAgICAgICAgbGV0IGxlbiA9IGRhdGEuQ3JlZGl0cy5sZW5ndGg7XG4gICAgICAgIGNvbnNvbGUubG9nKGxlbik7XG4gICAgICAgIGxldCB0ZW1wbGF0ZSA9IFxuICAgICAgICAnPGRpdiBjbGFzcz1cImxvYW5zX190YWJsZVwiPicgK1xuICAgICAgICAnICAgPGRpdiBjbGFzcz1cImxvYW5zX19yb3dcIj4nICtcbiAgICAgICAgJyAgICAgICA8ZGl2IGNsYXNzPVwibG9hbnNfX2dlbmVyYWwtY2VsbC1hY3RpdmVcIj7QndC+0LzQtdGAINCx0LjQu9C10YLQsDwvZGl2PicgK1xuICAgICAgICAnICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fZ2VuZXJhbC1jZWxsLWFjdGl2ZS1ldmVuXCI+0KHRgtCw0YLRg9GBINCx0LjQu9C10YLQsDwvZGl2PicgK1xuICAgICAgICAnICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fZ2VuZXJhbC1jZWxsLWFjdGl2ZVwiPtCU0LDRgtCwINC+0LrQvtC90YfQsNC90LjRjyDQt9Cw0LnQvNCwPC9kaXY+JyArXG4gICAgICAgICcgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19nZW5lcmFsLWNlbGwtYWN0aXZlLWV2ZW5cIj7QntGB0YLQsNC70L7RgdGMINC00L3QtdC5PC9kaXY+JyArXG4gICAgICAgICcgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19nZW5lcmFsLWNlbGwtYWN0aXZlXCI+0KHRg9C80LzQsCDQt9Cw0LnQvNCwLCDRgjwvZGl2PicgK1xuICAgICAgICAnICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fZ2VuZXJhbC1jZWxsLWFjdGl2ZS1ldmVuXCI+0J/RgNC+0YbQtdC90YIg0LfQsNC50LzQsDwvZGl2PicgK1xuICAgICAgICAnICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fZ2VuZXJhbC1jZWxsLWFjdGl2ZVwiPtCe0LHRidCw0Y8g0YHRg9C80LzQsCDQt9Cw0LTQvtC70LbQtdC90L3QvtGB0YLQuDwvZGl2PicgK1xuICAgICAgICAnICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fZ2VuZXJhbC1jZWxsLWFjdGl2ZS1ldmVuXCI+Jm5ic3A7PC9kaXY+JyArXG4gICAgICAgICcgICA8L2Rpdj4nICtcbiAgICAgICAgJyAgIDwlIGl0ZW1zLmZvckVhY2goZnVuY3Rpb24oaXRlbSkgeyAlPicgK1xuICAgICAgICAnICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fcm93XCI+JyArXG4gICAgICAgICcgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fY2VsbC1hY3RpdmVcIj4nICtcbiAgICAgICAgJyAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fY2VsbC1hY3RpdmVfX2Jsb2NrXCI+JyArXG4gICAgICAgICcgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJsb2Fuc19fbnVtYmVyLWFjdGl2ZVwiPjwlPSBpdGVtLlRpY2tldCAlPjwvc3Bhbj4nICtcbiAgICAgICAgJyAgICAgICAgICAgICAgIDwvZGl2PicgK1xuICAgICAgICAnICAgICAgICAgICA8L2Rpdj4nICtcbiAgICAgICAgJyAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19jZWxsLWFjdGl2ZS1ldmVuXCI+JyArXG4gICAgICAgICcgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19jZWxsLWFjdGl2ZS1ldmVuX19ibG9ja1wiPicgK1xuICAgICAgICAnICAgICAgICAgICAgICAgICAgIDwlPSBpdGVtLlN0YXR1cyAlPicgK1xuICAgICAgICAnICAgICAgICAgICAgICAgIDwvZGl2PicgK1xuICAgICAgICAnICAgICAgICAgICA8L2Rpdj4nICtcbiAgICAgICAgJyAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19jZWxsLWFjdGl2ZVwiPicgK1xuICAgICAgICAnICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fY2VsbC1hY3RpdmVfX2Jsb2NrXCI+JyArXG4gICAgICAgICcgICAgICAgICAgICAgICAgJm5ic3A7PC9kaXY+JyArXG4gICAgICAgICcgICAgICAgICAgIDwvZGl2PicgK1xuICAgICAgICAnICAgICAgICAgICA8ZGl2IGNsYXNzPVwibG9hbnNfX2NlbGwtYWN0aXZlLWV2ZW5cIj4nICtcbiAgICAgICAgJyAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibG9hbnNfX2NlbGwtYWN0aXZlLWV2ZW5fX2Jsb2NrXCI+JyArXG4gICAgICAgICcgICAgICAgICAgICAgICAgJm5ic3A7PC9kaXY+JyArXG4gICAgICAgICcgICAgICAgICAgIDwvZGl2PicgK1xuICAgICAgICAnICAgICAgICAgICA8ZGl2IGNsYXNzPVwibG9hbnNfX2NlbGwtYWN0aXZlXCI+JyArXG4gICAgICAgICcgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19jZWxsLWFjdGl2ZV9fYmxvY2tcIj4nICtcbiAgICAgICAgJyAgICAgICAgICAgICAgICAmbmJzcDs8L2Rpdj4nICtcbiAgICAgICAgJyAgICAgICAgICAgPC9kaXY+JyArXG4gICAgICAgICcgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fY2VsbC1hY3RpdmUtZXZlblwiPicgK1xuICAgICAgICAnICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fY2VsbC1hY3RpdmUtZXZlbl9fYmxvY2tcIj4nICtcbiAgICAgICAgJyAgICAgICAgICAgICAgICAmbmJzcDs8L2Rpdj4nICtcbiAgICAgICAgJyAgICAgICAgICAgPC9kaXY+JyArXG4gICAgICAgICcgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2Fuc19fY2VsbC1hY3RpdmVcIj4nICtcbiAgICAgICAgJyAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibG9hbnNfX2NlbGwtYWN0aXZlX19ibG9ja1wiPicgK1xuICAgICAgICAnICAgICAgICAgICAgICAgICZuYnNwOzwvZGl2PicgK1xuICAgICAgICAnICAgICAgICAgICA8L2Rpdj4nICtcbiAgICAgICAgJyAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19jZWxsLWFjdGl2ZS1ldmVuXCI+JyArXG4gICAgICAgICcgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYW5zX19jZWxsLWFjdGl2ZS1ldmVuX19ibG9ja1wiPicgK1xuICAgICAgICAnICAgICAgICAgICAgICAgICZuYnNwOzwvZGl2PicgK1xuICAgICAgICAnICAgICAgICAgICA8L2Rpdj4nICtcbiAgICAgICAgJyAgICAgICA8L2Rpdj4nICtcbiAgICAgICAgJyAgIDwlIH0pOyAlPicgK1xuICAgICAgICAnPC9kaXY+JztcbiAgICAgICAgbGV0IHRtcGx0ID0gXy50ZW1wbGF0ZSh0ZW1wbGF0ZSk7XG4gICAgICAgICR3cmFwLmh0bWwodG1wbHQoe2l0ZW1zOiBkYXRhLkNyZWRpdHN9KSk7XG4gICAgfVxuXHRzZW5kUmVxdWVzdChwYXJhbXMsIHJlc29sdmUpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1sgUGVyc29uYWxPZmZpY2Uvc2VuZFJlcXVlc3QgXScsIHBhcmFtcyk7XG5cbiAgICAgICAgQWpheC5nZXQocGFyYW1zLnJlcXVlc3QsIHBhcmFtcykudGhlbigoZGF0YSkgPT4ge1xuXHRcdFx0cmVzb2x2ZShkYXRhKTtcbiAgICAgICAgfSkuY2F0Y2goZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihlcnJvcik7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IHtcbiAgICBQZXJzb25hbE9mZmljZVxufTtcbiIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qIGdsb2JhbCAkICovXHJcblxyXG5sZXQgc2V0dGluZ3MgPSB7XHJcbiAgICBzbGlkZXJTZWxlY3RvcjogJy5qcy1wcm9kdWN0LWdhbGxlcnktc2xpZGVyJyxcclxuICAgIHNsaWRlckNvbnRhaW5lclNlbGVjdG9yOiAnLmpzLXByb2R1Y3QtZ2FsbGVyeS1zbGlkZXItY29udGFpbmVyJyxcclxuICAgIHNsaWRlckl0ZW1TZWxlY3RvcjogJy5qcy1wcm9kdWN0LWdhbGxlcnktc2xpZGVyLWl0ZW0nLFxyXG4gICAgc2xpZGVyUHJldlNlbGVjdG9yOiAnLmpzLXByb2R1Y3QtZ2FsbGVyeS1zbGlkZXItcHJldicsXHJcbiAgICBzbGlkZXJOZXh0U2VsZWN0b3I6ICcuanMtcHJvZHVjdC1nYWxsZXJ5LXNsaWRlci1uZXh0JyxcclxuICAgIHNsaWRlck5hdlNlbGVjdG9yOiAnLmpzLXByb2R1Y3QtZ2FsbGVyeS1zbGlkZXItbmF2JyxcclxuICAgIHNsaWRlck9wdGlvbnM6IHtcclxuICAgICAgICBkdXJhdGlvbjogNDAwLFxyXG4gICAgICAgIGxvb3A6IHRydWVcclxuICAgIH0sXHJcbiAgICBzY3JvbGxlclNlbGVjdG9yOiAnLmpzLXByb2R1Y3QtZ2FsbGVyeS1zY3JvbGxlcicsXHJcbiAgICBzY3JvbGxlckNvbnRhaW5lclNlbGVjdG9yOiAnLmpzLXByb2R1Y3QtZ2FsbGVyeS1zY3JvbGxlci1jb250YWluZXInLFxyXG4gICAgc2Nyb2xsZXJJdGVtU2VsZWN0b3I6ICcuanMtcHJvZHVjdC1nYWxsZXJ5LXNjcm9sbGVyLWl0ZW0nLFxyXG4gICAgc2Nyb2xsZXJPcHRpb25zOiB7XHJcbiAgICAgICAgZHVyYXRpb246IDIwMCxcclxuICAgICAgICBoaWRlQXJyczogdHJ1ZVxyXG4gICAgfVxyXG59O1xyXG5cclxuY2xhc3MgUHJvZHVjdEdhbGxlcnkge1xyXG4gICAgY29uc3RydWN0b3Iob3B0aW9ucykge1xyXG4gICAgXHR0aGlzLm9wdCA9ICQuZXh0ZW5kKHt9LCBzZXR0aW5ncywgb3B0aW9ucyk7XHJcbiAgICAgICAgdGhpcy5pbml0KCk7XHJcblx0fVxyXG4gICAgaW5pdCgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnWyBQcm9kdWN0R2FsbGVyeS9pbml0IF0nKTtcclxuXHJcbiAgICAgICAgbGV0ICRzbGlkZXIgPSAkKHRoaXMub3B0LnNsaWRlclNlbGVjdG9yKTtcclxuICAgICAgICBpZiAoJHNsaWRlci5sZW5ndGgpIHtcclxuICAgICAgICAgICAgbGV0ICRjb250YWluZXIgPSAkc2xpZGVyLmZpbmQodGhpcy5vcHQuc2xpZGVyQ29udGFpbmVyU2VsZWN0b3IpLFxyXG4gICAgICAgICAgICBcdCRpdGVtcyA9ICRzbGlkZXIuZmluZCh0aGlzLm9wdC5zbGlkZXJJdGVtU2VsZWN0b3IpLFxyXG4gICAgICAgICAgICAgICAgJHByZXYgPSAkc2xpZGVyLmZpbmQodGhpcy5vcHQuc2xpZGVyUHJldlNlbGVjdG9yKSxcclxuICAgICAgICAgICAgICAgICRuZXh0ID0gJHNsaWRlci5maW5kKHRoaXMub3B0LnNsaWRlck5leHRTZWxlY3RvciksXHJcbiAgICAgICAgICAgICAgICAkbmF2ID0gJHNsaWRlci5maW5kKHRoaXMub3B0LnNsaWRlck5hdlNlbGVjdG9yKTtcclxuXHJcbiAgICAgICAgICAgICRpdGVtcy5zbGlkZXIoJC5leHRlbmQoe30sIHRoaXMub3B0LnNsaWRlck9wdGlvbnMsIHtcclxuICAgICAgICAgICAgICAgIGNvbnRhaW5lcjogJGNvbnRhaW5lcixcclxuICAgICAgICAgICAgICAgIHByZXY6ICRwcmV2LFxyXG4gICAgICAgICAgICAgICAgbmV4dDogJG5leHQsXHJcbiAgICAgICAgICAgICAgICBuYXY6ICRuYXZcclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0ICRzY3JvbGxlciA9ICQodGhpcy5vcHQuc2Nyb2xsZXJTZWxlY3Rvcik7XHJcbiAgICAgICAgaWYgKCRzY3JvbGxlci5sZW5ndGgpIHtcclxuICAgICAgICBcdGxldCAkY29udGFpbmVyID0gJHNjcm9sbGVyLmZpbmQodGhpcy5vcHQuc2Nyb2xsZXJDb250YWluZXJTZWxlY3RvciksXHJcbiAgICAgICAgICAgICAgICAkaXRlbXMgPSAkc2Nyb2xsZXIuZmluZCh0aGlzLm9wdC5zY3JvbGxlckl0ZW1TZWxlY3Rvcik7XHJcblxyXG4gICAgICAgICAgICAkaXRlbXMuc2Nyb2xsZXIoJC5leHRlbmQoe30sIHRoaXMub3B0LnNjcm9sbGVyT3B0aW9ucywge1xyXG4gICAgICAgICAgICBcdGNvbnRhaW5lcjogJGNvbnRhaW5lclxyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQge1xyXG4gICAgUHJvZHVjdEdhbGxlcnlcclxufTtcclxuIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyogZ2xvYmFsICQgKi9cclxuXHJcbmxldCBzZXR0aW5ncyA9IHtcclxuICAgIHNlbGVjdG9yOiAnLmpzLXByb2R1Y3RzLXNsaWRlcicsXHJcbiAgICBpdGVtU2VsZWN0b3I6ICcuanMtcHJvZHVjdHMtc2xpZGVyX19pdGVtJyxcclxuICAgIHByZXZTZWxlY3RvcjogJy5qcy1wcm9kdWN0cy1zbGlkZXJfX3ByZXYnLFxyXG4gICAgbmV4dFNlbGVjdG9yOiAnLmpzLXByb2R1Y3RzLXNsaWRlcl9fbmV4dCcsXHJcbiAgICBzbGlkZXI6IHtcclxuICAgICAgICBkdXJhdGlvbjogNDAwLFxyXG4gICAgICAgIGxvb3A6IHRydWVcclxuICAgIH1cclxufTtcclxuXHJcbmNsYXNzIFByb2R1Y3RzU2xpZGVyIHtcclxuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMpIHtcclxuICAgICAgICB0aGlzLm9wdCA9ICQuZXh0ZW5kKHt9LCBzZXR0aW5ncywgb3B0aW9ucyk7XHJcbiAgICAgICAgdGhpcy5pbml0KCk7XHJcbiAgICB9XHJcbiAgICBpbml0KCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdbIFByb2R1Y3RzU2xpZGVyL2luaXQgXScpO1xyXG5cclxuICAgICAgICBsZXQgJGJsb2NrcyA9ICQodGhpcy5vcHQuc2VsZWN0b3IpO1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8ICRibG9ja3MubGVuZ3RoOyBpICsrKSB7XHJcbiAgICAgICAgICAgIGxldCAkYmxvY2sgPSAkYmxvY2tzLmVxKGkpO1xyXG5cclxuICAgICAgICAgICAgbGV0ICRpdGVtcyA9ICRibG9jay5maW5kKHRoaXMub3B0Lml0ZW1TZWxlY3RvciksXHJcbiAgICAgICAgICAgICAgICAkcHJldiA9ICRibG9jay5maW5kKHRoaXMub3B0LnByZXZTZWxlY3RvciksXHJcbiAgICAgICAgICAgICAgICAkbmV4dCA9ICRibG9jay5maW5kKHRoaXMub3B0Lm5leHRTZWxlY3Rvcik7XHJcblxyXG4gICAgICAgICAgICAkaXRlbXMuc2Nyb2xsZXIoJC5leHRlbmQoe30sIHRoaXMub3B0LnNsaWRlciwge1xyXG4gICAgICAgICAgICAgICAgcHJldjogJHByZXYsXHJcbiAgICAgICAgICAgICAgICBuZXh0OiAkbmV4dFxyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQge1xyXG4gICAgUHJvZHVjdHNTbGlkZXJcclxufTtcclxuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKiBnbG9iYWwgJCAqL1xuXG5pbXBvcnQgeyBicmVha3BvaW50cyB9IGZyb20gJ3NldHRpbmdzJztcblxuXG5jbGFzcyBSZXNwb25zaXZlIHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0ID0gJC5leHRlbmQoe30sIHsgYnJlYWtwb2ludHMgfSwgb3B0aW9ucyk7XG4gICAgICAgIHRoaXMuY3VycmVudFBvaW50ID0gbnVsbDtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuICAgIGluaXQoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIFJlc3BvbnNpdmUgXSBpbml0Jyk7XG5cbiAgICAgICAgdGhpcy4kd2luZG93Lm9uKCdyZXNpemUnLCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLl93YXRjaFNpemUoKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuX3dhdGNoU2l6ZSgpO1xuICAgIH1cbiAgICBfd2F0Y2hTaXplKCkge1xuICAgICAgICBsZXQgd2lkdGggPSB0aGlzLmdldFdpZHRoKCksXG4gICAgICAgICAgICBwb2ludCA9IHRoaXMuZ2V0UG9pbnQod2lkdGgpO1xuXG4gICAgICAgIGlmICghdGhpcy5jdXJyZW50UG9pbnQgfHwgcG9pbnQgIT09IHRoaXMuY3VycmVudFBvaW50KSB7XG4gICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UG9pbnQpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgWyBSZXNwb25zaXZlIF0gT2ZmOiAke3RoaXMuY3VycmVudFBvaW50fWApO1xuICAgICAgICAgICAgICAgIHRoaXMuJHdpbmRvdy50cmlnZ2VyKCdyZXNwb25zaXZlOm9mZjonICsgdGhpcy5jdXJyZW50UG9pbnQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgWyBSZXNwb25zaXZlIF0gT246ICR7cG9pbnR9YCk7XG4gICAgICAgICAgICB0aGlzLiR3aW5kb3cudHJpZ2dlcigncmVzcG9uc2l2ZTpvbjonICsgcG9pbnQpO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UG9pbnQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiR3aW5kb3cudHJpZ2dlcigncmVzcG9uc2l2ZTpjaGFuZ2UnLCB7IHBvaW50OiBwb2ludCB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kd2luZG93LnRyaWdnZXIoJ3Jlc3BvbnNpdmU6aW5pdCcsIHsgcG9pbnQ6IHBvaW50IH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRQb2ludCA9IHBvaW50O1xuICAgICAgICB9XG4gICAgfVxuICAgIGdldFdpZHRoKCkge1xuICAgICAgICByZXR1cm4gdGhpcy4kd2luZG93LndpZHRoKCk7XG4gICAgfVxuICAgIGdldFBvaW50KHdpZHRoKSB7XG4gICAgICAgIGxldCBwb2ludCA9IHRoaXMucG9pbnRzWzBdO1xuICAgICAgICBmb3IgKGxldCBpID0gdGhpcy5wb2ludHMubGVuZ3RoIC0gMTsgaSA+IDA7IGkgLS0pIHtcbiAgICAgICAgICAgIGlmICh3aWR0aCA+IHRoaXMub3B0LmJyZWFrcG9pbnRzW3RoaXMucG9pbnRzW2kgLSAxXV0gJiYgd2lkdGggPD0gdGhpcy5vcHQuYnJlYWtwb2ludHNbdGhpcy5wb2ludHNbaV1dKSB7XG4gICAgICAgICAgICAgICAgcG9pbnQgPSB0aGlzLnBvaW50c1tpXTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcG9pbnQ7XG4gICAgfVxuICAgIGdldCBwb2ludHMoKSB7XG4gICAgICAgIGlmICghdGhpcy5fX3BvaW50cykge1xuICAgICAgICAgICAgdGhpcy5fX3BvaW50cyA9IFtdO1xuICAgICAgICAgICAgZm9yIChsZXQgayBpbiB0aGlzLm9wdC5icmVha3BvaW50cykge1xuICAgICAgICAgICAgICAgIHRoaXMuX19wb2ludHMucHVzaChrKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuX19wb2ludHMuc29ydCgoYSwgYikgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiAodGhpcy5vcHQuYnJlYWtwb2ludHNbYV0gPCB0aGlzLm9wdC5icmVha3BvaW50c1tiXSkgPyAtMSA6ICgodGhpcy5vcHQuYnJlYWtwb2ludHNbYV0gPiB0aGlzLm9wdC5icmVha3BvaW50c1tiXSkgPyAxIDogMCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fX3BvaW50cztcbiAgICB9XG4gICAgZ2V0ICR3aW5kb3coKSB7XG4gICAgICAgIGlmICghdGhpcy5fXyR3aW5kb3cpIHtcbiAgICAgICAgICAgIHRoaXMuX18kd2luZG93ID0gJCh3aW5kb3cpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9fJHdpbmRvdztcbiAgICB9XG59XG5cblxuZXhwb3J0IHtcbiAgICBSZXNwb25zaXZlXG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKiBnbG9iYWwgJCAqL1xuXG5sZXQgc2V0dGluZ3MgPSB7XG4gICAgc3RpY2t5TWVudVNlbGVjdG9yOiAnLmpzLXN0aWNreS1tZW51JyxcbiAgICBtYWluTWVudVNlbGVjdG9yOiAnLmpzLW1haW4tbWVudSdcbn07XG5cbmNsYXNzIFN0aWNreU1lbnUge1xuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMpIHtcbiAgICAgICAgdGhpcy5vcHQgPSAkLmV4dGVuZCh7fSwgc2V0dGluZ3MsIG9wdGlvbnMpO1xuICAgICAgICB0aGlzLmluaXQoKTtcbiAgICB9XG4gICAgaW5pdCgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1sgU3RpY2t5TWVudS9pbml0IF0nKTtcblxuICAgICAgICAvKmxldCAkc3RpY2t5TWVudSA9ICQodGhpcy5vcHQuc3RpY2t5TWVudVNlbGVjdG9yKSxcbiAgICAgICAgICAgICRtYWluTWVudSA9ICQodGhpcy5vcHQubWFpbk1lbnVTZWxlY3Rvcik7Ki9cblxuICAgICAgICBsZXQgc2Nyb2xsSW50ZXJ2YWxJRCA9IHNldEludGVydmFsKHRoaXMuc3RpY2tJdCwgMTApO1xuICAgIH1cbiAgICBzdGlja0l0KCkge1xuICAgICAgICBsZXQgJHN0aWNreU1lbnUgPSAkKCcuanMtc3RpY2t5LW1lbnUnKSxcbiAgICAgICAgICAgICRtYWluTWVudSA9ICQoJy5qcy1tYWluLW1lbnUnKTtcblxuICAgICAgICBsZXQgb3JpZ2luYWxNZW51UG9zID0gJG1haW5NZW51Lm9mZnNldCgpLFxuICAgICAgICAgICAgb3JpZ2luYWxNZW51VG9wID0gb3JpZ2luYWxNZW51UG9zLnRvcDtcblxuICAgICAgICBpZiAoJCh3aW5kb3cpLnNjcm9sbFRvcCgpID49IChvcmlnaW5hbE1lbnVUb3ApKSAge1xuICAgICAgICAgICAgJG1haW5NZW51LmNzcygndmlzaWJpbGl0eScsICdoaWRkZW4nKTtcbiAgICAgICAgICAgICRzdGlja3lNZW51LnJlbW92ZUNsYXNzKCdpcy1oaWRkZW4nKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICRzdGlja3lNZW51LmFkZENsYXNzKCdpcy1oaWRkZW4nKTtcbiAgICAgICAgICAgICRtYWluTWVudS5jc3MoJ3Zpc2liaWxpdHknLCAndmlzaWJsZScpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5leHBvcnQge1xuICAgIFN0aWNreU1lbnVcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbi8qIGdsb2JhbCAkICovXG5cbmxldCBzZXR0aW5ncyA9IHtcbiAgICBsaXN0U2VsZWN0b3I6ICcuanMtdG9nZ2xlLW1lbnVfX2xpc3QnLFxuICAgIGl0ZW1TZWxlY3RvcjogJy5qcy10b2dnbGUtbWVudV9faXRlbScsXG4gICAgc3VibGlzdFNlbGVjdG9yOiAnLmpzLXRvZ2dsZS1tZW51X19zdWJsaXN0JyxcbiAgICB0cmlnZ2VyU2VsZWN0b3I6ICcuanMtdG9nZ2xlLW1lbnVfX3RyaWdnZXInXG59O1xuXG5jbGFzcyBUb2dnbGVNZW51IHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0ID0gJC5leHRlbmQoe30sIHNldHRpbmdzLCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuICAgIGluaXQoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIFRvZ2dsZU1lbnUvaW5pdCBdJyk7XG5cbiAgICAgICAgbGV0ICR0cmlnZ2VyID0gJCh0aGlzLm9wdC50cmlnZ2VyU2VsZWN0b3IpLFxuICAgICAgICAgICAgJGxpc3QgPSAkKHRoaXMub3B0Lmxpc3RTZWxlY3RvciksXG4gICAgICAgICAgICAkaXRlbSA9ICQodGhpcy5vcHQuaXRlbVNlbGVjdG9yKTtcblxuICAgICAgICAkdHJpZ2dlci5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAkbGlzdC50b2dnbGVDbGFzcygnaXMtb3BlbmVkJyk7XG4gICAgICAgICAgICAkdHJpZ2dlci50b2dnbGVDbGFzcygnaXMtYWN0aXZlJyk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IHtcbiAgICBUb2dnbGVNZW51XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG5cbmZ1bmN0aW9uIF9fZGV0ZWN0U2Vzc2lvbklEKCkge1xuICAgIGxldCBzZXNzaW9uSWQgPSAnJztcbiAgICBpZiAod2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCkubWF0Y2goL1NFU1NfSUQ9KFxcZCspLykpIHtcbiAgICAgICAgc2Vzc2lvbklkID0gUmVnRXhwLiQxO1xuICAgIH1cbiAgICByZXR1cm4gc2Vzc2lvbklkO1xufVxuXG5sZXQgZmFuY3lib3ggPSB7fTtcblxubGV0IGFqYXggPSB7XG4gICAgdHlwZTogJ3Bvc3QnLFxuICAgIHVybDogJy9qc29uLycsXG4gICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICB0aW1lb3V0OiAxMDAwMCxcbiAgICB0cmFkaXRpb25hbDogdHJ1ZSxcbiAgICBTRVNTSU9OX0lEOiBfX2RldGVjdFNlc3Npb25JRCgpXG59O1xuXG5sZXQgeWFDb3VudGVyID0gJ3lhQ291bnRlcjAwMDAwMCc7XG5cbmxldCBicmVha3BvaW50cyA9IHtcbiAgICB4eGw6IEluZmluaXR5LFxuICAgIHhsOiAxNjIwLFxuICAgIGw6IDEyODAsXG4gICAgbTogMTAwMCxcbiAgICBzOiA3ODAsXG4gICAgeHM6IDYwMCxcbiAgICB4eHM6IDQyMFxufTtcblxuXG5sZXQgaW50ZXJmYWNlTmFtZSA9IGRvY3VtZW50LmJvZHkuZGF0YXNldFsnaW50ZXJmYWNlJ107XG5cblxuZXhwb3J0IHtcbiAgICBmYW5jeWJveCxcbiAgICBhamF4LFxuICAgIGJyZWFrcG9pbnRzLFxuICAgIHlhQ291bnRlcixcblxuICAgIGludGVyZmFjZU5hbWVcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cbi8qIGdsb2JhbCAkICovXG5cbmltcG9ydCB7IGFqYXggfSBmcm9tICdzZXR0aW5ncyc7XG5cblxuY2xhc3MgQWpheCB7XG4gICAgc3RhdGljIGdldChhY3QsIGRhdGEsIG9wdGlvbnMpIHtcbiAgICAgICAgY29uc29sZS5sb2coYFsgQWpheC9nZXQgXSAke2FjdH1gLCBkYXRhKTtcblxuICAgICAgICBsZXQgb3B0ID0gJC5leHRlbmQoe30sIGFqYXgsIG9wdGlvbnMpO1xuXG4gICAgICAgIGlmIChvcHQuU0VTU0lPTl9JRCkge1xuICAgICAgICAgICAgaWYgKGRhdGEgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICAgICAgICAgIGRhdGEucHVzaCh7IG5hbWU6ICdTRVNTX0lEJywgdmFsdWU6IG9wdC5TRVNTSU9OX0lEIH0pO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChkYXRhIGluc3RhbmNlb2YgRm9ybURhdGEpIHtcbiAgICAgICAgICAgICAgICBkYXRhLmFwcGVuZCgnU0VTU19JRCcsIG9wdC5TRVNTSU9OX0lEKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZGF0YSBpbnN0YW5jZW9mIFVSTFNlYXJjaFBhcmFtcykge1xuICAgICAgICAgICAgICAgIGRhdGEuYXBwZW5kKCdTRVNTX0lEJywgb3B0LlNFU1NJT05fSUQpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgZGF0YSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICBpZiAoZGF0YSAhPSAnJykgZGF0YSArPSAnJic7XG4gICAgICAgICAgICAgICAgZGF0YSArPSAnU0VTU19JRD0nK29wdC5TRVNTSU9OX0lEO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgZGF0YSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICAgICAgICBkYXRhLlNFU1NfSUQgPSBvcHQuU0VTU0lPTl9JRDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdDYW5cXCd0IGFwcGVuZCBTRVNTSU9OX0lEIHRvIHRoaXMgdHlwZSBvZiBkYXRhJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgZGVsZXRlIG9wdC5TRVNTSU9OX0lEO1xuXG4gICAgICAgIGlmICghL15cXC8vLnRlc3QoYWN0KSkge1xuICAgICAgICAgICAgb3B0LnVybCArPSBhY3Q7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBvcHQudXJsID0gYWN0O1xuICAgICAgICB9XG4gICAgICAgIGlmICghL1xcLyQvLnRlc3Qob3B0LnVybCkpIHtcbiAgICAgICAgICAgIG9wdC51cmwgKz0gJy8nO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGRhdGEgaW5zdGFuY2VvZiBGb3JtRGF0YSB8fCBkYXRhIGluc3RhbmNlb2YgVVJMU2VhcmNoUGFyYW1zKSB7XG4gICAgICAgICAgICBvcHQuY29udGVudFR5cGUgPSBmYWxzZTtcbiAgICAgICAgICAgIG9wdC5wcm9jZXNzRGF0YSA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgb3B0LmRhdGEgPSBkYXRhO1xuXG4gICAgICAgIGxldCBoYW5kbGVyID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICBvcHQuc3VjY2VzcyA9IGZ1bmN0aW9uKGRhdGEsIHRleHRTdGF0dXMsIGpxWEhSKSB7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZShkYXRhLCBqcVhIUik7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgb3B0LmVycm9yID0gZnVuY3Rpb24oanFYSFIpIHtcbiAgICAgICAgICAgICAgICByZWplY3QoanFYSFIuc3RhdHVzVGV4dCwganFYSFIpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICQuYWpheChvcHQpO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gaGFuZGxlcjtcbiAgICB9XG59XG5cblxuZXhwb3J0IHsgQWpheCB9O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG5pbXBvcnQgeyBZYW5kZXhNZXRyaWthIH0gZnJvbSAnLi9jb3VudGVycy95YW5kZXgtbWV0cmlrYSc7XG5pbXBvcnQgeyBHb29nbGVBbmFseXRpY3MgfSBmcm9tICcuL2NvdW50ZXJzL2dvb2dsZS1hbmFseXRpY3MnO1xuXG5cbmNvbnN0IGNvdW50ZXJzID0gW1xuICAgIG5ldyBZYW5kZXhNZXRyaWthKCksXG4gICAgbmV3IEdvb2dsZUFuYWx5dGljcygpXG5dO1xuXG5cbmV4cG9ydCB7XG4gICAgY291bnRlcnNcbn07XG4iLCIndXNlIHN0cmljdCc7XG5cblxuY2xhc3MgQWJzdHJhY3RDb3VudGVyIHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0ID0gb3B0aW9ucztcbiAgICB9XG4gICAgcmVhY2hHb2FsKCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1lvdSBtdXN0IGRlZmluZSB5b3VyIG93biBtZXRob2QnKTtcbiAgICB9XG4gICAgZ2V0IHR5cGUoKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignWW91IG11c3QgZGVmaW5lIHlvdXIgb3duIG1ldGhvZCcpO1xuICAgIH1cbiAgICBnZXQgY291bnRlcigpIHtcbiAgICAgICAgaWYgKCF0aGlzLl9fY291bnRlcikge1xuICAgICAgICAgICAgdGhpcy5fX2NvdW50ZXIgPSB0aGlzLm9wdC5jb3VudGVyIHx8IHdpbmRvd1t0aGlzLm9wdC5jb3VudGVyTmFtZV07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX19jb3VudGVyO1xuICAgIH1cbn1cblxuXG5leHBvcnQge1xuICAgIEFic3RyYWN0Q291bnRlclxufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyogZ2xvYmFsICQgKi9cblxuaW1wb3J0IHsgQWJzdHJhY3RDb3VudGVyIH0gZnJvbSAnLi9hYnN0cmFjdC1jb3VudGVyJztcblxuXG5jbGFzcyBHb29nbGVBbmFseXRpY3MgZXh0ZW5kcyBBYnN0cmFjdENvdW50ZXIge1xuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMpIHtcbiAgICAgICAgc3VwZXIoJC5leHRlbmQoe30sIHsgY291bnRlck5hbWU6ICdnYScgfSwgb3B0aW9ucykpO1xuICAgIH1cbiAgICByZWFjaEdvYWwoZ29hbCwgcGFyYW1zPXt9KSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGBbIEdvb2dsZUFuYWx5dGljcy9yZWFjaEdvYWwgXSAke2dvYWx9YCwgcGFyYW1zKTtcblxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdGhpcy5jb3VudGVyKCdzZW5kJywgJ2V2ZW50JywgZ29hbCwgcGFyYW1zKTtcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhgWyBHb29nbGVBbmFseXRpY3MgXSBHb2FsICR7Z29hbH0gaXMgcmVhY2hlZCBzdWNjZXNzZnVsbHlgKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGBbIEdvb2dsZUFuYWx5dGljcyBdIENhbid0IHJlYWNoIGdvYWwgJHtnb2FsfWAsIGVycik7XG4gICAgICAgIH1cbiAgICB9XG4gICAgc2V0UGFyYW1zKHBhcmFtcykge1xuICAgICAgICBjb25zb2xlLmxvZygnWyBHb29nbGVBbmFseXRpY3Mvc2V0UGFyYW1zIF0nLCBwYXJhbXMpO1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmNvdW50ZXIoJ3NldCcsIHBhcmFtcyk7XG4gICAgICAgICAgICBjb25zb2xlLmluZm8oJ1sgR29vZ2xlQW5hbHl0aWNzIF0gUGFyYW1zIGFyZSBzZXQgc3VjY2Vzc2Z1bGx5Jyk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcignWyBHb29nbGVBbmFseXRpY3MgXSBDYW5cXCd0IHNldCBwYXJhbXMnLCBlcnIpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGdldCB0eXBlKCkge1xuICAgICAgICByZXR1cm4gJ2dvb2dsZS1hbmFseXRpY3MnO1xuICAgIH1cbn1cblxuXG5leHBvcnQge1xuICAgIEdvb2dsZUFuYWx5dGljc1xufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyogZ2xvYmFsICQgKi9cblxuaW1wb3J0IHsgeWFDb3VudGVyIH0gZnJvbSAnc2V0dGluZ3MnO1xuaW1wb3J0IHsgQWJzdHJhY3RDb3VudGVyIH0gZnJvbSAnLi9hYnN0cmFjdC1jb3VudGVyJztcblxuXG5jbGFzcyBZYW5kZXhNZXRyaWthIGV4dGVuZHMgQWJzdHJhY3RDb3VudGVyIHtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zKSB7XG4gICAgICAgIHN1cGVyKCQuZXh0ZW5kKHt9LCB7IGNvdW50ZXJOYW1lOiB5YUNvdW50ZXIgfSwgb3B0aW9ucykpO1xuICAgIH1cbiAgICByZWFjaEdvYWwoZ29hbCwgcGFyYW1zPXt9KSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGBbIFlhbmRleE1ldHJpa2EvcmVhY2hHb2FsIF0gJHtnb2FsfWAsIHBhcmFtcyk7XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHRoaXMuY291bnRlci5yZWFjaEdvYWwoZ29hbCwgcGFyYW1zKTtcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhgWyBZYW5kZXhNZXRyaWthIF0gR29hbCAke2dvYWx9IGlzIHJlYWNoZWQgc3VjY2Vzc2Z1bGx5YCk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihgWyBZYW5kZXhNZXRyaWthIF0gQ2FuJ3QgcmVhY2ggZ29hbCAke2dvYWx9YCwgZXJyKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBzZXRQYXJhbXMocGFyYW1zKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdbIFlhbmRleE1ldHJpa2Evc2V0UGFyYW1zIF0nLCBwYXJhbXMpO1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmNvdW50ZXIucGFyYW1zKHBhcmFtcyk7XG4gICAgICAgICAgICBjb25zb2xlLmluZm8oJ1sgWWFuZGV4TWV0cmlrYSBdIFBhcmFtcyBhcmUgc2V0IHN1Y2Nlc3NmdWxseScpO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ1sgWWFuZGV4TWV0cmlrYSBdIENhblxcJ3Qgc2V0IHBhcmFtcycsIGVycik7XG4gICAgICAgIH1cbiAgICB9XG4gICAgZ2V0IHR5cGUoKSB7XG4gICAgICAgIHJldHVybiAneWFuZGV4LW1ldHJpa2EnO1xuICAgIH1cbn1cblxuXG5leHBvcnQge1xuICAgIFlhbmRleE1ldHJpa2Fcbn07XG4iXX0=
