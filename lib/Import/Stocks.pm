package astlombard::lib::Import::Stocks;

use strict;
use utf8;

use base qw/astlombard::lib::Import::Core/;

use Engine::GlobalVars;
use Engine::SysUtils;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(@_);
    bless $self, $class;
    return $self;
}

# ================================================================================

sub Start {
    my $self = shift;
    my (%params) = @_;

    
}

# ================================================================================

sub ImportObjectStock {
    my $self = shift;
    my ($stock_id) = @_;

    if (my $stock = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id => $stock_id, attrs => ['lombards'])->fetchrow_hashref()) {
        print "[ INFO ] [ Import::Stocks::ImportObjectStock ] Processing stock $stock->{'object_name'}\n";

        my @fields = (
            { name => 'lombard_code', type => 'string' },
            { name => 'start_date', type => 'date' },
            { name => 'finish_date', type => 'date' }
        );

        my $string = $stock->{'lombards'};

        open my $fh, "<:utf8", \$string or die "Can't open string as stream: $!";
        my $CSV = $self->CSV();

        my $lombards = $self->_getLombardRel(fields => 'lombard_id, lombard_code');
        my @lombards = ();

        while (my $row = $CSV->getline($fh)) {
            my $item = {};
            for (my $i = 0; $i < @fields; $i ++) {
                my $field = $fields[$i];
                $item->{ $field->{'name'} } = $self->_processField($field->{'type'}, $row->[$i]);
            }

            if (my $lombard = $lombards->{ $item->{'lombard_code'} }) {
                print "[ DEBUG ] [ Import::Stocks::ImportObjectStock ] Lombard $item->{'lombard_code'} is found\n";

                delete $item->{'lombard_code'};
                $item->{'lombard_id'} = $lombard->{'lombard_id'};
                push @lombards, $item;
            } else {
                print "[ WARNING ] [ Import::Stocks::ImportObjectStock ] Cannon find lombard $item->{'lombard_code'}\n";
            }
        }

        close $fh;

        $DB->LOMBARDS_UPDATE_STOCK_LOMBARDS(stock_id => $stock->{'object_id'}, lombards => \@lombards);
    }
}

1;
