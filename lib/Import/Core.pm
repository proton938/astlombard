package astlombard::lib::Import::Core;

use strict;
use utf8;

use Engine::GlobalVars;

use Text::CSV;
use Digest::MD5 qw(md5_hex);
use Encode qw(encode_utf8);

use constant DAYS => {
    "Пн" => 1,
    "Вт" => 2,
    "Ср" => 3,
    "Чт" => 4,
    "Пт" => 5,
    "Сб" => 6,
    "Вс" => 7
};

# ================================================================================

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = { @_ };
    bless $self, $class;
    return $self;
}

# ================================================================================

sub _getLombardRel {
    my $self = shift;
    my (%params) = @_;

    return $DB->LOMBARDS_GET_LOMBARDS(%params)->fetchall_hashref('lombard_code');
}

sub _getRegionRel {
    my $self = shift;
    my (%params) = @_;

    return $DB->LOMBARDS_GET_REGIONS(%params)->fetchall_hashref('region_name');
}

sub _getCityRel {
    my $self = shift;
    my (%params) = @_;

    return $DB->LOMBARDS_GET_CITIES(%params)->fetchall_hashref(['region_id', 'city_name']);
}

# ================================================================================

sub _processField {
    my $self = shift;
    my ($type, $value) = @_;

    $value =~ s/^\s+//g;
    $value =~ s/\s+$//g;
    $value =~ s/\x{feff}//;

    if ($type eq 'number') {
        $value =~ s/\,/./g;
        $value =~ s/\s+//g;
        $value *= 1;
    } elsif ($type eq 'bool') {
        $value = ($value) ? 1 : 0;
    } elsif ($type eq 'date') {
        $value =~ s/^(\d+)\.(\d+)\.(\d+).*?$/$3-$2-$1/;
    } elsif ($type eq 'hours') {
        my @hours = ();
        foreach my $row (split /\s*\;\s*/, $value) {
            my ($dname, $start_time, $finish_time) = split /\s*\-\s*/, $row;
            my $day = DAYS->{$dname};

            if (!defined $day or (!$start_time and !$finish_time)) {
                print "[ WARNING ] [ Import::Core::_processField / hours ] Wrong row: $row\n";
                next;
            }

            $start_time ||= '00:00';
            $finish_time ||= '23:59';

            if ($start_time gt $finish_time) {
                print "[ WARNING ] [ Import::Core::_processField / hours ] Start time $start_time is greater then finish time $finish_time\n";
                next;
            }

            push @hours, {
                day => $day,
                start_time => $start_time,
                finish_time => $finish_time
            };
        }
        $value = \@hours;
    }

    return $value;
}

# ================================================================================

sub GetFileSum {
    my $self = shift;
    my ($filename) = @_;
    
    return '' if (!-f $filename);
    (my $md5sum = `md5sum -b '$filename'`) =~ s/\s\*.*$//;
    chomp $md5sum;
    return $md5sum;
}

sub GetVarSum {
    my $self = shift;
    my ($var) = @_;

    return md5_hex($self->SerializeVar($var));
}

sub SerializeVar {
    my $self = shift;
    my ($var) = @_;

    if (!ref $var) {
        return encode_utf8($var // '__UNDEF__');
    } elsif (ref $var eq 'ARRAY') {
        return join '__AJOIN__', map { $self->SerializeVar($_) } @$var;
    } elsif (ref $var eq 'HASH') {
        return join '__HJOIN__', map { $_ . '__EQ__' . $self->SerializeVar($var->{$_}) } (sort keys %$var);
    }
}

# ================================================================================

sub _getTimestamp {
    my $self = shift;
    my ($time, $diff) = @_;

    my ($h, $m) = split /:/, $time;
    return $h*60 + $m - $diff;
}

# ================================================================================

sub CSV {
    my $self = shift;
    if (!exists $self->{'__CSV'}) {
        $self->{'__CSV'} = Text::CSV->new({
            sep_char => '^',
            quote_char => '',
            escape_char => '',
            allow_whitespace => 1,
            @_
        }) or die "Can't create CSV: " . Text::CSV->error_diag;
    }
    return $self->{'__CSV'};
}

1;
