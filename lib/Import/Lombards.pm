package astlombard::lib::Import::Lombards;

use strict;
use utf8;

use base qw/astlombard::lib::Import::Core/;

use Engine::GlobalVars;
use Engine::SysUtils;
use Data::Dumper;

use astlombard::lib::Geocode;

# Смещение времени в минутах относительно серверного
use constant TIMEOFFSETS => {
    "Астана область" => 180,
    "Актобенская область" => 120
};

# Количество минут в сутках
use constant DAY => 1440;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(@_);
    bless $self, $class;
    return $self;
}

# ================================================================================

sub Start {
    my $self = shift;
    my (%params) = @_;

    my @fields = (
        { name => 'lombard_code', type => 'string' },

        { name => 'region', type => 'string' },
        { name => 'city', type => 'string' },
        { name => 'lombard_address', type => 'string' },

        { name => 'lombard_name', type => 'string' },

        { name => 'work_hours', type => 'hours' },
        { name => 'lunch_hours', type => 'hours' },

        { name => 'buy__jewellery', type => 'number' },
        { name => 'buy__appliance', type => 'number' },
        { name => 'buy__cars', type => 'number' },

        { name => 'sale__jewellery', type => 'number' },
        { name => 'sale__appliance', type => 'number' },
        { name => 'sale__cars', type => 'number' },

        { name => 'is_24hours', type => 'bool' }
    );

    my $filepath = $path.$DB->SYS_OPTION_VALUE(name => 'Import.Lombards.File');
    if (-e $filepath) {
        my $old_sum = $DB->SYS_OPTION_VALUE(name => 'Import.Lombards.Hash');
        my $new_sum = $self->GetFileSum($filepath);

        if (!$params{'force'} and $new_sum eq $old_sum) {
            print "[ INFO ] [ Import::Lombards::Start ] $filepath is not modified\n";
            return 1;
        }

        my @lombards = ();
    
        open my $fh, "<:utf8", $filepath or die "Can't open $filepath: $!";
        my $CSV = $self->CSV();

        while (my $row = $CSV->getline($fh)) {
            my $item = {};
            for (my $i = 0; $i < @fields; $i ++) {
                my $field = $fields[$i];
                $item->{ $field->{'name'} } = $self->_processField($field->{'type'}, $row->[$i]);
            }
            $item->{'slices_work_hours'} = $self->_constructSlices($item->{'work_hours'}, $item->{'region'});
            $item->{'slices_lunch_hours'} = $self->_constructSlices($item->{'lunch_hours'}, $item->{'region'});
            push @lombards, $item;
        }

        close $fh;

        my $old_lombards = $self->_getLombardRel();
        my $old_regions = $self->_getRegionRel();
        my $old_cities = $self->_getCityRel();

        my $new_lombards = {};
        my $new_regions = {};
        my $new_cities = {};

        my $groups = $DB->CAT_MEGAGET(
            class_name => 'prod_group',
            attrs => ['mark']
        )->fetchall_hashref('mark');

        foreach my $lombard (@lombards) {
            if ($lombard->{'region'}) {
                $lombard->{'region_id'} = $self->_importRegion($lombard, $old_regions, $new_regions);
            } else {
                $lombard->{'region_id'} = undef;
            }
            delete $lombard->{'region'};

            if ($lombard->{'city'}) {
                $lombard->{'city_id'} = $self->_importCity($lombard, $old_cities, $new_cities);
            } else {
                $lombard->{'city_id'} = undef;
            }
            delete $lombard->{'city'};

            $lombard->{'hours'} = [];
            if ($lombard->{'work_hours'}) {
                push @{ $lombard->{'hours'} }, map { { %$_, type => 'work' } } @{ $lombard->{'work_hours'} };
            }
            delete $lombard->{'work_hours'};
            if ($lombard->{'lunch_hours'}) {
                push @{ $lombard->{'hours'} }, map { { %$_, type => 'lunch' } } @{ $lombard->{'lunch_hours'} };
            }
            delete $lombard->{'lunch_hours'};

            $lombard->{'timestamps'} = [];
            if ($lombard->{'slices_work_hours'}) {
                push @{ $lombard->{'timestamps'} }, map { { %$_, type => 'work' } } @{ $lombard->{'slices_work_hours'} };
            }
            delete $lombard->{'slices_work_hours'};
            if ($lombard->{'slices_lunch_hours'}) {
                push @{ $lombard->{'timestamps'} }, map { { %$_, type => 'lunch' } } @{ $lombard->{'slices_lunch_hours'} };
            }
            delete $lombard->{'slices_lunch_hours'};

            $lombard->{'services'} = [];
            foreach my $k (keys %$lombard) {
                if ($k =~ /^(buy|sale)__(\w+)$/) {
                    my $value = $lombard->{$k};
                    delete $lombard->{$k};

                    if ($value) {
                        my ($type, $code) = ($1, $2);
                        if (my $group = $groups->{ $code }) {
                            push @{ $lombard->{'services'} }, { type => $type, service_id => $group->{'object_id'} };
                        }
                    }
                } elsif ($k eq 'is_24hours') {
                    my $value = $lombard->{$k};
                    delete $lombard->{$k};

                    $lombard->{'24hours'} = $value;
                }
            }

            $self->_importLombard($lombard, $old_lombards, $new_lombards, $params{'force'});
        }

        $self->_clearLombards($old_lombards);
        $self->_clearRegions($old_regions);
        $self->_clearCities($old_cities);
        $self->_geocodeLombards();

        $DB->SYS_OPTION_VALUE_UPDATE(name => 'Import.Lombards.Hash', value => $new_sum);

        return 1;
    } else {
        print "[ WARNING ] [ Import::Lombards::Start ] $filepath is not found\n";
        return undef;
    }
}

# ================================================================================

sub _importRegion {
    my $self = shift;
    my ($lombard, $old_rel, $new_rel) = @_;

    my $region_id = undef;

    if (!exists $old_rel->{ $lombard->{'region'} }) {
        if (!exists $new_rel->{ $lombard->{'region'} }) {
            my $data = {
                region_name => $lombard->{'region'}
            };
            if ($region_id = $DB->LOMBARDS_CREATE_REGION(%$data)) {
                $new_rel->{ $lombard->{'region'} } = $region_id;
                print "[ DEBUG ] [ Import::Lombards::_importRegion ] Region $lombard->{'region'} is created\n";
            } else {
                print "[ ERROR ] [ Import::Lombards::_importRegion ] Region $lombard->{'region'} is not created\n";
            }
        } else {
            $region_id = $new_rel->{ $lombard->{'region'} };
        }
    } elsif (exists $new_rel->{ $lombard->{'region'} }) {
        $region_id = $new_rel->{ $lombard->{'region'} };
    } elsif (exists $old_rel->{ $lombard->{'region'} }) {
        $region_id = $old_rel->{ $lombard->{'region'} }->{'region_id'};
    }
    delete $old_rel->{ $lombard->{'region'} };

    return $region_id;
}

sub _importCity {
    my $self = shift;
    my ($lombard, $old_rel, $new_rel) = @_;

    my $city_id = undef;

    if (!exists $old_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} }) {
        if (!exists $new_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} }) {
            my $data = {
                city_name => $lombard->{'city'},
                region_id => $lombard->{'region_id'}
            };
            if ($city_id = $DB->LOMBARDS_CREATE_CITY(%$data)) {
                $new_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} } = $city_id;
                print "[ DEBUG ] [ Import::Lombards::_importCity ] City $lombard->{'region_id'} / $lombard->{'city'} is created\n";
            } else {
                print "[ ERROR ] [ Import::Lombards::_importCity ] City $lombard->{'region_id'} / $lombard->{'city'} is not created\n";
            }
        } else {
            $city_id = $new_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} };
        }
    } elsif (exists $new_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} }) {
        $city_id = $new_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} };
    } elsif (exists $old_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} }) {
        $city_id = $old_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} }->{'city_id'};
    }
    delete $old_rel->{ $lombard->{'region_id'} }->{ $lombard->{'city'} };

    return $city_id;
}

sub _importLombard {
    my $self = shift;
    my ($lombard, $old_rel, $new_rel, $force) = @_;

    $lombard->{'hash'} = $self->GetVarSum($lombard);
    $lombard->{'slink'} = TranslitName($lombard->{'lombard_address'});

    my $res = undef;
    if (exists $new_rel->{ $lombard->{'lombard_code'} }) {
        print "[ ERROR ] [ Import::Lombards::_importLombard ] Lombard $lombard->{'lombard_code'} is duplicated\n";
    } elsif (my $old_lombard = $old_rel->{ $lombard->{'lombard_code'} }) {
        if ($force or $lombard->{'hash'} ne $old_lombard->{'hash'}) {
            if (grep { $lombard->{ $_ } ne $old_lombard->{ $_ } } qw/region_id city_id address/) {
                $lombard->{'lombard_coords'} = '';
            }
            if ($DB->LOMBARDS_UPDATE_LOMBARD(lombard_id => $old_lombard->{'lombard_id'}, %$lombard)) {
                $new_rel->{ $lombard->{'lombard_code'} } = $old_lombard->{'lombard_id'};
                $res = $old_lombard->{'lombard_id'};
                print "[ DEBUG ] [ Import::Lombards::_importLombard ] Lombard $lombard->{'lombard_code'} is updated\n";
            } else {
                print "[ ERROR ] [ Import::Lombards::_importLombard ] Lombard $lombard->{'lombard_code'} is not updated\n";
            }
        } else {
            $new_rel->{ $lombard->{'lombard_code'} } = $old_lombard->{'lombard_id'};
            $res = $old_lombard->{'lombard_id'};
            print "[ DEBUG ] [ Import::Lombards::_importLombard ] Lombard $lombard->{'lombard_code'} is not modified\n";
        }
        delete $old_rel->{ $lombard->{'lombard_code'} }
    } else {
        $lombard->{'lombard_coords'} = '';
        if (my $id = $DB->LOMBARDS_CREATE_LOMBARD(%$lombard)) {
            $new_rel->{ $lombard->{'lombard_code'} } = $id;
            $res = $id;
            print "[ DEBUG ] [ Import::Lombards::_importLombard ] Lombard $lombard->{'lombard_code'} is created\n";
        } else {
            print "[ ERROR ] [ Import::Lombards::_importLombard ] Lombard $lombard->{'lombard_code'} is not created\n";
        }
    }
    return $res;
}

sub _clearRegions {
    my $self = shift;
    my ($old_rel) = @_;

    foreach my $region (keys %$old_rel) {
        if ($DB->LOMBARDS_DELETE_REGION(region_id => $old_rel->{ $region }->{'region_id'})) {
            print "[ DEBUG ] [ Import::Lombards::_clearRegions ] Region $region is deleted\n";
        } else {
            print "[ ERROR ] [ Import::Lombards::_clearRegions ] Region $region is not deleted\n";
        }
    }
}

sub _clearCities {
    my $self = shift;
    my ($old_rel) = @_;

    foreach my $region (keys %$old_rel) {
        foreach my $city (keys %{ $old_rel->{ $region } }) {
            if ($DB->LOMBARDS_DELETE_CITY(city_id => $old_rel->{ $region }->{ $city }->{'city_id'})) {
                print "[ DEBUG ] [ Import::Lombards::_clearCities ] City $region / $city is deleted\n";
            } else {
                print "[ ERROR ] [ Import::Lombards::_clearCities ] City $region / $city is not deleted\n";
            }
        }
    }
}

sub _clearLombards {
    my $self = shift;
    my ($old_rel) = @_;

    foreach my $lombard_code (keys %$old_rel) {
        if ($DB->LOMBARDS_DELETE_LOMBARD(lombard_id => $old_rel->{ $lombard_code }->{'lombard_id'})) {
            print "[ DEBUG ] [ Import::Lombards::_clearLombards ] Lombard $lombard_code is deleted\n";
        } else {
            print "[ ERROR ] [ Import::Lombards::_clearLombards ] Lombard $lombard_code is not deleted\n";
        }
    }
}

sub _geocodeLombards {
    my $self = shift;

    my $regions = $DB->LOMBARDS_GET_REGIONS()->fetchall_hashref('region_id');
    my $cities = $DB->LOMBARDS_GET_CITIES()->fetchall_hashref('city_id');
    my $lombards = $DB->LOMBARDS_GET_LOMBARDS(lombard_coords => '')->fetchall_arrayref({});

    foreach my $lombard (@$lombards) {
        my $region = $regions->{ $lombard->{'region_id'} };
        my $city = $cities->{ $lombard->{'city_id'} };

        if (!$region) {
            print "[ ERROR ] [ Import::Lombards::_geocodeLombards ] Region is not found\n";
        } elsif (!$city) {
            print "[ ERROR ] [ Import::Lombards::_geocodeLombards ] City $region is not found\n";
        } else {
            my $address = join ', ', grep { $_ ne '' } ($region->{'region_name'}, $city->{'city_name'}, $lombard->{'lombard_address'});
        
            if (my $geocode = $self->Geocode->Geocode($address)) {
                $lombard->{'lombard_coords'} = $geocode->{'latitude'} . ', ' . $geocode->{'longitude'};

                if ($DB->LOMBARDS_UPDATE_LOMBARD(lombard_id => $lombard->{'lombard_id'}, lombard_coords => $lombard->{'lombard_coords'})) {
                    print "[ DEBUG ] [ Import::Lombards::_geocodeLombards ] Lombard $lombard->{'lombard_code'} is updated\n";
                } else {
                    print "[ ERROR ] [ Import::Lombards::_geocodeLombards ] Lombard $lombard->{'lombard_code'} is not updated\n";
                }
            } else {
                print "[ ERROR ] [ Import::Lombards::_geocodeLombards ] Geocoding error\n";
            }
        }
    }
}

sub _constructSlices {
    my $self = shift;
    my ($hours, $region) = @_;

    my $offset = TIMEOFFSETS->{ $region };

    if (!defined $offset) {
        print "[ ERROR ] [ Import::Lombards::_constructSlices ] Can't find timeoffset for region $region\n";
        return [];
    }

    my @slices = ();
    my $slice = {};
    foreach my $hour (@$hours) {
        my $start_stamp = $self->_getTimestamp($hour->{'start_time'}, $offset);
        my $finish_stamp = $self->_getTimestamp($hour->{'finish_time'}, $offset);

        my ($prev_day, $next_day, $cur_day) = ({}, {}, {});

        if ($start_stamp < 0) {
            $prev_day->{'start_time'} = $start_stamp + DAY;
        } elsif ($start_stamp > DAY - 1) {
            $next_day->{'start_time'} = $start_stamp - DAY;
        } else {
            $cur_day->{'start_time'} = $start_stamp;
        }

        if ($finish_stamp < 0) {
            $prev_day->{'finish_time'} = $finish_stamp + DAY;
        } elsif ($finish_stamp > DAY - 1) {
            $next_day->{'finish_time'} = $finish_stamp - DAY;
        } else {
            $cur_day->{'finish_time'} = $finish_stamp;
        }

        my $start_time = undef;
        my $finish_time = undef;
        foreach my $d ($prev_day, $cur_day, $next_day) {
            next if (!%$d);
            $d->{'start_time'} //= 0;
            $d->{'finish_time'} //= DAY - 1;
        }

        if (%$prev_day) {
            my $d = $hour->{'day'} - 1;
            $d = 6 if ($d < 0);
            $slice = {
                day => $d,
                start_time => $prev_day->{'start_time'},
                finish_time => $prev_day->{'finish_time'}
            };
            push @slices, $slice;
        }
        if (%$next_day) {
            my $d = $hour->{'day'} + 1;
            $d = 1 if ($d > 6);
            $slice = {
                day => $d,
                start_time => $next_day->{'start_time'},
                finish_time => $next_day->{'finish_time'}
            };
            push @slices, $slice;
        }
        if (%$cur_day) {
            $slice = {
                day => $hour->{'day'},
                start_time => $cur_day->{'start_time'},
                finish_time => $cur_day->{'finish_time'}
            };
            push @slices, $slice;
        }
    }

    return \@slices;
}

# ================================================================================

sub Geocode {
    my $self = shift;
    if (!exists $self->{'__GEOCODE'}) {
        $self->{'__GEOCODE'} = astlombard::lib::Geocode->new(@_);
    }
    return $self->{'__GEOCODE'};
}

1;
