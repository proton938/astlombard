package astlombard::lib::Import;

use strict;
use utf8;

use astlombard::lib::Import::Lombards;
use astlombard::lib::Import::Stocks;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;
    my $self = { @_ };
    bless $self, $class;
    return $self;
}

# ================================================================================

sub Start {
	my $self = shift;
    my %params = map { $_ => 1 } @_;
    
    if (exists $params{'--lombards'} or exists $params{'--all'}) {
        $self->Lombards->Start(force => $params{'--force'});
    }
}

sub Lombards {
	my $self = shift;
    if (!exists $self->{'__LOMBARDS'}) {
        $self->{'__LOMBARDS'} = astlombard::lib::Import::Lombards->new(@_);
    }
    return $self->{'__LOMBARDS'};
}

sub Stocks {
    my $self = shift;
    if (!exists $self->{'__STOCKS'}) {
        $self->{'__STOCKS'} = astlombard::lib::Import::Stocks->new(@_);
    }
    return $self->{'__STOCKS'};
}

1;
