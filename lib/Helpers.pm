package astlombard::lib::Helpers;

use utf8;
use strict;

use Engine::GlobalVars;
use Engine::Helpers;
use base qw(Engine::Helpers);

use astlombard::lib::SysUtils;


our @EXPORT = (
    qw/ConstructURLAstlombard/,
    @Engine::Helpers::EXPORT
);

sub ConstructURLAstlombard {
    my (%attrs) = @_;

    my $url = '';
    my $absolute = 0;
    my $anchor = undef;

    if (%attrs) {
        if (defined $attrs{'_url'}) {
            $url = $attrs{'_url'};
            delete $attrs{'_url'};
        }
        if (defined $attrs{'_absolute'}) {
            $absolute = 1;
            delete $attrs{'_absolute'};
        }
        if (defined $attrs{'_anchor'}) {
            $anchor = $attrs{'_anchor'};
            delete $attrs{'_anchor'};
        }
        $url = ConstructUrlStringAstlombard($url, \%attrs, 1, $anchor);
    }
    $url = $App->Request->http_scheme.'://'.$App->Request->host.$url if ($absolute);
    return $url;
}

sub NavBar {
    return Engine::Helpers::NavBar(url_sub => \&ConstructURLAstlombard, @_);
}

1;

__END__;
