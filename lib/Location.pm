package astlombard::lib::Location;

use strict;
use utf8;

use Engine::GlobalVars;

use astlombard::lib::SysUtils;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;
    my $self = { @_ };
    bless $self, $class;
    $self->Load();
    return $self;
}

sub DESTROY {
    my $self = shift;
    $self->Save();
}

# ================================================================================

sub GetCity {
    my $self = shift;

    return $self->City || $self->DefineCity;
}

sub DefineCity {
    my $self = shift;

    return $self->GeoCity if ($self->GeoCity);

    if (IsRobot()) {
        return undef;
    }

    #my @ip = split /\s*,\s*/, $App->Request->headers_in->{'X-Forwarded-For'};
    return undef;
}

sub SaveCity {
    my $self = shift;
    my ($city) = @_;
    return $self->City($city);
}

# ================================================================================

sub City {
    my $self = shift;
    if (@_) {
        $self->{'__CITY'} = shift;
    }
    return $self->{'__CITY'};
}

sub GeoCity {
    my $self = shift;
    if (@_) {
        $self->{'__GEO_CITY'} = shift;
    }
    return $self->{'__GEO_CITY'};
}

# ================================================================================

sub Load {
    my $self = shift;
    print "[ Location ] Load\n";
    $self->City($App->Session->Var('Location.City'));
    $self->GeoCity($App->Session->Var('Location.GeoCity'));
}

sub Save {
    my $self = shift;
    print "[ Location ] Save\n";
    $App->Session->Var('Location.City', $self->City);
    $App->Session->Var('Location.GeoCity', $self->GeoCity);
}

1;
