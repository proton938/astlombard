package astlombard::lib::SysUtils;

use utf8;
use strict;

use Engine::GlobalVars;
use Engine::Search;

use Engine::SysUtils;
use base qw(Engine::SysUtils);

use URI::Escape qw(uri_escape_utf8);
use Encode qw(encode_utf8);

our @EXPORT = (
    qw/
        ConstructUrlStringAstlombard
        GetNodeByURLPathAstlombard
    /,
    @Engine::SysUtils::EXPORT
);

sub ConstructUrlStringAstlombard {
    my ($base_url, $attrs, $no_cookie_check, $anchor) = @_;

    if (exists $attrs->{'lombard_id'}) {
        delete $attrs->{'node_id'};

        my $cache = $App->{'_URLS_CACHE'} || {};
        if (!exists $cache->{'lombard_cat_id'}) {
            $cache->{'lombard_cat_id'} = $DB->GetRow(qq{SELECT `id` FROM `T_##_CAT_NODES` WHERE `class_name` = 'lombard_cat' LIMIT 1});
        }
        if (!exists $cache->{'lombard_slink_' . $attrs->{'lombard_id'}}) {
            $cache->{'lombard_slink_' . $attrs->{'lombard_id'}} = $DB->GetRow(qq{SELECT `slink` FROM `T_##_LOMBARDS` WHERE `lombard_id` = :lombard_id LIMIT 1}, $attrs);
        }
        my $node_id = $cache->{'lombard_cat_id'};
        my $slink = $cache->{'lombard_slink_' . $attrs->{'lombard_id'}};

        if ($node_id and $slink) {
            delete $attrs->{'lombard_id'};

            my $url = ConstructUrlString($base_url, { node_id => $node_id }) . $slink . '/';

            my $params_string = ConstructUrlParams($attrs);

            $url .= '?' . $params_string if ($params_string);
            $url .= '#' . uri_escape_utf8($anchor) if (defined $anchor);

            return $url;
        } else {
            print "[ WARNING ] [ SysUtils::ConstructUrlStringAstlombard ] Slink or node_id are undefined\n";
            return ConstructUrlString($base_url, $attrs, $no_cookie_check, $anchor);
        }
    } else {
        return ConstructUrlString($base_url, $attrs, $no_cookie_check, $anchor);
    }
}

sub GetNodeByURLPathAstlombard {
    my ($parent_id, $path_ref) = @_;
    $parent_id ||= 0;

    if (my $node = $DB->CAT_GET_OBJECT(object_id => $parent_id)->fetchrow_hashref()) {
        my $prev_node = $node;
        my $params = {};
        my $standard_mode = 1;

        while (my $slink = shift @$path_ref) {
            $node = $DB->ExecSQL(qq{
                SELECT `lft`, `rgt`, `id` AS `object_id`, `class_name`, (`parent_id` = :object_id) as `PARENT_TEST`
                FROM `T_##_CAT_NODES`
                WHERE `slink` = :slink AND `lft` > :lft AND `rgt` < :rgt
                ORDER BY `PARENT_TEST` DESC, `lft`
            }, {
               %$prev_node, slink => $slink
            })->fetchrow_hashref() if ($standard_mode);

            if (!$node) {
                if ($prev_node->{'class_name'} eq 'lombard_cat') {
                    $standard_mode = 0;

                    if (my $lombard_id = $DB->GetRow(qq{SELECT `lombard_id` FROM `T_##_LOMBARDS` WHERE `slink` = :slink LIMIT 1}, { slink => $slink })) {
                        $params->{'lombard_id'} = $lombard_id;
                        $prev_node->{'class_name'} = 'lombard';
                        $prev_node->{'object_id'} = $DB->GetRow(qq{SELECT `id` FROM `T_##_CAT_NODES` WHERE `class_name` = :class_name LIMIT 1}, $prev_node);
                        next;
                    }

                    print "[ WARNING ] [ SysUtils::GetNodeByURLPathAstlombard ] Slink $slink is not found\n";
                    return (0, undef, {});
                } else {
                    return (0, undef, {});
                }
            } else {
                $prev_node = $node;
            }
        }

        return ($prev_node->{'object_id'}, $prev_node->{'class_name'}, $params);
    }
    return (0, undef, {});
}

1;

__END__;
