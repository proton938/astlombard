package astlombard::lib::Utils;

use utf8;
use strict;

use base qw(Exporter);

use Engine::SysUtils;

use Data::Dumper;
$Data::Dumper::Indent = 1;
$Data::Dumper::Sortkeys = 1;

# UTF-8 for Data::Dumper
$Data::Dumper::Useqq = 1;
$Data::Dumper::Useperl = 1;
{
    no warnings 'redefine';
    sub Data::Dumper::qquote {
        my $s = shift;
        return "'$s'";
    }
}
# /UTF-8 for Data::Dumper

our @EXPORT = qw(
    Dumper
    Dump
);

sub Dumper {
    my $dump = Data::Dumper->Dump([@_]);

    # Увеличиваем отступы в начале строки в два раза
    $dump =~ s/^(\s*)/$1$1/gm;

    return $dump;
}

sub Dump {
    print Data::Dumper->Dump([@_]);
}

1;