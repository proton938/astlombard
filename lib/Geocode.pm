package astlombard::lib::Geocode;

use strict;
use utf8;

use Engine::GlobalVars;
use Data::Dumper;

use LWP::UserAgent;
use JSON;

my $SERVICE_URL = 'http://geocode-maps.yandex.ru/1.x/?format=json&geocode=';

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = {};
    bless $self, $class;
    return $self;
}

# ================================================================================

sub Geocode {
    my $self = shift;
    my ($address) = @_;

    print "[ DEBUG ] [ Geocode::Geocode ] $address\n";
    my $response = $self->Browser->get($SERVICE_URL.$address);
    if ($response->is_success()) {
        my $geo = decode_json($response->decoded_content());
        if (my $object = $geo->{'response'}->{'GeoObjectCollection'}->{'featureMember'}->[0]->{'GeoObject'}) {
            my ($lng, $lat) = split /\s+/, $object->{'Point'}->{'pos'};
            my $res = {
                name => $object->{'metaDataProperty'}->{'GeocoderMetaData'}->{'text'},
                type => $object->{'metaDataProperty'}->{'GeocoderMetaData'}->{'precision'},
                longitude => $lng,
                latitude => $lat
            };
            print "[ DEBUG ] [ Geocode::Geocode ] Geocoded successfully\n" . Dumper($res);
            return $res;
        } else {
            print "[ ERROR ] [ Geocode::Geocode ] GeoObject is not found\n" . Dumper($geo);
        }
    } else {
        print "[ ERROR ] [ Geocode::Geocode ] Can't got response\n" . Dumper($response);
    }
    return undef;
}

# ================================================================================

sub Browser {
    my $self = shift;
    if (!exists $self->{'__BROWSER'}) {
        $self->{'__BROWSER'} = LWP::UserAgent->new(@_);
    }
    return $self->{'__BROWSER'};
}

# ================================================================================

sub DESTROY {
    my $self = shift;
    undef $self->{'__BROWSER'};
}

1;
