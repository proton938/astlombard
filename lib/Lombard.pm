package astlombard::lib::Lombard;

use strict;
use utf8;

use Engine::GlobalVars;

use URI::Escape qw(uri_escape_utf8);
use Digest::SHA qw/sha1_hex/;
use Encode qw/encode decode/;

use SOAP::Lite;
#use SOAP::Lite +trace => 'debug';
use LWP::UserAgent;
use IO::Socket::SSL;
IO::Socket::SSL::set_defaults(SSL_verify_mode => 0);

use JSON;
use astlombard::lib::Utils;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = {};
    bless $self, $class;

    return $self;
}
# -------------------------------------------
sub SOAP::Transport::HTTP::Client::get_basic_credentials {
    my $user = "admin";
    my $password = "";
    return $user => $password;
}
# -------------------------------------------
sub soapGetBad {
    my $soap = shift;
    my $res = shift;
    if( ref( $res ) ) {
        my $err = $res->faultstring;
        print "SOAP FAULT: $err\n";
    }
    else {
        my $err = $soap->transport->status;
        print "TRANSPORT ERROR: $err\n";
    }
    return new SOAP::SOM;
}
# -------------------------------------------
sub Register {
    my $self = shift;
    my (%params) = @_;

    $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

    my $service_url = 'https://37.230.155.133:9443/LombKz/ws/service_lombards.1cws';

    my $soap = SOAP::Lite->new( proxy => $service_url);
    $soap->uri('DataForSiteSoap');
    $soap->default_ns('astana_lombard/service_lombards');
    my $type = 1;
    if (defined($params{'code'} && defined($params{'password'}))) {
        $type = 2;
        $params{'iin'} = $App->Session->Var('register')->{'iin'};
        $params{'phone'} = $App->Session->Var('register')->{'phone'};
        print  $App->Session->Dump();
    } 
    print "===================================\n";
    print Dumper(%params);
    my $som = $soap->call('Register',
        SOAP::Data->name('IIN')->value($params{'iin'})->type(''),
        SOAP::Data->name('Phone')->value($params{'phone'})->type(''),
        SOAP::Data->name('Code')->value($params{'code'})->type(''),
        SOAP::Data->name('Password')->value($params{'password'})->type(''),
        SOAP::Data->name('Type')->value($type)->type('')
    );
    die $som->faultstring if ($som->fault);
    print "Result: ",$som->result,"\n";
    if ($som->result == 0 && $type == 2) {
        $App->Session->UndefVar('register');
    }
    return {ok => $som->result, type => $type};
}
# -------------------------------------------
sub Authenticate {
    my $self = shift;
    my (%params) = @_;

    $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

    my $service_url = 'https://37.230.155.133:9443/LombKz/ws/service_lombards.1cws';

    my $soap = SOAP::Lite->new( proxy => $service_url);
    $soap->uri('DataForSiteSoap');
    $soap->default_ns('astana_lombard/service_lombards');
    print "===================================\n";
    print Dumper(%params);

    my $som = $soap->call('Authenticate',
        SOAP::Data->name('Phone')->value($params{'phone'})->type(''),
        SOAP::Data->name('Password')->value($params{'password'})->type('')
    );
    die $som->faultstring if ($som->fault);
    print "Result: ",$som->result,"\n";
    return {ok => $som->result, type => '1'};

}
sub GetTickets {
    my $self = shift;
    my (%params) = @_;

    $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

    my $service_url = 'https://37.230.155.133:9443/LombKz/ws/service_lombards.1cws';

    my $soap = SOAP::Lite->new( proxy => $service_url);
    $soap->uri('DataForSiteSoap');
    $soap->default_ns('astana_lombard/service_lombards');
    print "===================================\n";
    print Dumper(%params);

    my $som = $soap->call('GetTickets',
        SOAP::Data->name('Phone')->value($params{'phone'})->type(''),
        SOAP::Data->name('IIN')->value($params{'iin'})->type('')
    );
    die $som->faultstring if ($som->fault);
    print "Result: ",$som->result,"\n";
    return $som->result;
}
1;

__END__

#use SOAP::Lite;
use SOAP::Lite +trace => 'debug';
use LWP::UserAgent;
use IO::Socket::SSL;
IO::Socket::SSL::set_defaults(SSL_verify_mode => 0);

use Unicode::MapUTF8 qw(to_utf8 from_utf8 utf8_supported_charset);
use MIME::Base64;
use Data::Dumper;

binmode(STDOUT,':utf8');

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

my $user = "admin";
my $password = "";


sub SOAP::Transport::HTTP::Client::get_basic_credentials {
    return $user => $password;
}

sub soapGetBad {
    my $soap = shift;
    my $res = shift;
    if( ref( $res ) ) {
        my $err = $res->faultstring;
        print "SOAP FAULT: $err\n";
    }
    else {
        my $err = $soap->transport->status;
        print "TRANSPORT ERROR: $err\n";
    }
    return new SOAP::SOM;
}
my $service_url = 'https://37.230.155.133:9443/LombKz/ws/service_lombards.1cws';

my $soap = SOAP::Lite->new( proxy => $service_url);
$soap->uri('DataForSiteSoap');
$soap->default_ns('astana_lombard/service_lombards');

my $som = $soap->call('GetTickets',
   SOAP::Data->name('IIN')->value('222222222222')->type(''),
   SOAP::Data->name('Phone')->value('79304570983')->type('')
);
die $som->faultstring if ($som->fault);

print $som->result;
