package astlombard::dblib::Lombards;

use strict;
use utf8;
no strict 'refs';

use Engine::GlobalVars;
use Engine::SysUtils;

use Data::Dumper;

use constant {
    REGIONS_TABLE => 'T_##_REGIONS',
    REGIONS_KEY => 'region_id',
    CITIES_TABLE => 'T_##_CITIES',
    CITIES_KEY => 'city_id',
    LOMBARDS_TABLE => 'T_##_LOMBARDS',
    LOMBARDS_KEY => 'lombard_id',
    LOMBARDS_RELS => [qw/services stocks hours timestamps/]
};

sub import {
    my $from = __PACKAGE__;
    my $to = caller();
    while (my ($k, $v) = each(%{"$from\::"})) {
        next if (substr($k, -1) eq ":" or $k eq 'BEGIN');
        *{"$to\::$k"} = $v;
    }
}

# ================================================================================

sub LOMBARDS_CREATE_REGION {
	my $self = shift;
    my (%params) = @_;

    my $region_id = $self->__create(REGIONS_TABLE, REGIONS_KEY, \%params);
    if (!defined $region_id) {
        return undef;
    }
    return $region_id;
}

sub LOMBARDS_UPDATE_REGION {
	my $self = shift;
	my (%params) = @_;

	my $region_id = $params{'region_id'};
	delete $params{'region_id'};

	if (%params) {
		my $res = $self->__update(REGIONS_TABLE, { region_id => $region_id, values => \%params });
		if (!defined $res) {
            return undef;
        }
	}
	return $region_id;
}

sub LOMBARDS_DELETE_REGION {
	my $self = shift;
	my (%params) = @_;

	my $res = $self->__delete(REGIONS_TABLE, { region_id => $params{'region_id'} });
    if (!defined $res) {
        return undef;
    }
    return 1;
}

sub LOMBARDS_GET_REGIONS {
	my $self = shift;
	my (%params) = @_;

	return $self->__select(REGIONS_TABLE, \%params);
}

sub LOMBARDS_GET_REGION {
	return shift->LOMBARDS_GET_REGIONS(@_, limit_rows => 1);
}

# ================================================================================

sub LOMBARDS_CREATE_CITY {
    my $self = shift;
    my (%params) = @_;

    my $city_id = $self->__create(CITIES_TABLE, CITIES_KEY, \%params);
    if (!defined $city_id) {
        return undef;
    }
    return $city_id;
}

sub LOMBARDS_UPDATE_CITY {
    my $self = shift;
    my (%params) = @_;

    my $city_id = $params{'city_id'};
    delete $params{'city_id'};

    if (%params) {
        my $res = $self->__update(CITIES_TABLE, { city_id => $city_id, values => \%params });
        if (!defined $res) {
            return undef;
        }
    }
    return $city_id;
}

sub LOMBARDS_DELETE_CITY {
    my $self = shift;
    my (%params) = @_;

    my $res = $self->__delete(CITIES_TABLE, { city_id => $params{'city_id'} });
    if (!defined $res) {
        return undef;
    }
    return 1;
}

sub LOMBARDS_GET_CITIES {
    my $self = shift;
    my (%params) = @_;

    return $self->__select(CITIES_TABLE, \%params);
}

sub LOMBARDS_GET_CITY {
    return shift->LOMBARDS_GET_CITIES(@_, limit_rows => 1);
}

# ================================================================================

sub LOMBARDS_CREATE_LOMBARD {
    my $self = shift;
    my (%params) = @_;

    my %rels = ();
    foreach my $rel (grep { exists $params{ $_ } } @{ +LOMBARDS_RELS }) {
        $rels{ $rel } = $params{ $rel };
        delete $params{ $rel };
    }

    $self->StartTransaction() if (!$params{'_skip_transaction'});

    my $lombard_id = $self->__create(LOMBARDS_TABLE, LOMBARDS_KEY, \%params);
    if (!defined $lombard_id) {
        $self->Rollback() if (!$params{'_skip_transaction'});
        return undef;
    }

    foreach my $rel (grep { exists $rels{ $_ } } @{ +LOMBARDS_RELS }) {
        my $res = $self->LOMBARDS_UPDATE_LOMBARD_RELS(
            lombard_id => $lombard_id,
            rel => $rel,
            $rel => $rels{ $rel },
            _skip_deleting => 1,
            _skip_transaction => 1
        );
        if (!defined $res) {
            $self->Rollback() if (!$params{'_skip_transaction'});
            return undef;
        }
    }

    $self->Commit() if (!$params{'_skip_transaction'});

    return $lombard_id;
}

sub LOMBARDS_UPDATE_LOMBARD {
    my $self = shift;
    my (%params) = @_;

    my $lombard_id = $params{'lombard_id'};
    delete $params{'lombard_id'};

    my %rels = ();
    foreach my $rel (grep { exists $params{ $_ } } @{ +LOMBARDS_RELS }) {
        $rels{ $rel } = $params{ $rel };
        delete $params{ $rel };
    }

    $self->StartTransaction() if (!$params{'_skip_transaction'});

    if (%params) {
        my $res = $self->__update(LOMBARDS_TABLE, { lombard_id => $lombard_id, values => \%params });
        if (!defined $res) {
            return undef;
        }
    }

    foreach my $rel (grep { exists $rels{ $_ } } @{ +LOMBARDS_RELS }) {
        my $res = $self->LOMBARDS_UPDATE_LOMBARD_RELS(
            lombard_id => $lombard_id,
            rel => $rel,
            $rel => $rels{ $rel },
            _skip_transaction => 1
        );
        if (!defined $res) {
            $self->Rollback() if (!$params{'_skip_transaction'});
            return undef;
        }
    }

    $self->Commit() if (!$params{'_skip_transaction'});

    return $lombard_id;
}

sub LOMBARDS_DELETE_LOMBARD {
    my $self = shift;
    my (%params) = @_;

    my $res = $self->__delete(LOMBARDS_TABLE, { lombard_id => $params{'lombard_id'} });
    if (!defined $res) {
        return undef;
    }
    return 1;
}

sub LOMBARDS_GET_LOMBARDS {
    my $self = shift;
    my (%params) = @_;
 
    my @tables = ({ name => LOMBARDS_TABLE, synonym => 'lombards' });
    my @fields = map { { table => 'lombards', name => $_ } } $self->GetFields(LOMBARDS_TABLE);

    if (defined $params{'is_opened'}) {
        my ($time, $day) = $self->GetArray(qq{SELECT FLOOR(TIME_TO_SEC(CURTIME()) / 60), WEEKDAY(NOW()) + 1});
        my $cond = qq{
            24hours OR (
                EXISTS (SELECT * FROM `T_##_LOMBARDS_TIMESTAMPS` AS `_t` WHERE `_t`.`lombard_id` = lombard_id AND `_t`.`type` = 'work' AND `_t`.`day` = $day AND `_t`.`start_time` <= $time AND `_t`.`finish_time` >= $time)
                AND NOT EXISTS (SELECT * FROM `T_##_LOMBARDS_TIMESTAMPS` AS `_t` WHERE `_t`.`lombard_id` = lombard_id AND `_t`.`type` = 'lunch' AND `_t`.`day` = $day AND `_t`.`start_time` <= $time AND `_t`.`finish_time` >= $time)
            )
        };

        if (!$params{'is_opened'}) {
            $cond = qq{NOT ($cond)};
        }
                
        $params{'condition'} = $self->__push_conds($params{'condition'}, $cond);
    }
    delete $params{'is_opened'};

    if (defined $params{'services_buy'}) {
        $params{'_services_buy'} = $params{'services_buy'};
        my $cond = qq{
            EXISTS (SELECT * FROM `T_##_LOMBARDS_SERVICES` AS `_s` WHERE `_s`.`lombard_id` = lombard_id AND `_s`.`type` = 'buy' AND `_s`.`service_id` IN (:_services_buy))
        };

        $params{'condition'} = $self->__push_conds($params{'condition'}, $cond);
    }
    delete $params{'services_buy'};

    if (defined $params{'services_sale'}) {
        $params{'_services_sale'} = $params{'services_sale'};
        my $cond = qq{
            EXISTS (SELECT * FROM `T_##_LOMBARDS_SERVICES` AS `_s` WHERE `_s`.`lombard_id` = lombard_id AND `_s`.`type` = 'sale' AND `_s`.`service_id` IN (:_services_sale))
        };

        $params{'condition'} = $self->__push_conds($params{'condition'}, $cond);
    }
    delete $params{'services_sale'};

    my ($sql, $args) = $self->ConstructSelectQuery(\%params, \@tables, \@fields);
    my $res = $params{'count_only'} ? $self->GetRow($sql, $args) :  $self->ExecSQL($sql, $args);
    if ($self->Err) {
        return undef;
    }
    return $res;
}

sub LOMBARDS_GET_LOMBARD {
    return shift->LOMBARDS_GET_LOMBARDS(@_, limit_rows => 1);
}

# ================================================================================

sub LOMBARDS_UPDATE_LOMBARD_RELS {
    my $self = shift;
    my (%params) = @_;

    $self->StartTransaction() if (!$params{'_skip_transaction'});

    my $rel = $params{'rel'};
    delete $params{'rel'};
    my $table = LOMBARDS_TABLE . '_' . uc $rel;

    my $res = $self->__update_rels($table, LOMBARDS_KEY, $rel, \%params);

    if (!defined $res) {
        $self->Rollback() if (!$params{'_skip_transaction'});
        return undef;
    }

    $self->Commit() if (!$params{'_skip_transaction'});

    return $res;
}

sub LOMBARDS_GET_LOMBARD_RELS {
    my $self = shift;
    my (%params) = @_;

    my $rel = $params{'rel'};
    delete $params{'rel'};
    my $table = LOMBARDS_TABLE . '_' . uc $rel;

    return $self->__select($table, \%params);
}

# ================================================================================

sub LOMBARDS_UPDATE_LOMBARD_SERVICES {
    my $self = shift;
    return $self->LOMBARDS_UPDATE_LOMBARD_RELS(@_, rel => 'services');
}
sub LOMBARDS_GET_LOMBARD_SERVICES {
    my $self = shift;
    return $self->LOMBARDS_GET_LOMBARD_RELS(@_, rel => 'services');
}

sub LOMBARDS_UPDATE_LOMBARD_STOCKS {
    my $self = shift;
    return $self->LOMBARDS_UPDATE_LOMBARD_RELS(@_, rel => 'stocks');
}
sub LOMBARDS_GET_LOMBARD_STOCKS {
    my $self = shift;
    return $self->LOMBARDS_GET_LOMBARD_RELS(@_, rel => 'stocks');
}

sub LOMBARDS_UPDATE_LOMBARD_HOURS {
    my $self = shift;
    return $self->LOMBARDS_UPDATE_LOMBARD_RELS(@_, rel => 'hours');
}
sub LOMBARDS_GET_LOMBARD_HOURS {
    my $self = shift;
    return $self->LOMBARDS_GET_LOMBARD_RELS(@_, rel => 'hours');
}

sub LOMBARDS_UPDATE_LOMBARD_TIMESTAMPS {
    my $self = shift;
    return $self->LOMBARDS_UPDATE_LOMBARD_RELS(@_, rel => 'timestamps');
}
sub LOMBARDS_GET_LOMBARD_TIMESTAMPS {
    my $self = shift;
    return $self->LOMBARDS_GET_LOMBARD_RELS(@_, rel => 'timestamps');
}

# ================================================================================

sub LOMBARDS_UPDATE_STOCK_LOMBARDS {
    my $self = shift;
    my (%params) = @_;

    my $table = LOMBARDS_TABLE . '_STOCKS';

    my $res = $self->__update_rels($table, 'stock_id', 'lombards', \%params);

    if (!defined $res) {
        $self->Rollback() if (!$params{'_skip_transaction'});
        return undef;
    }

    $self->Commit() if (!$params{'_skip_transaction'});

    return $res;
}
sub LOMBARDS_GET_STOCK_LOMBARDS {
    my $self = shift;
    my (%params) = @_;

    my $table = LOMBARDS_TABLE . '_STOCKS';

    return $self->__select($table, \%params);
}

# ================================================================================

sub __create {
    my $self = shift;
    my ($table_name, $id_key, $values) = @_;

    my @tables = ({ name => $table_name, synonym => 'table' });
    my @fields = map { { table => 'table', name => $_ } } $self->GetFields($table_name);

    my ($sql, $args) = $self->ConstructInsertQuery($values, \@tables, \@fields);
    $self->ExecSQL($sql, $args);
    if ($self->Err) {
        return undef;
    }
    if (defined $id_key and defined $values->{ $id_key }) {
        return $values->{ $id_key };
    } elsif (exists $values->{'values'}) {
        return 1;
    }
    return $self->LastId();
}

sub __update {
	my $self = shift;
    my ($table_name, $values) = @_;

	my @tables = ({ name => $table_name, synonym => 'table' });
    my @fields = map { { table => 'table', name => $_ } } $self->GetFields($table_name);

    my ($sql, $args) = $self->ConstructUpdateQuery($values, \@tables, \@fields);
    $self->ExecSQL($sql, $args);
    if ($self->Err) {
        return undef;
    }
    return 1;
}

sub __delete {
	my $self = shift;
    my ($table_name, $values) = @_;

    my @tables = ({ name => $table_name, synonym => 'table' });
    my @fields = map { { table => 'table', name => $_ } } $self->GetFields($table_name);

    my ($sql, $args) = $self->ConstructDeleteQuery($values, \@tables, \@fields);
    $self->ExecSQL($sql, $args);
    if ($self->Err) {
        return undef;
    }
    return 1;
}

sub __select {
	my $self = shift;
    my ($table_name, $values) = @_;

    my @tables = ({ name => $table_name, synonym => 'table' });
    my @fields = map { { table => 'table', name => $_ } } $self->GetFields($table_name);

    my ($sql, $args) = $self->ConstructSelectQuery($values, \@tables, \@fields);
    my $res = $values->{'count_only'} ? $self->GetRow($sql, $args) :  $self->ExecSQL($sql, $args);
    if ($self->Err) {
        return undef;
    }
    return $res;
}

sub __update_rels {
    my $self = shift;
    my ($table_name, $id_key, $rel_key, $values) = @_;

    if (!$values->{'_skip_deleting'}) {
        my $res = $self->__delete($table_name, { $id_key => $values->{ $id_key } });
        if (!defined $res) {
            return undef;
        }
    }

    my @values = map { { %$_, $id_key => $values->{ $id_key } } } @{ $values->{ $rel_key } };
    if (@values) {
        my $res = $self->__create($table_name, undef, { values => \@values });
        if (!defined $res) {
            return undef;
        }
    }
    return 1;
}

sub __push_conds {
    my $self = shift;
    my ($condition, $cond) = @_;

    $condition ||= [];
    if (ref $condition eq 'ARRAY') {
        push @$condition, $cond;
    } else {
        $condition = [ $condition, $cond ];
    }

    return $condition;
}

1;
