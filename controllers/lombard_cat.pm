package astlombard::controllers::lombard_cat;

use utf8;
use strict;

use base qw(astlombard::controllers::__lombard__);

use Engine::GlobalVars;
use astlombard::lib::Helpers;

use Data::Dumper;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(@_);
    bless $self, $class;
    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $obj->{'lombards'} = $self->GetLombards();
        $obj->{'filter'} = $self->GetLombardFilter();

        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $object_id, 
        attrs => ['header', 'content']
    )->fetchrow_hashref()) {

        return $obj;
    }
    return undef;
}

sub GetLombardFilter {
    my $self = shift;

    my $params = $self->__ExtractParams();

    my $cities = $self->GetLombardCities();

    my $services_buy = $self->GetServices('buy');
    my $services_sale = $self->GetServices('sale');

    my @location_fields = (
        { name => 'city', type => 'select', publ_name => 'Выберите город', variants => $cities }
    );

    my @hours_fields = (
        { name => 'is_24hours', type => 'flag', publ_name => 'Круглосуточно', attrs => { ($params->{'is_24hours'}) ? (checked => 'checked') : () } },
        { name => 'is_opened', type => 'flag', publ_name => 'Работает сейчас', attrs => { ($params->{'is_opened'}) ? (checked => 'checked') : () } }
    );

    my @services_fields = (
        { name => 'buy', type => 'checkbox', publ_name => 'Выберите вид принимаемого имущества:', variants => $services_buy },
        { name => 'sale', type => 'checkbox', publ_name => 'Выберите вид распродажи:', variants => $services_sale }
    );

    return {
        action => ConstructURLAstlombard(node_id => $App->Param('node_id')),
        blocks => [
            {
                name => 'base',
                subblocks => [
                    {
                        name => 'lombard-location',
                        fields => \@location_fields
                    },
                    {
                        name => 'lombard-flags',
                        publ_name => 'График работы:',
                        fields => \@hours_fields
                    },
                    {
                        name => 'lombard-services',
                        fields => \@services_fields
                    },
                    {
                        name => 'submit',
                        submit => {
                            submit => 'Показать',
                            reset => 'Сбросить'
                        },
                        reset => {
                            href => ConstructURLAstlombard(node_id => $App->Param('node_id'))
                        }
                    }
                ]
            }
        ]
    }
}

sub GetLombardCities {
    my $self = shift;

    my @places = ();
    my $values = $DB->LOMBARDS_GET_CITIES()->fetchall_arrayref({});
    my $params = $self->__ExtractParams();

    foreach my $value (@$values) {
        push @places, {
            name => $value->{'city_name'},
            value => $value->{'city_id'},
            attrs => { ($value->{'city_id'} eq $params->{'city_id'}) ? (selected => 'selected') : () }
        }
    }

    return \@places;
}

sub GetServices {
    my $self = shift;
    my ($type) = @_;

    my @services = ();
    my $params = $self->__ExtractParams();
    my %service = map { $_ => 1 } @{ $params->{'services_' . $type} || [] };

    my $groups = $DB->CAT_MEGAGET(
        class_name => 'prod_group',
        attrs => ['publication'],
        condition => qq{publication}
    )->fetchall_arrayref({});

    foreach my $group (@$groups) {
        push @services, {
            name => $group->{'object_name'},
            value => $group->{'object_id'},
            attrs => { ($service{ $group->{'object_id'} }) ? (checked => 'checked') : () }
        }
    }

    return \@services;
}

1;
