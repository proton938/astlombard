package astlombard::controllers::prod_page;

use utf8;
use strict;

use base qw(astlombard::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;

use Data::Dumper;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $obj->{'info'} = $self->GetProductInfo($obj);
        $obj->{'similar_products'} = $self->GetSimilarProducts($obj);
        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $object_id, 
        attrs => ['header', 'vendor_code', 'lombard', 'price', 'picture', 'content']
    )->fetchrow_hashref()) {

        return $obj;
    }
    return undef;
}

sub GetSimilarProducts {
    my $self = shift;
    my ($prod) = @_;

    my $prod_group = $self->GetProductGroup($prod->{'parent_id'});

    my $similar_products = undef;
    my $similar_products_count = undef;
    ($similar_products, $similar_products_count) = $self->GetProducts(
        $prod_group->{'object_id'},
        order_by => [['RAND()']]
    );

    return $similar_products;
}

sub GetProductInfo {
    my $self = shift;
    my ($prod) = @_;

    my $prod_type = $self->GetProductType($prod->{'parent_id'});

    my $attrs = $self->__GetAttrs($prod->{'class_name'}, 'full');
    my @attrs = ();
    foreach my $attr (@$attrs) {
        my $value = $self->__GetAttrValue($prod_type, $prod, $attr);

        push @attrs, {
            attr_name => $attr->{'attr_name'},
            publ_name => $attr->{'publ_name'},
            publ_comment => $attr->{'publ_comment'},
            value => $value
        } if ($value ne '');
    }

    my @photos = ();
    my $photos = $DB->CAT_GET_OBJECT_GALLERY(
        object_id => $prod->{'object_id'},
        publication => 1
    )->fetchall_arrayref({});
    foreach my $photo (@$photos) {
        push @photos, {
            img => GetImage(file => $photo, alt => $photo->{'photo_name'}),
            thumb => GetImage(file => $photo, src => 'small_src', alt => $photo->{'photo_name'}),
            photo_name => $photo->{'photo_name'},
            photo_comment => $photo->{'description'}
        };
    }

    my $picture_img = undef;
    if (!@photos) {
        $picture_img = GetImage(file_id => $prod->{'picture'}, alt => $prod->{'header'}) if ($prod->{'picture'} > 0);
    }

    my $prod_group = $DB->CAT_GET_OBJECT(object_id => $prod_type->{'parent_id'})->fetchrow_hashref();
    my $booking_offer = $DB->CAT_GET_OBJECT_ATTR(object_id => $prod_group->{'parent_id'}, attr_name => 'booking_offer');

    my $info = {
        header => $prod->{'header'},
        object_name => $prod->{'object_name'},
        vendor_code => $prod->{'vendor_code'},
        price => $prod->{'price'},
        content => $prod->{'content'},
        attrs => \@attrs,
        photos => \@photos,
        picture_img => $picture_img,
        booking_offer => $booking_offer
    };

    return $info;
}

sub __GetAttrs {
    my $self = shift;
    my ($class, $group) = @_;

    my $suff = '_'.$class;

    if (defined $group) {
        $suff .= '_'.$group;
    }

    if (!exists $self->{'__ATTRS'.$suff}) {
        $self->{'__ATTRS'.$suff} = $DB->CAT_GET_CLASS_ATTRS(
            class_name => $class,
            attr_group_name => $group
        )->fetchall_arrayref({});
    }

    return $self->{'__ATTRS'.$suff};
}

sub __GetAttrValue {
    my $self = shift;
    my ($product_type, $product, $attr) = @_;

    if (!$self->__AttrIsArray($product_type, $attr)) {
        my $value = $product->{$attr->{'attr_name'}} // $DB->CAT_GET_OBJECT_ATTR(object_id => $product->{'object_id'}, attr_name => $attr->{'attr_name'});

        if ($self->__AttrIsPointer($product_type, $attr)) {
            $value = ($value > 0) ? $DB->GetRow(qq{SELECT `name` FROM `T_##_CAT_NODES` WHERE `id` = :value LIMIT 1}, { value => $value }) : '';
        } elsif ($self->__AttrIsBoolean($product_type, $attr)) {
            $value = $value ? 'Да' : 'Нет';
        }

        return $value;
    } else {
        my $value = $product->{$attr->{'attr_name'}} // [ $DB->CAT_GET_OBJECT_ATTR(object_id => $product->{'object_id'}, attr_name => $attr->{'attr_name'}) ];

        if (@$value) {
            if ($self->__AttrIsPointer($product_type, $attr)) {
                my $rel = $DB->ExecSQL(qq{SELECT `id`, `name` FROM `T_##_CAT_NODES` WHERE `id` IN (:value)}, { value => $value })->fetchall_hashref('id');
                $value = [ map { (exists $rel->{$_}) ? $rel->{$_}->{'name'} : () } @$value ];
            }

            return join ', ', @$value;
        }
    }

    return '';
}

sub __AttrIsRange {
    my $self = shift;
    my ($obj, $attr) = @_;

    return ($attr->{'field_type'} eq 'int' or $attr->{'field_type'} eq 'double');
}

sub __AttrIsArray {
    my $self = shift;
    my ($obj, $attr) = @_;

    return ($attr->{'field_type'} eq 'array' or $attr->{'field_type'} eq 'set');
}

sub __AttrIsPointer {
    my $self = shift;
    my ($obj, $attr) = @_;

    return ($attr->{'field_type'} eq 'array' or $attr->{'field_type'} eq 'pointer');
}

sub __AttrIsBoolean {
    my $self = shift;
    my ($obj, $attr) = @_;

    return ($attr->{'field_type'} eq 'bool');
}

1;
