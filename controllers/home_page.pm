package astlombard::controllers::home_page;

use utf8;
use strict;

use base qw(astlombard::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Data::Dumper;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;  
    
    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $obj->{'slides'} = $self->GetHomeSlides();
        $obj->{'advantages'} = $self->GetAdvantages();
        $obj->{'stocks'} = $self->GetStocks(limit_rows => $App->Settings->GetParamValue('PerPage.HomePageStocks'));
        $obj->{'news'} = $self->GetNews(limit_rows => $App->Settings->GetParamValue('PerPage.HomePageNews'));
        $obj->{'cat_groups'} = {
            jewellery => $self->GetCatalogGroup('jewellery'),
            appliance => $self->GetCatalogGroup('appliance'),
            cars => $self->GetCatalogGroup('cars')
        };
        ($obj->{'jewellery'}, $obj->{'jewellery_count'}) = $self->GetProducts(
            parent_id => $obj->{'cat_groups'}->{'jewellery'}->{'object_id'},
            order_by => [['RAND()']],
            limit_rows => $App->Settings->GetParamValue('PerPage.HomePageProducts')
        );
        ($obj->{'appliance'}, $obj->{'appliance_count'}) = $self->GetProducts(
            parent_id => $obj->{'cat_groups'}->{'appliance'}->{'object_id'},
            order_by => [['RAND()']],
            limit_rows => $App->Settings->GetParamValue('PerPage.HomePageProducts')
        );
        
        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $object_id, 
        attrs => ['content']
    )->fetchrow_hashref()) {
        
        return $obj;
    }
    return undef;
}

sub GetHomeSlides {
    my $self = shift;

    if (my $page = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
        class_name => 'home_page',
        parent_id => $App->Param('i_c')
    )->fetchrow_hashref()) {

        if (my $cat = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
            class_name => 'slide_cat',
            parent_id => $page->{'object_id'}
        )->fetchrow_hashref()) {

            my $slides = $DB->CAT_MEGAGET(
                class_name => 'slide',
                parent_id => $cat->{'object_id'},
                attrs => ['picture', 'annotation', 'publication'],
                condition => qq{ publication },
                use_publ_pos => 1
            )->fetchall_arrayref({});

            foreach my $slide (@$slides) {
                $slide->{'picture_img'} = GetImage(file_id => $slide->{'picture'}, alt => $slide->{'object_name'}) if ($slide->{'picture'} > 0);
            }

            return $slides;
        }        
    }
    return [];
}

1;
