package astlombard::controllers::stock;

use utf8;
use strict;

use base qw(astlombard::controllers::__site__);
use base qw(astlombard::controllers::__lombard__);

use Engine::GlobalVars;
use Engine::Helpers;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $obj->{'lombards'} = $self->GetLombards();

        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id => $object_id, attrs => ['picture', 'content'])->fetchrow_hashref()) {
        $obj->{'picture_img'} = GetImage(
            file_id => $obj->{'picture'},
            alt => $obj->{'object_name'}
        ) if ($obj->{'picture'} > 0);

        return $obj;
    }
    return undef;
}

1;
