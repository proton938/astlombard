package astlombard::controllers::prod_group;

use utf8;
use strict;

use base qw(astlombard::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::Price;

use Data::Dumper;

use constant PRICE_RANGES => [
    { 'from' => 0, 'to' => 10000 },
    { 'from' => 10000, 'to' => 50000 },
    { 'from' => 50000, 'to' => 100000 },
    { 'from' => 100000, 'to' => 500000 }
];

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $obj->{'stocks'} = $self->GetStocks(limit_rows => $App->Settings->GetParamValue('PerPage.CatalogStocks'));
        $obj->{'filter'} = $self->GetProductsFilter();
        ($obj->{'products'}, $obj->{'navbar'}) = $self->GetProductItems($obj->{'object_id'});

        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $object_id, 
        attrs => ['header']
    )->fetchrow_hashref()) {

        return $obj;
    }
    return undef;
}

sub GetProductsFilter {
    my $self = shift;

    my $prod_types = $self->GetProdTypes();
    my $price_ranges = $self->GetPriceRanges();

    my @products_fields = (
        { name => 'prod_type', type => 'select', publ_name => 'Выберите тип изделия', variants => $prod_types },
        { name => 'price', type => 'checkbox', publ_name => 'Выберите стоимость:', variants => $price_ranges }
    );

    return {
        action => ConstructURL(node_id => $App->Param('node_id')),
        blocks => [
            {
                name => 'base',
                subblocks => [
                    {
                        name => 'prod_types',
                        fields => \@products_fields
                    },
                    {
                        name => 'submit',
                        submit => {
                            submit => 'Показать',
                            reset => 'Сбросить'
                        },
                        reset => {
                            href => ConstructURL(node_id => $App->Param('node_id'))
                        }
                    }
                ]
            }
        ]
    }
}

sub GetProductItems {
    my $self = shift;
    my ($parent_id) = @_;

    my $page = $App->Param('page') || 1;
    my $per_page = $App->Settings->GetParamValue('PerPage.CatalogProducts') || 6;
    my $query = $self->__GetQuery();

    my ($items, $items_count) = $self->GetProducts(
        parent_id => $parent_id,
        %$query,
        on_page => $per_page,
        page_num => $page
    );
    my $navbar = NavBar(
        count => $items_count,
        page => $page,
        per_page => $per_page,
        no_first => 1,
        down => '<',
        up => '>',
        url_params => { node_id => $parent_id, prod_type => $App->Param('prod_type'), price => [ $App->Param('price') ] }
    );

    return ($items, $navbar);
}

sub GetProdTypes {
    my $self = shift;

    my @groups = ();
    my $values = $self->GetProductsTypes($App->Param('node_id'));
    my $params = $self->__ExtractParams();

    foreach my $value (@$values) {
        push @groups, {
            name => $value->{'filter_header'},
            value => $value->{'object_id'},
            attrs => { ($value->{'object_id'} eq $params->{'type_id'}) ? (selected => 'selected') : () }
        };
    }

    return \@groups;
}

sub GetPriceRanges {
    my $self = shift;

    my @ranges = ();
    my $params = $self->__ExtractParams();
    my %price = map { $_ => 1 } @{ $params->{'price'} || [] };

    foreach my $range (@{ +PRICE_RANGES }) {
        my $value = $range->{'from'} . '-' . $range->{'to'};
        my $from = Engine::Helpers::Price->new(value => $range->{'from'}, space => 1)->html();
        my $to = Engine::Helpers::Price->new(value => $range->{'to'}, space => 1, sign => '₸')->html();

        push @ranges, {
            name => qq{от $from до $to},
            value => $value,
            attrs => { ($price{$value}) ? (checked => 'checked') : () }
        };
    }

    return \@ranges;
}

sub __ExtractParams {
    my $self = shift;

    my $params = {};
    if ($App->Param('prod_type')) {
        $params->{'type_id'} = $App->Param('prod_type');
    }
    if ($App->Param('price')) {
        $params->{'price'} = [ $App->Param('price') ];
    }
    
    return $params;
}

sub __GetQuery {
    my $self = shift;

    my $params = $self->__ExtractParams();
    my %query = ();
    my @attrs = ();

    if ($params->{'type_id'}) {
        $query{'parent_id'} = $params->{'type_id'};
    }

    if ($params->{'price'}) {
        my @cond = ();
        my $i = 0;
        foreach my $value (@{ $params->{'price'} }) {
            my ($from, $to) = split /-/, $value;
            my $key_from = '_price_from_' . $i;
            my $key_to = '_price_to_' . $i;

            push @cond, qq{(price >= :$key_from AND price <= :$key_to)};
            push @attrs, 'price';
            $query{ $key_from } = $from;
            $query{ $key_to } = $to;

            $i ++;
        }
        $query{'condition'} = join qq{ OR }, @cond;
    }

    $query{'extra_attrs'} = \@attrs if (@attrs);

    return \%query;
}

1;
