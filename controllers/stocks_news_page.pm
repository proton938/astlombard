package astlombard::controllers::stocks_news_page;

use utf8;
use strict;

use base qw(astlombard::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Data::Dumper;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $obj->{'stocks'} = $self->GetStocks();

        my $page = $App->Param('page') || 1;
        my $per_page = $App->Settings->GetParamValue('PerPage.StocksNewsPageNews') || 3;

        $obj->{'news'} = $self->GetNews(
            on_page => $per_page,
            page_num => $page
        );

        $obj->{'navbar'} = NavBar(
            count => $self->GetNewsCount(),
            page => $page,
            per_page => $per_page,
            no_first => 1,
            down => '<',
            up => '>',
            url_params => { node_id => $obj->{'object_id'} }
        );

        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $object_id, 
        attrs => ['header']
    )->fetchrow_hashref()) {

        return $obj;
    }
    return undef;
}

1;
