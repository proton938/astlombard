package astlombard::controllers::lombard;

use utf8;
use strict;

use base qw(astlombard::controllers::__lombard__);

use Engine::GlobalVars;
use astlombard::lib::Helpers;

use Storable qw(dclone);

use Data::Dumper;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(@_);
    bless $self, $class;
    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    if (my $obj = $self->GetObject($App->Param('node_id'))) {
        $obj->{'lombard'} = $self->GetLombard($App->Param('lombard_id'));
        $obj->{'lombards_href'} = $self->GetLombardsHref();

        $self->AddData({ object => $obj });
    }
}

sub GetObject {
    my $self = shift;
    my ($object_id) = @_;

    if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $object_id, 
        attrs => ['content']
    )->fetchrow_hashref()) {
        return $obj;
    }
    return undef;
}

sub GetLombard {
    my $self = shift;
    my ($lombard_id) = @_;

    if (my $lombard = $DB->LOMBARDS_GET_LOMBARD(lombard_id => $lombard_id)->fetchrow_hashref()) {
        $lombard->{'hours'} = $self->GetLombardHours(lombard_id => $lombard_id);
        $lombard->{'services'} = $self->GetLombardServices(lombard_id => $lombard_id);
        $lombard->{'stocks'} = $self->GetLombardStocks(lombard_id => $lombard_id);

        $lombard->{'region'} = $DB->LOMBARDS_GET_REGION(region_id => $lombard->{'region_id'})->fetchrow_hashref();
        $lombard->{'city'} = $DB->LOMBARDS_GET_CITY(city_id => $lombard->{'city_id'})->fetchrow_hashref();

        return $lombard;
    }

    return undef;
}

sub GetLombardsHref {
    my $self = shift;

    if (my $lombard_cat = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
        class_name => 'lombard_cat',
        parent_id => $App->Param('i_c')
    )->fetchrow_hashref()) {

        return ConstructURLAstlombard(node_id => $lombard_cat->{'object_id'});
    }

    return undef;
}

sub GetBreadcrumbs {
    my $self = shift;

    my $items = [];
    $items = dclone($self->Path);

    pop @$items;

    for (my $i = 0; $i < @$items; $i ++) {
        my $item = $items->[$i];

        if ($item->{'slink'} ne '') {
            $item->{'href'} = ConstructURLAstlombard(node_id => $item->{'object_id'});
        }
    }

    if (my $home = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
        class_name => 'home_page',
        parent_id => $App->Param('i_c')
    )->fetchrow_hashref()) {
        $home->{'href'} = ConstructURLAstlombard(node_id => $home->{'object_id'});
        unshift @$items, $home;
    }

    if (my $lombard = $DB->LOMBARDS_GET_LOMBARD(lombard_id => $App->Param('lombard_id'))->fetchrow_hashref()) {
        $lombard->{'city'} = $DB->LOMBARDS_GET_CITY(city_id => $lombard->{'city_id'})->fetchrow_hashref();

        push @$items, { object_name => $lombard->{'city'}->{'city_name'} . ', ' . $lombard->{'lombard_address'} };
        $items->[@$items - 1]->{'_active'} = 1 if (@$items);
    }

    return $items;
}

1;
