package astlombard::controllers::__site__;

use utf8;
use strict;

use base qw(Engine::Controller);

use Engine::GlobalVars;
use Engine::Xslate::Functions qw(XslateFunctions);

use astlombard::lib::Helpers;
use astlombard::lib::Location;

use Data::Dumper;

use HTTP::BrowserDetect;
use Storable qw(dclone);


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;
    
    return $self;
}

sub Init {
    my $self = shift;
    my (%params) = @_;

    $self->__InitTemplateFunctions();

    my $data = {
        meta => $self->GetMeta(),
        current_node => $self->GetCurrentNode(),
        current_url => $self->GetCurrentURL(),
        interface_name => $self->GetInterfaceName(),
        inline => $self->GetInline(),
        mode => $self->Mode,
        browser => $self->Browser,
        contacts => $self->GetContacts(),
        city => $self->GetCity(),
        cabinet => $self->GetCabinet()
    };

    $data->{'menu'} = {
        top => $self->GetMenu(1),
        main => $self->GetMenu(2),
        main_adaptive => $self->GetMenu(3),
        footer => $self->GetMenu(4),
        footer_adaptive => $self->GetMenu(5)
    };

    $data->{'banners'} = {
        foot => $self->GetBanner(1)
    };

    $data->{'breadcrumbs'} = $self->GetBreadcrumbs();

    $self->AddData($data);
}

# ===========================================================

sub GetCabinet {
    my $self = shift;

    my $cabinet = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
        parent_id => $App->Param('i_c'),
        class_name => 'personal_office'
    )->fetchrow_hashref();
    
    $cabinet->{'href'} = ConstructURLAstlombard(node_id => $cabinet->{'object_id'});

    return $cabinet;
}

# ===========================================================

sub GetCity {
    my $self = shift;

    if (my $city_id = $self->Location->GetCity()) {
        print "city_id\n" . Dumper($city_id);
        return $DB->LOMBARDS_GET_CITIES(city_id => $city_id)->fetchrow_hashref();
    }

    return undef;
}

# ===========================================================

sub GetCatalog {
    my $self = shift;

    my $cat = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
        parent_id => $App->Param('i_c'),
        class_name => 'prod_cat'
    )->fetchrow_hashref();
    
    return $cat;
}

sub GetCatalogGroup {
    my $self = shift;
    my ($type) = @_;

    my $cat = $self->GetCatalog();
    
    if (my $group = $DB->CAT_MEGAGET(
        parent_id => $cat->{'object_id'},
        class_name => 'prod_group',
        attrs => ['header', 'mark', 'publication'],
        condition => qq{publication AND mark = :type},
        type => $type
    )->fetchrow_hashref()) {
        $group->{'href'} = ConstructURLAstlombard(node_id => $group->{'object_id'});

        return $group;
    }
    return undef;
}

sub GetProductsTypes {
    my $self = shift;
    my ($parent_id) = @_;

    my $prod_types = $DB->CAT_MEGAGET(
        parent_id => $parent_id,
        class_name => 'prod_type',
        attrs => ['header', 'filter_header', 'prod_class_name', 'publication'],
        condition => qq{publication}
    )->fetchall_arrayref({});

    return $prod_types;
}

sub GetProductType {
    my $self = shift;
    my ($object_id) = @_;
    
    if (my $prod_type = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $object_id,
        attrs => ['prod_class_name']
    )->fetchrow_hashref()) {

        return $prod_type;
    }  
    return undef;
}

sub GetProductGroup {
    my $self = shift;
    my ($child_id) = @_;

    my $prod_type = $self->GetProductType($child_id);

    if (my $prod_group = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $prod_type->{'parent_id'},
        attrs => ['header', 'mark']
    )->fetchrow_hashref()) {
        
        return $prod_group;
    }
    return undef;
}

sub GetProducts {
    my $self = shift;
    my (%params) = @_;

    my @classes = $DB->GetArray(qq{SELECT `name` FROM `T_##_CAT_CLASSES` WHERE `name` LIKE 'prod_page%'});
    my @item_attrs = ('header', 'picture', 'price', 'sale_price', 'booking', 'publication');
    my @count_attrs = ('booking', 'publication');

    if (exists $params{'extra_attrs'}) {
        foreach my $attr (@{ $params{'extra_attrs'} }) {
            push @item_attrs, $attr if (!grep { $_ eq $attr } @item_attrs);
            push @count_attrs, $attr if (!grep { $_ eq $attr } @count_attrs);
        }
        delete $params{'extra_attrs'};
    }

    my $items = $DB->CAT_MEGAGET(
        class_name => \@classes,
        attrs => \@item_attrs,
        condition => qq{publication AND !booking},
        recursive => 1,
        %params
    )->fetchall_arrayref({});

    my $count = $DB->CAT_MEGAGET(
        class_name => \@classes,
        attrs => \@count_attrs,
        condition => qq{publication AND !booking},
        recursive => 1,
        %params,
        on_page => 1,
        page_num => 1,
        count_only => 1
    );

    foreach my $item (@$items) {
        $item->{'href'} = ConstructURLAstlombard(node_id => $item->{'object_id'});
        $item->{'picture_img'} = GetImage(file_id => $item->{'picture'}, alt => $item->{'header'}) if ($item->{'picture'} > 0);
    }

    return ($items, $count);
}

# ===========================================================

sub GetNews {
    my $self = shift;
    my (%params) = @_;

    my $news = {};

    if (my $stocks_news_page = $DB->CAT_MEGAGET(
        parent_id => $App->Param('i_c'),
        class_name => 'stocks_news_page',
        attrs => ['publication'],
        condition => qq{publication}
    )->fetchrow_hashref()) {
        $news->{'href'} =  ConstructURLAstlombard(node_id => $stocks_news_page->{'object_id'});
    }

    if (my $cat = $DB->CAT_MEGAGET(
        parent_id => $App->Param('i_c'),
        class_name => 'news_cat',
        attrs => ['header', 'publication'],
        condition => qq{publication}
    )->fetchrow_hashref()) {
        
        my $items = $DB->CAT_MEGAGET(
            class_name => 'news',
            attrs => ['publication_date', 'preview', 'content', 'publication'],
            order_by => [['publication_date', 'DESC']],
            %params
        )->fetchall_arrayref({});

        foreach my $item (@$items) {
            $item->{'href'} = ConstructURLAstlombard(node_id => $item->{'object_id'});
        }

        $news->{'items'} = $items;
        $news->{'header'} = $cat->{'header'};

        return $news;
    }
    return undef;
}

sub GetNewsCount {
    my $self = shift;

    return $DB->CAT_MEGAGET(
        class_name => 'news',
        attrs => ['publication'],
        condition => qq{publication},
        count_only => 1
    );
}

# ===========================================================

sub GetStocks {
    my $self = shift;
    my (%params) = @_;
    
    my $stocks = {};

    if (my $stocks_news_page = $DB->CAT_MEGAGET(
        parent_id => $App->Param('i_c'),
        class_name => 'stocks_news_page',
        attrs => ['publication'],
        condition => qq{publication}
    )->fetchrow_hashref()) {
        $stocks->{'href'} =  ConstructURLAstlombard(node_id => $stocks_news_page->{'object_id'});
    }

    if (my $cat = $DB->CAT_MEGAGET(
        parent_id => $App->Param('i_c'),
        class_name => 'stock_cat',
        attrs => ['header', 'publication'],
        condition => qq{publication}
    )->fetchrow_hashref()) {

        my $items = $DB->CAT_MEGAGET(
            class_name => 'stock',
            attrs => ['picture', 'publication'],
            condition => qq{publication},
            order_by => [['RAND()']],
            %params
        )->fetchall_arrayref({});

        foreach my $item (@$items) {
            $item->{'picture_img'} = GetImage(file_id => $item->{'picture'}, alt => $item->{'object_name'}) if ($item->{'picture'} > 0);
            $item->{'href'} = ConstructURLAstlombard(node_id => $item->{'object_id'});
        }

        $stocks->{'items'} = $items;
        $stocks->{'header'} = $cat->{'header'};

        return $stocks;
    }
    return undef;
}

# ===========================================================

sub GetAdvantages {
    my $self = shift;

    my $advantages = {};

    if (my $cat = $DB->CAT_MEGAGET(
        parent_id => $App->Param('a_c'),
        class_name => 'advantage_cat',
        attrs => ['header', 'publication'],
        condition => qq{publication}
    )->fetchrow_hashref()) {

        my $items = $DB->CAT_MEGAGET(
            parent_id => $cat->{'object_id'},
            class_name => 'advantage',
            attrs => ['icon', 'annotation', 'publication'],
            condition => qq{publication},
            use_publ_pos => 1
        )->fetchall_arrayref({});

        foreach my $item (@$items) {
            $item->{'icon_img'} = GetImage(file_id => $item->{'icon'}, alt => $item->{'object_name'}) if ($item->{'icon'} > 0);
        }

        $advantages->{'items'} = $items;
        $advantages->{'header'} = $cat->{'header'};

        return $advantages;
    }
    return undef;
}

# ===========================================================

sub GetContacts {
    my $self = shift;

    if (my $contacts = $DB->CAT_MEGAGET(
        class_name => 'contacts',
        attrs => ['logo', 'phone_1', 'phone_caption_1', 'phone_caption_mobile_1']
    )->fetchrow_hashref()) {

        ($contacts->{'phone_clean_1'} = $contacts->{'phone_1'}) =~ s/[^0-9\+]//g;
        $contacts->{'logo_img'} = GetImage(file_id => $contacts->{'logo'}) if ($contacts->{'logo'} > 0);
        
        return $contacts;
    }  
    return undef;
}

# ===========================================================

sub GetCurrentNode {
    my $self = shift;
    return $App->Data->Var('current_node');
}

sub GetCurrentURL {
    my $self = shift;
    return $App->Request->url;
}

sub GetInterfaceName {
    my $self = shift;
    return $App->Data->Var('current_node')->{'class_name'} || '404';
}

sub GetInline {
    my $self = shift;

    my $modes = {
        dev => '/css/public/inline.css',
        prod => '/css/dist/inline.css'
    };
    my $inline = {
        css => '',
        js => ''
    };

    my $css_file = $path.$modes->{$self->Mode};
    if (-f $css_file) {
        $inline->{'css'} = `cat "$css_file"`;
    }

    return $inline;
}

# ===========================================================

sub GetMeta {
    my $self = shift;
    return Metatags('html5');
}

sub GetMenu {
    my $self = shift;
    my ($place) = @_;

    my $items = $DB->GET_MENU_ITEMS(menu_place => $place)->fetchall_arrayref({});
    foreach my $item (@$items) {
        my $icon = $DB->CAT_GET_OBJECT_ATTR(object_id => $item->{'object_id'}, attr_name => 'icon');

        $item->{'icon'} = GetImage(
            file_id => $icon, 
            alt => $item->{'object_name'}
        ) if ($icon > 0);
        $item->{'href'} = $item->{'link'} || ConstructURLAstlombard(node_id => $item->{'node'});
        $item->{'_active'} = 1 if ($self->PathRel->{ $item->{'node'} });

        my $subitems = $DB->GET_MENU_ITEMS(
            parent_id => $item->{'object_id'}
        )->fetchall_arrayref({});

        foreach my $subitem (@$subitems) {
            my $subitem_icon = $DB->CAT_GET_OBJECT_ATTR(object_id => $subitem->{'object_id'}, attr_name => 'icon');

            $subitem->{'icon'} = GetImage(
                file_id => $subitem_icon, 
                alt => $subitem->{'object_name'}
            ) if ($subitem_icon > 0);
            $subitem->{'href'} = $subitem->{'link'} || ConstructURLAstlombard(node_id => $subitem->{'node'});
            $subitem->{'_active'} = 1 if ($self->PathRel->{ $subitem->{'node'} });
            $item->{'_subitems'} = 1;
        }

        $item->{'subitems'} = $subitems;
    }
    
    return $items;
}

sub GetBanner {
    my $self = shift;
    return GetBanners(@_);
}

sub GetBreadcrumbs {
    my $self = shift;

    print "Processing GetBreadcrumbs\n";

    my $items = [];
    if ($App->Data->Var('current_node')->{'class_name'} ne 'home_page') {
    
        print "Not homepage\n";
    
        $items = dclone($self->Path);

        for (my $i = 0; $i < @$items; $i ++) {
            my $item = $items->[$i];

            if ($item->{'slink'} ne '' and $i != @$items - 1) {
                $item->{'href'} = ConstructURLAstlombard(node_id => $item->{'object_id'});
            }
        }

        if (my $home = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
            class_name => 'home_page',
            parent_id => $App->Param('i_c')
        )->fetchrow_hashref()) {
            $home->{'href'} = ConstructURLAstlombard(node_id => $home->{'object_id'});
            unshift @$items, $home;
        }

        $items->[@$items - 1]->{'_active'} = 1 if (@$items);
        print Dumper($items);
    }
    return $items;
}

# ===========================================================

sub ProcessForm {
    my $self = shift;
    my ($obj, $params) = @_;

    if ($App->Param('form_id') == $obj->{'object_id'} and $App->Param('_action') eq 'send_form') {
        my $handler_path = $path.'/handlers/Forms.pm';
        my $handler_name = $project.'::handlers::Forms';
        require $handler_path or die "Can't require $handler_path: $!";
        my $handler = $handler_name->new();
        my $res = $handler->ProcessForm();
        $self->can_render(0) if ($res == 1);
    }

    if ((!$App->Param('form_id') or $App->Data->Var('__AJAX_REQUEST')) and $App->Session->Var('FORMS_'.$obj->{'_form_class_name'})) {
        $App->Session->UndefVar('FORMS_'.$obj->{'_form_class_name'});
        $obj->{'_form_sent'} = 1;
    } elsif (!$params->{'skip_construction'}) {
        $obj->{'form'} = $self->ConstructForm($obj, $params);
    } else {
        my $attrs = $DB->CAT_GET_CLASS_ATTRS(class_name => $obj->{'_form_class_name'}, attr_group_name => 'display')->fetchall_hashref('attr_id');
        my $errors = $App->Data->Var($obj->{'_form_class_name'}.'_errors');
        $errors = { map { $attrs->{$_}->{'attr_name'} => $errors->{$_} } (keys %$errors) };
        $errors->{'captcha'} = 'error' if ($App->Data->Var('__CAPTCHA_ERROR'));
        $errors->{'agreement'} = 'error' if ($App->Data->Var('__AGREEMENT_ERROR'));
        $obj->{'errors'} = $errors;
    }

    if (defined $App->Param('node') and (my $node = $DB->CAT_GET_OBJECT(object_id => $App->Param('node'))->fetchrow_hashref())) {
        $obj->{$_} =~ s{
            \#\#(\w+)\#\#
        }{
            (defined $node->{$1}) ? $node->{$1} : ''
        }egx foreach (grep { $obj->{$_} } qw/object_name _content _s_content/);
    }

    return $obj;
}

=comment
    params:
        action - action формы
        no_agreement - выключить соглашение о персональных данных
        no_captcha - выключить каптчу
        no_remark - не показывать подпись
        hiddens - массив скрытых полей вида { name => 'field', value => 'value' }

=cut
sub ConstructForm {
    my $self = shift;
    my ($obj, $params) = @_;
    $params ||= {};

    my $action = '#'.$obj->{'_form_class_name'};
    if (defined $params->{'action'}) {
        $action = $params->{'action'};
    } elsif ($obj->{'slink'}) {
        my %url = (
            node_id => $obj->{'object_id'},
            _anchor => 'form-'.$obj->{'_form_class_name'}
        );
        $url{'page'} = $App->Param('page') if (defined $App->Param('page'));
        $action = ConstructURLAstlombard(%url);
    }

    my $preprocess = sub {
        my ($input, $field) = @_;

        if ($field->{'_required'} and $field->{'field_type'} ne 'array' and $field->{'field_type'} ne 'set') {
            $input->{'required'} = 'required';
        }
        if ($field->{'field_type'} eq 'e_mail') {
            $input->{'type'} = 'email';
        } elsif ($field->{'field_type'} eq 'url') {
            $input->{'type'} = 'url';
        } elsif ($field->{'attr_name'} =~ /phone/) {
            $input->{'type'} = 'tel';
            $input->{'pattern'} = '\\+7 \\(\\d{3}\\) \\d{3}-\\d{2}-\\d{2}';
            $input->{'title'} = '+7 (xxx) xxx-xx-xx';
        }

        if ($field->{'field_type'} eq 'file') {
            my $file_type = $App->Settings->GetParamValue('_XCatalog.AttrParams.Type.'.$field->{'attr_id'});
            my $file_cat_id = $App->Settings->GetParamValue('_XCatalog.AttrParams.FilesCat.'.$field->{'attr_id'});

            $input->{'accept'} = 'image/*' if ($file_type eq 'image');
            $input->{'data-file_cat_id'} = $file_cat_id if ($file_cat_id);
            $input->{'data-file_type'} = $file_type if ($file_type);
        }

        return $input;
    };

    my @hiddens = (
        { name => '_action', value => 'send_form' },
        { name => 'form_id', value => $obj->{'object_id'} }
    );
    push @hiddens, { name => 'node', value => $App->Param('node') } if (defined $App->Param('node'));
    push @hiddens, { name => '__agreement', value => 1 } if ($params->{'no_agreement'});
    push @hiddens, @{ $params->{'hiddens'} } if (defined $params->{'hiddens'});

    $App->Param($obj->{'_form_class_name'}.'_error', 1);

    my $form = {
        action => $action,
        object => $obj,
        fields  => GetForm(form_class => $obj->{'_form_class_name'}, attrs_group => 'display', preprocess => $preprocess),
        hiddens => \@hiddens,
        submit_label => $App->Settings->GetParamValue('Forms.SubmitLabel')
    };

    $form->{'captcha'} = {
        src => CreateCaptcha(color1 => '#333333', color2 => '#bbbbbb', type => 2, id => $obj->{'_form_class_name'}),
        label => $App->Settings->GetParamValue('Forms.CaptchaLabel'),
        comment => $App->Settings->GetParamValue('Forms.CaptchaComment'),
        _error => ($App->Data->Var('__CAPTCHA_ERROR')) ? 1 : 0
    } if (!$App->Data->Var('__AJAX_REQUEST') and !$params->{'no_captcha'});

    $form->{'agreement'} = {
        label => $App->Settings->GetParamValue('Forms.AgreementLabel'),
        comment => $App->Settings->GetParamValue('Forms.AgreementComment'),
        _error => ($App->Data->Var('__AGREEMENT_ERROR')) ? 1 : 0
    } if (!$params->{'no_agreement'});

    $form->{'remark'} = $App->Settings->GetParamValue('Forms.RemarkText') if (!$params->{'no_remark'});

    $App->UndefParam($obj->{'_form_class_name'}.'_error');

    return $form;
}

# ===========================================================

sub Path {
    my $self = shift;
    if (!exists $self->{'__PATH'}) {
        $self->{'__PATH'} = [ grep { $_->{'class_name'} ne 'root' and $_->{'class_name'} ne 'information_cat' and $_->{'class_name'} ne 'auxiliary_cat' } @{ $DB->CAT_GET_PATH_TO_NODE(
            node_id => $App->Param('node_id'),
            result_type => 'sql'
        )->fetchall_arrayref({}) } ];
    }
    return $self->{'__PATH'};
}

sub PathRel {
    my $self = shift;
    if (!exists $self->{'__PATH_REL'}) {
        $self->{'__PATH_REL'} = { map { $_->{'object_id'} => $_ } @{ $self->Path } };
    }
    return $self->{'__PATH_REL'};
}

sub Browser {
    my $self = shift;
    if (!exists $self->{'__BROWSER'}) {
        my $ua = HTTP::BrowserDetect->new($App->Request->user_agent);
        my $type = 'desktop';
        if ($ua->mobile) {
            $type = 'mobile';
        } elsif ($ua->tablet) {
            $type = 'tablet';
        }
        $self->{'__BROWSER'} = {
            is_mobile => $ua->mobile,
            is_tablet => $ua->tablet,
            is_desktop => !$ua->device,
            type => $type
        };
    }
    return $self->{'__BROWSER'};
}

sub Mode {
    my $self = shift;
    if (!exists $self->{'__MODE'}) {
        my $mode = $App->Session->Var('mode') || 'dev';
        if (defined $App->Param('mode')) {
            $mode = $App->Param('mode');
            $mode = 'prod' if ($mode ne 'dev' and $mode ne 'prod');
        } elsif (defined $App->Param('set_mode')) {
            $mode = $App->Param('set_mode');
            $mode = 'prod' if ($mode ne 'dev' and $mode ne 'prod');
            $App->Session->Var('mode', $mode);
        }
        $self->{'__MODE'} = $mode;
    }
    return $self->{'__MODE'};
}

# ===========================================================

sub AddData {
    my $self = shift;
    my ($data) = @_;
    my $superdata = $self->Data;
    $superdata->{$_} = $data->{$_} foreach (keys %$data);
}

sub __InitTemplateFunctions {
    my $self = shift;
    $self->Templates(
        function => XslateFunctions()
    );
}

# ===========================================================

sub Location {
    my $self = shift;
    if (!exists $self->{'__LOCATION'}) {
        $self->{'__LOCATION'} = astlombard::lib::Location->new(@_);
    }
    return $self->{'__LOCATION'};
}

1;
