package astlombard::controllers::__lombard__;

use utf8;
use strict;

use base qw(astlombard::controllers::__site__);


use Engine::GlobalVars;
use astlombard::lib::Helpers;

use Data::Dumper;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;
    
    return $self;
}

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    my $data = {
        
    };

    $self->AddData($data);
}

# ===========================================================

sub GetLombards {
    my $self = shift;

    my $lombards = {};

    my $query = $self->__GetQuery();

    $lombards = $DB->LOMBARDS_GET_LOMBARDS(%$query)->fetchall_arrayref({});

    foreach my $lombard (@$lombards) {
        if (my $region = $DB->LOMBARDS_GET_REGIONS(
            region_id => $lombard->{'region_id'}
        )->fetchrow_hashref()) {
            $lombard->{'region_name'} = $region->{'region_name'};
        }

        if (my $city = $DB->LOMBARDS_GET_CITIES(
            city_id => $lombard->{'city_id'}
        )->fetchrow_hashref()) {
            $lombard->{'city_name'} = $city->{'city_name'};
        }

        $lombard->{'href'} = ConstructURLAstlombard(lombard_id => $lombard->{'lombard_id'});
        $lombard->{'location'} = $lombard->{'region_name'} . ', ' . $lombard->{'city_name'} . ', ' . $lombard->{'lombard_address'};
        $lombard->{'hours'} = $self->GetLombardHours(lombard_id => $lombard->{'lombard_id'});
        $lombard->{'services'} = $self->GetLombardServices(lombard_id => $lombard->{'lombard_id'});
        $lombard->{'stocks'} = $self->GetLombardStocks(lombard_id => $lombard->{'lombard_id'});
    }

    return $lombards;
}

# ===========================================================

sub GetLombardServices {
    my $self = shift;
    my (%params) = @_;

    my $items = $DB->LOMBARDS_GET_LOMBARD_SERVICES(
        lombard_id => $params{'lombard_id'}
    )->fetchall_arrayref({});

    foreach my $item (@$items) {
        if (my $group = $DB->CAT_GET_OBJECT_WITH_ATTRS(
            object_id => $item->{'service_id'},
            attrs => ['icon']
        )->fetchrow_hashref()) {
            $item->{'service_name'} = $group->{'object_name'};
            $item->{'icon_img'} = GetImage(file_id => $group->{'icon'}, alt => $group->{'header'}) if ($group->{'icon'} > 0);
            $item->{'href'} = ConstructURLAstlombard(node_id => $group->{'object_id'});
        }
    }

    my $services = {
        buy => [ grep { $_->{'type'} eq 'buy' } @$items ],
        sale => [ grep { $_->{'type'} eq 'sale' } @$items ]
    };

    return $services;
}

# ===========================================================

sub GetLombardHours {
    my $self = shift;
    my (%params) = @_;

    my $hours = {};

    my $items = $DB->LOMBARDS_GET_LOMBARD_HOURS(
        lombard_id => $params{'lombard_id'},
        order_by => 'day, start_time, finish_time'
    )->fetchall_arrayref({});

    my $work_hours = [ grep { $_->{'type'} eq 'work' } @$items ];
    my $lunch_hours = [ grep { $_->{'type'} eq 'lunch' } @$items ];

    $hours->{'work_hours'} = $self->MergeHours($work_hours);
    $hours->{'lunch_hours'} = $self->MergeHours($lunch_hours);

    return $hours;
}

sub MergeHours {
    my $self = shift;
    my ($hours) = @_;

    return [] if (!@$hours);

    my @slices = ();
    my $slice = {
        days => [ $self->GetWeekDay($hours->[0]->{'day'}) ],
        start_time => $hours->[0]->{'start_time'},
        finish_time => $hours->[0]->{'finish_time'}
    };
    for (my $i = 1; $i < @$hours; $i ++) {
        my $hour = $hours->[$i];
        if ($hour->{'start_time'} eq $slice->{'start_time'} and $hour->{'finish_time'} eq $slice->{'finish_time'}) {
            push @{ $slice->{'days'} }, $self->GetWeekDay($hour->{'day'});
        } else {
            push @slices, $slice;
            $slice = {
                days => [ $self->GetWeekDay($hour->{'day'}) ],
                start_time => $hour->{'start_time'},
                finish_time => $hour->{'finish_time'}
            };
        }
    }
    push @slices, $slice;

    return \@slices;
}

sub GetWeekDay {
    my $self = shift;
    my ($day) = @_;

    my $DAYS = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];
    return $DAYS->[ $day - 1 ];
}

# ===========================================================

sub GetLombardStocks {
    my $self = shift;
    my (%params) = @_;

    my $stocks = {};

    my $items = $DB->LOMBARDS_GET_LOMBARD_STOCKS(
        lombard_id => $params{'lombard_id'}
    )->fetchall_arrayref({});

    foreach my $item (@$items) {
        if (my $stock = $DB->CAT_GET_OBJECT_WITH_ATTRS(
            object_id => $item->{'stock_id'},
            attrs => ['picture']
        )->fetchrow_hashref()) {
            $item->{'picture_img'} = GetImage(file_id => $stock->{'picture'}, alt => $stock->{'header'}) if ($stock->{'picture'} > 0);
            $item->{'href'} = ConstructURLAstlombard(node_id => $stock->{'object_id'});
        }
    }
    $stocks->{'items'} = $items;

    return $stocks;
}

# ===========================================================

sub __ExtractParams {
    my $self = shift;

    my $params = {};
    if ($App->Param('city')) {
        $params->{'city_id'} = $App->Param('city');
    } 
    if ($App->Param('is_opened')) {
        $params->{'is_opened'} = $App->Param('is_opened');
    }
    if ($App->Param('is_24hours')) {
        $params->{'is_24hours'} = $App->Param('is_24hours');   
    }
    if ($App->Param('buy')) {
        $params->{'services_buy'} = [ $App->Param('buy') ];
    }
    if ($App->Param('sale')) {
        $params->{'services_sale'} = [ $App->Param('sale') ];
    }

    return $params;
}

sub __GetQuery {
    my $self = shift;

    my $params = $self->__ExtractParams();
    my %query = ();

    if ($params->{'city_id'}) {
        $query{'city_id'} = $params->{'city_id'};
    }

    if ($params->{'is_24hours'}) {
        $query{'24hours'} = $params->{'is_24hours'};
    }

    if ($params->{'is_opened'}) {
        $query{'is_opened'} = 1;
    }

    if ($params->{'services_buy'}) {
        $query{'services_buy'} = $params->{'services_buy'};
    }

    if ($params->{'services_sale'}) {
        $query{'services_sale'} = $params->{'services_sale'};
    } 

    return \%query;
}

1;
