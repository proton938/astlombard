#!/usr/bin/perl

$project = 'astlombard';
$prefix = '/usr/local/flexites';
$path = $prefix.'/projects/'.$project;

use utf8;
use strict;
use warnings;
use POSIX;

use lib '/usr/local/flexites/lib';
use lib '/usr/local/flexites/projects';

use Engine::GlobalVars;
use Engine::Config;
use Engine::Application;

use astlombard::dblib::Database;
use astlombard::lib::Import;

my $mod = join '', @ARGV;

my $pidfile = $path.'/var/import'.$mod.'.pid';

die "Script already running" unless (PIDFileCheck());

my $logfile = $path.'/var/import'.$mod.'.log';
my $errfile = $path.'/var/import.errors'.$mod.'.log';
my $warnfile = $path.'/var/import.warnings'.$mod.'.log';

my $LOG;
open $LOG, '>:utf8', $logfile or die "Can't open log file $logfile";
select $LOG;

$SIG{'__DIE__'} = $SIG{'TERM'} = $SIG{'INT'} = \&Finish;

Start();
Process();
Finish();

sub Start {
    print "Импорт данных: ".strftime('%d.%m.%Y %H:%M:%S', localtime())."\n\n";
    print "=======================================================\n\n";

    $Conf = Engine::Config->new();
    $Conf->LoadFromFile($prefix.'/conf/'.$project.'.conf');

    $DB = astlombard::dblib::Database->new();
    $DB->Connect(
        $Conf->Param('database/dsn'),
        $Conf->Param('database/host'),
        $Conf->Param('database/username'),
        $Conf->Param('database/password')
    );
    $DB->DatabaseVersion($Conf->Param('database/database_version'));
    $App = Engine::Application->new();
}

sub Process {
    my $import = astlombard::lib::Import->new();
    $import->Start(@ARGV);
}

sub Finish {
    my $why = shift;
    my $stack = 1;

    for (my $stack = 1; my $s = (CORE::caller($stack))[3]; $stack ++) {
        if ($s eq '(eval)') {
            return;
        }
    }

    print "[ ERROR ] $why\n" if ($why);

    print "\n=======================================================\n\n";
    if ($why) {
        print "Завершено аварийно: ".strftime('%d.%m.%Y %H:%M:%S', localtime())."\n";
    } else {
        print "Завершено успешно: ".strftime('%d.%m.%Y %H:%M:%S', localtime())."\n";
    }
    close $LOG;
    select STDIN;
    chmod 0666, $logfile;

    `cat $logfile | grep WARNING > $warnfile`;
    `cat $logfile | grep ERROR > $errfile`;

    if (!-s $warnfile) {
        unlink $warnfile;
    } else {
        chmod 0666, $warnfile;
    }
    if (!-s $errfile) {
        unlink $errfile;
    } else {
        chmod 0666, $errfile;
    }

    undef $App;
    undef $DB;

    unlink $pidfile;

    if ($why) {
        exit 1;
    } else {
        exit;
    }
}

sub PIDFileCheck {
    return 0 if (-f $pidfile);
    my $fh;
    open $fh, '>', $pidfile or die "Can't open PID file $pidfile";
    print $fh $$;
    close $fh;
    return 1;
}
