package astlombard::handlers::Backoffice;

use strict;
use utf8;

use Engine::GlobalVars;

use astlombard::lib::Import;

sub new {
    my $class = shift;
    my $self = {};
    bless $self, $class;
    return $self;
}

# -------------------------------------------

sub TransitObjectBefore {
    my $self = shift;
    my $flag = $App->Param('cmd');
    print "[ Backoffice ] TransitObjectBefore $flag\n";

    # Flags: 
    # 1 - нерекурсивное копирование
    # 2 - рекурсивное копирование
    # 3 - нерекурсивное перемещение
    # 4 - рекурсивное перемещение

    #my $object = $DB->CAT_GET_OBJECT(object_id => $App->Param('object_id'))->fetchrow_hashref();
    #my $parent = $DB->CAT_GET_OBJECT(object_id => $App->Param('parent_id'))->fetchrow_hashref();
    #return if (!$object or !$parent);
}

sub TransitObjectAfter {
    my $self = shift;
    my $flag = $App->Param('cmd');
    print "[ Backoffice ] TransitObjectAfter $flag\n";
}

sub NewObjectBefore {
    my $self = shift;

    print "[ Backoffice ] NewObjectBefore\n";
}

sub NewObjectAfter {
    my $self = shift;
    print "[ Backoffice ] NewObjectAfter\n";

    my $object = $DB->CAT_GET_OBJECT(object_id => $App->Data->Var('created_object_id'))->fetchrow_hashref();
    return if (!$object);

    if ($object->{'class_name'} eq 'stock') {
        $self->Import->Stocks->ImportObjectStock($object->{'object_id'});
    }
}

sub EditObjectBefore {
    my $self = shift;
    print "[ Backoffice ] EditObjectBefore\n";

    #my $object = $DB->CAT_GET_OBJECT(object_id => $App->Param('object_id'))->fetchrow_hashref();
    #return if (!$object);
}

sub EditObjectAfter {
    my $self = shift;
    print "[ Backoffice ] EditObjectAfter\n";

    my $object = $DB->CAT_GET_OBJECT(object_id => $App->Param('object_id'))->fetchrow_hashref();
    return if (!$object);

    if ($object->{'class_name'} eq 'stock') {
        $self->Import->Stocks->ImportObjectStock($object->{'object_id'});
    }
}

sub DelObjectBefore {
    my $self = shift;
    my $flag = $App->Param('cmd');
    print "[ Backoffice ] DelObjectBefore $flag\n";

    #my $object = $DB->CAT_GET_OBJECT(object_id => $App->Param('object_id'))->fetchrow_hashref();
    #return if (!$object);
}

sub DelObjectAfter {
    my $self = shift;
    my $flag = $App->Param('cmd');
    print "[ Backoffice ] DelObjectAfter $flag\n";
}

# -------------------------------------------

sub Import {
    my $self = shift;
    if (!exists $self->{'__IMPORT'}) {
        $self->{'__IMPORT'} = astlombard::lib::Import->new();
    }
    return $self->{'__IMPORT'};
}

1;
