SET NAMES utf8;

#DROP TABLE IF EXISTS
#	 `T_AL_REGIONS`,
#	 `T_AL_CITIES`,
#    `T_AL_LOMBARDS`,
#    `T_AL_LOMBARDS_SERVICES`,
#    `T_AL_LOMBARDS_HOURS`,
#	 `T_AL_LOMBARDS_TIMESTAMPS`,
#    `T_AL_STOCKS`,
#;

# ==============================================================

CREATE TABLE IF NOT EXISTS `T_AL_REGIONS` (
	`region_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`region_name` varchar(255) NOT NULL,
	PRIMARY KEY (`region_id`),
	UNIQUE KEY `regions_name_unq` (`region_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_AL_CITIES` (
	`city_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`city_name` varchar(255) NOT NULL,
	`region_id` int(10) unsigned NOT NULL,
	PRIMARY KEY (`city_id`),
	UNIQUE KEY `cities_name_region_unq` (`city_id`, `region_id`),
	CONSTRAINT `cities_region_fk` FOREIGN KEY (`region_id`) REFERENCES `T_AL_REGIONS` (`region_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_AL_LOMBARDS` (
	`lombard_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`lombard_code` varchar(255) NOT NULL,
	`lombard_address` varchar(255) NOT NULL,
	`lombard_coords` varchar(255) NOT NULL,
	`lombard_name` varchar(255) NOT NULL,
	`24hours` tinyint(1) unsigned NOT NULL,
	`slink` varchar(255) NOT NULL,
	`region_id` int(10) unsigned NOT NULL,
	`city_id` int(10) unsigned NOT NULL,
	`hash` varchar(255) NOT NULL,
	PRIMARY KEY (`lombard_id`),
	UNIQUE KEY `lombards_code_unq` (`lombard_code`),
	UNIQUE KEY `lombards_hash_unq` (`hash`),
	UNIQUE KEY `lombards_slink_unq` (`slink`, `city_id`),
	CONSTRAINT `lombards_region_fk` FOREIGN KEY (`region_id`) REFERENCES `T_AL_REGIONS` (`region_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `lombards_city_fk` FOREIGN KEY (`city_id`) REFERENCES `T_AL_CITIES` (`city_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_AL_LOMBARDS_SERVICES` (
	`lombard_id` int(10) unsigned NOT NULL,
	`service_id` int(8) NOT NULL,
	`type` enum('sale', 'buy') NOT NULL,
	PRIMARY KEY (`lombard_id`, `service_id`, `type`),
	CONSTRAINT `services_lombard_fk` FOREIGN KEY (`lombard_id`) REFERENCES `T_AL_LOMBARDS` (`lombard_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `services_service_fk` FOREIGN KEY (`service_id`) REFERENCES `T_AL_CAT_NODES` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_AL_LOMBARDS_HOURS` (
	`lombard_id` int(10) unsigned NOT NULL,
	`day` tinyint(1) unsigned NOT NULL,
	`start_time` time,
	`finish_time` time,
	`type` enum('work', 'lunch'),
	PRIMARY KEY (`lombard_id`, `day`, `start_time`, `finish_time`, `type`),
	CONSTRAINT `hours_lombard_fk` FOREIGN KEY (`lombard_id`) REFERENCES `T_AL_LOMBARDS` (`lombard_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_AL_LOMBARDS_TIMESTAMPS` (
	`lombard_id` int(10) unsigned NOT NULL,
	`day` tinyint(1) unsigned NOT NULL,
	`start_time` int(10) unsigned NOT NULL,
	`finish_time` int(10) unsigned NOT NULL,
	`type` enum('work', 'lunch'),
	PRIMARY KEY (`lombard_id`, `day`, `start_time`, `finish_time`, `type`),
	CONSTRAINT `timestamps_lombard_fk` FOREIGN KEY (`lombard_id`) REFERENCES `T_AL_LOMBARDS` (`lombard_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_AL_LOMBARDS_STOCKS` (
    `lombard_id` int(10) unsigned NOT NULL,
	`stock_id` int(8) NOT NULL,
	`start_date` date NOT NULL,
	`finish_date` date NOT NULL,
	PRIMARY KEY (`lombard_id`, `stock_id`, `start_date`, `finish_date`),
	CONSTRAINT `stocks_lombard_fk` FOREIGN KEY (`lombard_id`) REFERENCES `T_AL_LOMBARDS` (`lombard_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `stocks_stock_fk` FOREIGN KEY (`stock_id`) REFERENCES `T_AL_CAT_NODES` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
