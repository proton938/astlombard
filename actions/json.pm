package astlombard::actions::json;

use utf8;
use strict;

use base qw(astlombard::actions::__ajax__);

use Engine::GlobalVars;
use astlombard::lib::Helpers;
use astlombard::lib::Lombard;
use astlombard::lib::Utils;

use JSON;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless $self, $class;

    return $self;
}

#
# -----------------------------------------------------------------------
#

sub Init {
    my $self = shift;
    $self->SUPER::Init(@_);

    my $action = $App->NodePath(1);
    my $act = 'H_'.$action;
    my $res = {};
    eval { $res = $self->$act(); };
    if ($@) {
        print "[ ERROR ] [ astlombard::actions::json::Init ] Error in $act: $@\n";
        $res = {};
        $self->Status(404);
    }
    $self->{'json'} = $res;
}

sub Run {
    my $self = shift;
    $App->ContentType('application/json');
    $self->Text(encode_json($self->{'json'} || {}));
}

#
# -----------------------------------------------------------------------
#

sub H_get_form {
    my $self = shift;

    if (!defined $App->Param('form_id') and defined $App->Param('href')) {
        $App->Param('form_id', $self->GetNodeByHref($App->Param('href')));
    }

    if (my $obj = $DB->CAT_GET_OBJECT(object_id => $App->Param('form_id'))->fetchrow_hashref()) {
        my $controller = $self->CustomController($obj->{'class_name'}, $obj->{'class_name'});
        my $obj = $controller->GetObject($obj->{'object_id'});

        my $data = {
            object => $obj
        };

        my ($content, $error) = $self->Template->Content(template => 'ajax/'.$obj->{'class_name'}.'.tx', data => $data);

        return { errors => { template => 1 } } if ($error);
        return { ok => 1, content => $content };
    }

    return { errors => { form_id => 1 } };
}

sub H_send_form {
    my $self = shift;

    if (my $obj = $DB->CAT_GET_OBJECT(object_id => $App->Param('form_id'))->fetchrow_hashref()) {
        my $controller = $self->CustomController($obj->{'class_name'}, $obj->{'class_name'});
        my $obj = $controller->GetObject($obj->{'object_id'}, { skip_construction => 1 });

        if ($obj->{'_form_sent'}) {
            return {
                ok => 1,
                content => $obj->{'_s_content'}
            };
        }

        return {
            errors => $obj->{'errors'}
        };
    }

    return { errors => { form_id => 1 } };
}

#
# -----------------------------------------------------------------------
#

sub H_get_location_select {
    my $self = shift;

    my $regions = $DB->LOMBARDS_GET_REGIONS(order_by => 'region_name')->fetchall_arrayref({});
    my $cities = $DB->LOMBARDS_GET_CITIES(order_by => 'city_name')->fetchall_arrayref({});

    my $active_city_id = $self->Location->GetCity();

    foreach my $region (@$regions) {
        my @cities = ();
        my $i = 0;
        while ($i < @$cities) {
            my $city = $cities->[$i];

            if ($city->{'region_id'} == $region->{'region_id'}) {
                if (defined $active_city_id and $city->{'city_id'} == $active_city_id) {
                    $city->{'_active'} = 1;
                }

                push @cities, $city;

                splice @$cities, $i, 1;
            } else {
                $i ++;
            }
        }

        $region->{'cities'} = \@cities;
    }

    my $data = {
        regions => $regions
    };

    my ($content, $error) = $self->Template->Content(template => 'ajax/location_select.tx', data => $data);

    if ($error) {
        return {
            errors => { template => 1 }
        };
    }

    return {
        ok => 1,
        content => $content
    };
}

sub H_save_location {
    my $self = shift;

    if (my $city = $self->Location->SaveCity($App->Param('city_id'))) {
        $self->Location->Save();
        return {
            ok => 1,
            city_id => $city
        };
    }

    return { errors => { error => 1 } };
}
# Функции для сопряжения с вызовами сервисов через SOAP::Lite Названы по именам сервисов.
sub H_Register {
    my $self = shift;

    my %params = (
                     iin      => $App->Param('iin'),
                     phone    => $App->Param('phone'),
                     code     => $App->Param('code'),
                     password => $App->Param('password')
                 );
    print  $App->Session->Dump();
#    $App->Session->UndefVar('register');
    if ( (defined($App->Param('iin')) && defined($App->Param('phone'))) ) {
        print "Register hash init\n";

        $App->Session->Var('register', {});
        $App->Session->Var('register')->{'iin'} = $App->Param('iin');
        $App->Session->Var('register')->{'phone'} = $App->Param('phone');
    }
    print  $App->Session->Dump();
    my $res = $self->Lombard->Register(%params);
    print "Result:\n";
    print Dumper($res);
    return $res;
}
#-------------------------------------------------------
sub H_Authenticate {
    my $self = shift;
    my %params = (
                     phone    => $App->Param('phone'),
                     password => $App->Param('password')
                 );
    $App->Session->Var('autenticate', {});
    $App->Session->Var('autenticate')->{'iin'} = $App->Param('iin');
    $App->Session->Var('autenticate')->{'phone'} = $App->Param('phone');
    my $res = $self->Lombard->Authenticate(%params);
    print "Result:\n";
    print Dumper($res);
    return $res;
}
#-------------------------------------------------------
sub H_GetTickets {
    my $self = shift;
    my %params = (
                     phone => $App->Session->Var('autenticate')->{'phone'},
                     iin   => $App->Session->Var('autenticate')->{'iin'}
                 );
    my $res = $self->Lombard->GetTickets(%params);
    print "Result:\n";
    print Dumper($res);
    return $res;
}
#-------------------------------------------------------
sub Lombard {
    my $self = shift;
    if (!exists $self->{'_LOMBARD'}) {
        $self->{'_LOMBARD'} = astlombard::lib::Lombard->new();
    }
    return $self->{'_LOMBARD'};
}
1;
