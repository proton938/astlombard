package astlombard::actions::index;

use utf8;
use strict;

use Engine::GlobalVars;
use base qw(Actions::index);

use astlombard::lib::SysUtils;
use Data::Dumper;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = $class->SUPER::new(@_);
    return $self;
}

sub GetCurrents {
    my $self = shift;
}

sub CreateController {
    my $self = shift;

    my $class_name = undef;

    if (int($self->Status / 100) == 4) {
        $class_name = '__page_'.$self->Status;
    } elsif ($App->Data->Var('current_node') and $App->Data->Var('current_node')->{'class_name'}) {
        $class_name = $App->Data->Var('current_node')->{'class_name'};
    } elsif ($App->Param('lombard_id')) {
        $class_name = 'lombard';
    }

    my ($controller_name, $template_name) = $self->ConstructController($class_name);

    if (defined $controller_name) {
        if (my $controller = $self->CustomController($controller_name, $template_name)) {
            $self->Controller($controller);

            my $prev_status = $self->Status;

            $self->Controller->Init();

            if ($self->Status == 404 and $self->Status != $prev_status) {
                $self->TemplateNotFound();
            } else {
                $self->Controller->Render();

                if ($self->Controller->Error) {
                    $self->Status(404);
                } else {
                    $self->Controller->Finish();
                }
            }
        } else {
            $self->Status(404);
            $self->TemplateNotFound();
        }
    } else {
        $self->Status(404);
        $self->TemplateNotFound();
    }
}

sub ConstructController {
    my $self = shift;
    my ($class_name) = @_;

    my $controller_name = undef;
    my $template_name = undef;
    my $subpath = $path.'/'.$Conf->Param('common/controllers');

    if (-f $subpath.'/'.$class_name.'.pm') {
        $controller_name = $class_name;
        $template_name = $class_name;
    } elsif ($class_name =~ /^prod_page/) {
        $controller_name = 'prod_page';
        $template_name = 'prod_page';
    }

    return ($controller_name, $template_name);
}

sub CustomController {
    my $self = shift;
    my ($controller_name, $template_name) = @_;

    print "[ astlombard::actions::index::CustomController ] $controller_name, $template_name\n";

    my $controller_file = $project.'::'.$Conf->Param('common/controllers').'::'.$controller_name;
    eval "require $controller_file";
    die $@ if ($@);
    return $controller_file->new($template_name);
}

sub ParseUrl {
    my $self = shift;

    my $node_id = 0;
    my $class_name = undef;
    my $params = {};

    if (!$App->NodePath(0)) {
        if ($self->{'default_class_name'} eq 'information_cat') {
            $node_id = $App->Param('i_c');
            $class_name = 'information_cat';
        } elsif (my $home = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(
            parent_id => $App->Param('i_c'),
            class_name => $self->{'default_class_name'}
        )->fetchrow_hashref()) {
            $node_id = $home->{'object_id'};
            $class_name = $home->{'class_name'};
        }
    } else {
        ($node_id, $class_name, $params) = GetNodeByURLPathAstlombard($App->Param('i_c'), $App->NodePath);
    }

    print "[ index::ParseUrl ] node_id: $node_id\n";
    print "[ index::ParseUrl ] class_name: $class_name\n";
    print "[ index::ParseUrl ] params:\n".Dumper($params);

    $node_id = $App->NodePath(1) if (!$node_id and !$Conf->Param('cgi/strict_url') and $App->NodePath(0) eq 'nodes');

    $App->Param($_, (ref $params->{$_} eq 'ARRAY') ? @{ $params->{$_} } : $params->{$_}) foreach (keys %$params);
    $App->Param('node_id', $node_id);

    if ($Conf->Param('cgi/exact_url')) {
        # Проверяем на несовпадение исходного урла с тем, который должен быть
        my $sample = ConstructUrlStringAstlombard('', { node_id => $node_id, %$params });
        if ($App->Request->uri ne $sample) {
            print "[ index::ParseUrl ] wrong canonical url: $sample\n";
            $node_id = 0;
            $App->Param('node_id', 0);
        }
    }
}

1;
